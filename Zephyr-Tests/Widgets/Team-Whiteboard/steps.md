# OpenDash360™ Widget: Team White Board
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Create White Board:||![]()
1|<li>Click the `Start White Board` button.<li>Complete the `Name` and `Metadata Tags` fields.<li>Click the `Launch Board` button.||New White Board is created.|![](./assets/tw1.gif)
||Add participant:||![]()
1|<li>Click the `Add Participant` button.<li>Select appropriate users.<li>Click the `Invite Users` button.||Users are sent notifications asking them to join the White Board.|![](./assets/tw2.gif)
||Claim presenter:||![]()
1|Click the `Claim Presenter` button.||User can now edit the White Board.|![](./assets/tw3.gif)
||Export:||![]()
1|Click the `Export Board` button in the bottom left corner of the widget.||White Board is exported as a .png.|![](./assets/tw4.gif)
||Add shape:||![]()
1|<li>Click the `Shape` icon in the left sidebar menu.<li>Select shape type and stroke thickness.<li>Draw shape onto canvas.||Shape is added to White Board.|![](./assets/tw5.gif)
||Add text:||![]()
1|<li>Click the `Text` icon.<li>Click onto the canvas and begins typing.||Text is added to White Board.|![](./assets/tw6.gif)
||Erase layers:||![]()
1|<li>Click the `Eraser` icon.<li>Click the canvas to begin erasing layers.||Layers are erased.<br><br>Note: Only non-edited layers are erasable.|![](./assets/tw7.gif)
||Select color:||![]()
1|<li>Click the `Color Pallet` icon.<li>Select the appropriate color.<li>Draw shape.||A shape is drawn on the White Board with the desired color.|![](./assets/tw8.gif)
||Add image:||![]()
1|<li>Click the `Add Image` icon.<li>Select the image from the local PC.||The appropriate image is added to the White Board.|![](./assets/tw9.gif)
||Move layer:||![]()
1|<li>Click the `Cursor` icon in the left sidebar menu.<li>Click layers to drag-and-drop the layer to desired location.||The layer is moved to the desired location.<br><br>Note: Only non-edited layers are movable.|![](./assets/tw10.gif)