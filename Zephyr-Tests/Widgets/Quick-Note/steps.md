# OpenDash360™ Widget: Quick Note
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Create new note:||![]()
1|<li>Click the text field.<li>Type the Note Title and Note Body.<li>Click the `Publish` button.||Expanded Note opens.<br><br>Note added to the `Others` list.|![](./assets/qn1.gif)
||Create new note:||![]()
2|Edit note:<br><br><li>Click the `Options` icon for the appropriate note.<li>Click the `Edit Note` option form the select menu.||Note opens within a pop-up window.|![](./assets/qn2.gif)
||Pin note:||![]()
1|Click the `Pin Note` option from the select menu.||Note is moved to the `Pinned` list.|![](./assets/qn3.gif)
||Share note:||![]()
1|<li>Click the `Add User` icon on the bottom of the note.<li>Select `Roles and User` from the drop-down menu.<li>Click the `Share` button.<li>Click the `Save` button.||Note is shared to appropriate roles and users.|![](./assets/qn4.gif)
||Highlight note:||![]()
1|<li>Click the `Color Pallet` icon on the bottom of the note.<li>Select the appropriate color.<li>Click the `Save` button.||Notes background is highlighted with the specified color.|![](./assets/qn5.gif)
||Add image:||![]()
1|<li>Click the `image` icon on the bottom of the note.<li>Select the appropriate image from the local PC.<li>Click the `Save` button.||Image is added to note.|![](./assets/qn6.gif)
||Add task list:||![]()
1|<li>Click the `Check` icon on the bottom of the note.<li>Add tasks titles.<li>Click the `Save` button.||Task list is added to note.|![](./assets/qn7.gif)
||Delete note:||![]()
1|<li>Click the `Options` icon for the appropriate note.<li>Click the `Delete Note` option from the select menu.||Note is Deleted.|![](./assets/qn8.gif)
||Filter by label:||![]()
1|<li>Click the select menu at the top right corner of the widget.<li>Select an option from the drop-down menu.||List of Notes is filtered by labels.|![](./assets/qn9.gif)
||Search notes:||![]()
1|<li>Type search criteria in the search field at the bottom of the widget.<li>Press the `Enter` key..||Note results are filtered by search criteria.|