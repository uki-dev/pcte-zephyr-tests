# OpenDash360™ Widget: World Clock
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Set time zone:||![]()
1|<li>Hover over the widget.<li>Click the `Filter` icon.<li>Select the `Set Timezone` option from the select menu.<li>Select the appropriate option from the select menu.<li>Click the `Update` button.||World Clock displays the appropriate time.|![](./assets/wc1.gif)