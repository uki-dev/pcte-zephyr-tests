# OpenDash360™ Widget: Task List: Lists
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Create a new Task from widget Header List:||![]()
1|<li>In widget header select the plus icon.<li>Select `Add new task list` from menu.<li>In the `Create New Task List` dialog, type in the title of the task in the `Task Title` text field.<li> Press the `Enter` key or click the `Create New Task List` button||A new task list appears under the `To Do` section of the widget.|![](./assets/1.gif)
||Expand a Task List:||![]()
1|Click to open the list by clicking in the list header.|| List panel drawer expands outward, showing tasks and the `Add new task` button.|![](./assets/2.gif)
||Edit a Task List (list owner only):||![]()
1|<li>Mouse over the List header of the list you wish to update.<li>Select the vertical bullets that appear.<li>Select the `Edit list` menu option<li>Enter the new title for the list in the `Task title` text field<li> Press the `Enter` key or click the `Edit` button||The list name is updated in the relevant row.|![](./assets/3.gif)
||Share a list( list owner only):||![]()
1|<li>Mouse over the List header of the list you wish to share.<li>Select the vertical bullets that appear.<li>Select the `Share list` menu option<li> Within the `Share List` dialog that appears:<ul> <li>to share with specific roles, click the `Share by role` text field and select the roles you wish to share with <li>to share with specific users, click the `Share by user` text field and select the users you wish to share with </ul> <li> Click the `Share` button||The appropriate list will now have a People icon, indicating the list is shared.  The list can be seen by all the selected users and roles.|![](./assets/4.gif)
||Delete a Task List (list owner only):||![]()
1|<li>Mouse over the List header of the list you wish to update.<li>Select the vertical bullets that appear.<li>Select the `Delete list` menu option<li> Select the `Delete` button||The appropriate list disappears from the list panel.|![](./assets/5.gif)
||Filter Lists by Context:||![]()
1|<li>Within the Widget Header, click the `Context Selector` field<li> Select the appropriate menu option; NOTE: The `Event` option does not exist outside of Events. ||Lists that appear within the list panel will be sorted by All tasks/Personal/Event, based on the option selected.|![](./assets/6.gif)
