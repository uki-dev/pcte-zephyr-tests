# OpenDash360™ Widget: Task List: Tasks
## Steps 
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Add new Task from Widget Header:||![]()
1|<li>Select the plus icon in the widget header.<li>Select the `Add new task` menu option.<li>Within the `Create New Task` dialog, enter the task's title in the title field <li> Press the `Enter` key or click the `Create New Task` button ||A new task is added to the `Uncategorized` list.|![](./assets/1.gif)
||Add new Task within a Task List:||![]()
1|<li> Click the `Add new task` button within the List panel<li>Within the `Create New Task` dialog, enter the task's title in the title field <li> Press the `Enter` key or click the `Create New Task` button||A new task is generated within the appropriate list.|![](./assets/2.gif)
||Edit a Task:||![]()
1|<li>Mouse over the Task that you wish to edit. <li>Click the `Pencil` icon that appears, located to the immediate right of the task title. <li> Within the `Edit Task` dialog, enter the new task title in the `Task Title` field <li> Press the `Enter` key or click the `Edit` button.||The new title appears for the appropriate task.|![](./assets/3.gif)
||Delete a Task:||![]()
1|<li>Mouse over the Task that you wish to delete. <li>Click the `Trash Can` icon that appears, located all the way to the far right of the task title. <li> Within the `Delete Task` dialog, click the `Delete` button. ||The task disappears from the appropriate list.|![](./assets/4.gif)
||Toggle Task Completion Status:||![]()
1|To update a Task's completion status, click the checkbox to the left of the Task title.||The checkbox should appear green with a check, or white with no check, depending on the previous state of the task.|![](./assets/5.gif)
||Reorder tasks:||![]()
1|<li>Click to open the list by clicking in the list header.<li>Drag-and-drop the task above or below or to other tasks.||Task is moved where the user drags.|![](./assets/6.gif)
