# OpenDash360™ Shared Request
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Add Shared Requests:||![]()
1|Open Microapp Manager Microapplication:<br><br><li>Click `Microapp Manager` within the main OpenDash360™ navigation.||The Microapplication Manager microapplication will load, displaying a table full of applications within OpenDash360™.|![](./assets/1.gif)
2|Open `Shared Request` Tab:<br><br><li>Click the `Shared Requests` tab.||User lands on the `Shared Request` interface.|![](./assets/shared-requests.png)
3|Add Shared Request:<br><br><li>Click the `Add Shared Request` button located in the top right corner of the Shared Request interface.<li>Fill out the required fields and click the `Save` button. ||The modal will close and the new Shared Request will be added to the table.|![](./assets/add-shared-request.gif)
||Edit Shared Requests:||![]()
1|Open Microapp Manager Microapplication:<br><br><li>Click `Microapp Manager` within the main OpenDash360™ navigation.||The Microapplication Manager microapplication will load, displaying a table full of applications within OpenDash360™.|![](./assets/1.gif)
2|Open `Shared Request` Tab:<br><br><li>Click the `Shared Requests` tab.||User lands on the `Shared Request` interface.|![](./assets/Shared-Request-Tab.gif)
3|Open `Update Shared Request` Modal:<br><br><li>Located the targeted Shared Request.<li>Hover over the row containing the Shared Request.<li>Click the `Options` icon that appears.<li>Click the `Edit` option.||The `Update Shared Request` modal will appear.|![](./assets/edit-sr.gif)
4|Edit Shared Request:<br><br><li>Fill out the fields that require updates and click the `Save` button. ||The modal will close and the Shared Request will be updated.|![](./assets/edit-shared-request.gif)