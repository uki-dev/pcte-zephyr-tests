# OpenDash360™ Add New Microapplication
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Add Microapplications:||![]()
1|Open Microapp Manager Microapplication:<br><br><li>Click `Microapp Manager` within the main OpenDash360™ navigation.||The Microapplication Manager microapplication will load, displaying a table full of applications within OpenDash360™.|![](./assets/1.gif)
2|Step 1:<br><br><li>Click the `Add New Microapp` button.<li>Select your vendor from the select box.<li>Choose your desired upload method.<li>Complete upload form fields required.<li>Click `View Details` button.<li>Click the `Submit` button.||Microapplication is successfully uploaded.|![](./assets/upload-1.png)
3|Step 2:<br><br><li>Confirm Information.<li>Your application icon, name and description has been pulled from your .odmanifest file.<li>You can change this information by editing the textbox and textarea.<li>Click `Confirm App` button.||The modal will reload and lead user to submitting the new application|![](./assets/step2.gif)
4|Application Overview:<br><br><li>The new application's `Overview Page` loads.<li>User Clicks the `Permissions` tab. ||Permissions Tab Loads.|![](./assets/upload-3.png)
5|Set Permissions:<br><br><li>Drag the Appropriate Roles from the Left Column into the  `App Visibility` and `Client Permissions` containers.||Notification Appears saying that the roles have been updated. Note: User needs to refresh the page to see applications updated in the naviation.|![](./assets/upload.gif)
||Edit Microapplications:||![]()
1|Open Microapp Manager Microapplication:<br><br><li>Click `Microapp Manager` within the main OpenDash360™ navigation.||The Microapplication Manager microapplication will load, displaying a table full of applications within OpenDash360™.|![](./assets/1.gif)
2|Choose an Application:<br><br><li>Click the title of the application that you wish to edit.||User is on the Microapplication Details page.|![](./assets/2.gif)
3|Click Options Icon:<br><br><li>Click the `Edit` option.||Edit Application Modal Appears.|![](./assets/edit.png)
4|Edit Microapplication Details:<br><br><li>Edit the application overview form and click the `Edit Microapp` button.||The application is updated with the edited information.|![](./assets/edit-microapp.gif)
||Change Microapplication Permissions:||![]()
1|Open Microapp Manager Microapplication:<br><br><li>Click `Microapp Manager` within the main OpenDash360™ navigation.||The Microapplication Manager microapplication will load, displaying a table full of applications within OpenDash360™.|![](./assets/1.gif)
2|Select a microapplication:<br><br><li>Click the title of the application that you wish to edit.||The user is now on the Microapplication Details page.|![](./assets/2.gif)
3|Click the `Permission` tab.||User is now on the able to edit microapplication permissions.|
4|Change Microapplication Permissions:<br><br><li>Drag & Drop the Permission over to the `App Visibilty` section||Roles are updated based on where the permissions are located. Users with the appropriate roles immediately have access to the application.|![](./assets/permissions.gif)
||Delete Microapplication:||![]()
1|Open Microapp Manager Microapplication:<br><br><li>Click `Microapp Manager` within the main OpenDash360™ navigation.||The Microapplication Manager microapplication will load, displaying a table full of applications within OpenDash360™.|![](./assets/1.gif)
2|View Microapplication Details:<br><br><li>Click the title of the application that you wish to edit.||User is now on the Microapplication details page.|![](./assets/2.gif)
3|Delete Microapplication:<br><br><li>Click the `Option` icon located in the header of the page. <li>Select the `Delete` option.<li>Confirm deletion.||The application has been deleted.|![](./assets/delete.png)
||Activate / Deactivate Microapplication:||![]()
1|Open Microapp Manager Microapplication:<br><br><li>Click `Microapp Manager` within the main OpenDash360™ navigation.||The Microapplication Manager microapplication will load, displaying a table full of applications within OpenDash360™.|![](./assets/1.gif)
2|View Microapplication Details:<br><br><li>Click the title of the application that you wish to edit.||The user lands on the Microapplication's details page.|![](./assets/2.gif)
3|Activate/Deactivate Microapplication:<br><br><li>Toggle the `Status` option of the application within the `Overview` table.||The application is immediately enabled or disabled based on what was selected.|![](./assets/activate-deactivate.gif)