# Landing Page Manager - Lobby Tab
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Enter Lobby Tab:||![]()
1|Click Lobby tab at top of page||<br><br>Navigate to Lobby page in Landing Page Manager|
||Settings:||![]()
1|Click settings button on left hand side to update lobby settings||<br><br>Update any field to enable the `Update lobby` button, save changes by clicking that. You will get a notification if successful and will see updates in the preview section|![](./assets/settings.gif)
||Info Banner:||![]()
1|Click info banner button on left hand side to update lobby info banner||<br><br>If disabled, enable option. Update any field to enable the `Update lobby` button, save changes by clicking that. You will get a notification if successful and will see updates in the preview section|![](./assets/banner.gif)
||App Cards:||![]()
1|Click App cards button on left hand side to update lobby/toolbar app cards||<br><br>If disabled, enable option. Click add additional app card button then click on the newly generated card. Once open, fill out the forms required field and click save app to update the quick access apps. While hovering over the title of a card you will see two buttons. The eye gives you the abiltiy to toggle between showing/hiding the card, the three dots allows you to either clone or delete an app card. You will see live updates in the preview section |![](./assets/app_cards.gif)
||Quick Access:||![]()
1|Click Quick Access button on left hand side to update lobby/toolbar quick access applications||<br><br>If disabled, enable option. Click add additional app button then click on the newly generated card. Once open, fill out the forms required field and click save card to update the quick access apps. While hovering over the title of a card you will see two buttons. The eye gives you the abiltiy to toggle between showing/hiding the card, the three dots allows you to either clone or delete a quick access app. You will see live updates in the preview section|![](./assets/qa.gif)
||Mini Dash:||![]()
1|Click the MiniDash button on left hand side to update the minidash||<br><br>If disabled, enable option. Update any field to enable the `Update lobby` button, save changes by clicking that. You will get a notification if successful and will see updates in the preview section|![](./assets/mini.gif)
||Support Overview:||![]()
1|Click the Support button on left hand side to update the lobby Support||<br><br>If disabled, enable option. Update any field to enable the `Update lobby` button, save changes by clicking that. You will get a notification if successful and will see updates in the preview section|![](./assets/support.gif)
||FAQ:||![]()
1|Click the Support button on left hand side, once on support page press FAQs tab in support section||<br><br>Press Create FAQ button to create a new FAQ. Fill out form and click Publish FAQ button. Afterwards, clickthe refresh icon on the table to see the newly created FAQ. Click on three dots while hovering over a row to edit, delete, publish/unpublish a FAQ. |![](./assets/faq.gif)
||Videos:||![]()
1|Click the Support button on left hand side, once on support page press Videos tab in support section||<br><br>Press upload video to launch the upload dialog. Follow steps to upload a video. When finish wait a moment and then click the refresh button on the table. If successful, your video will appear. While hovering over a row click the three dot button on the far right in order to publish/unpublish, view, edit, or delete your video. Deleting a video may take a moment to reflect locally, so press the frefresh button shortly after deleting to see changes reflected. |![](./assets/video.gif)
