# Landing page manager - Environment tab
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Settings:||![]()
1|Click settings button on left hand side to update environment settings||<br><br>Update any field to enable the `Update environment` button, save changes by clicking that. You will get a notification if successful|![](./assets/env_settings.gif)
||SMTP:||![]()
1|Click SMTP button on left hand side to update SMTP settings||<br><br>If turned off, toggle the settings button on.Update any field to enable the `Update environment` button, save changes by clicking that. You will get a notification if successful|![](./assets/env_smtp.gif)
||Custom CSS:||![]()
1|Click custom css button on left hand side to update custom css settings||<br><br>If turned off, toggle the settings button on. If desired, add custom css into the field then return to lobby to view changes after saving|![](./assets/env_customcss.gif)
||White Labeling:||![]()
1|Click white labeling button on left hand side to update environment white labeling||<br><br>Update any field to enable the `Update environment` button, save changes by clicking that. You will see changes reflected in the live preview and get a notification if successful|![](./assets/white_label.gif)
||RCS Selector:||![]()
1|Click RCS selector button on left hand side to update environment RCS selector settings||<br><br>If turned off, toggle the settings button on. Click add additional environment button and complete form, click save card to update. While hovering over title of new card after saving select three dots and click clone to duplicate the card. You can also hide the card by pressing the eye button that appears on hover and delete by clicking the delete option that appears in the menu from the three dot button|![](./assets/rcs_selector.gif)


