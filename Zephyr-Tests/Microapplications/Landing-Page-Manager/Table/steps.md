# Landing Page Manager Environment table
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Create Environment:||![]()
1|Launch the Landing Page Manager.|The Landing Page Manager will open. The landing page will include a list of current environments.|![](./assets/landing-1.png)
2|Click the "Add New Environment" button, located at the top of the page.|A modal will appear.|![](./assets/landing-2.png)
2|Complete the entire for and click the "Create" button, located at the bottom of the modal.|The modal will disappear. After a few seconds the new environment will be added to the table containing the current environment.|![](./assets/LandingPageManager-CreateEnviron.gif)
||Delete Environment:||![]()
1|Hover over row of newly created environment in step one. On the far right of the table click the three dots that appear, then click the delete button. Once dialog opens, click red delete button||Menu should appear once three vertical dot button is clicked. After clicking on delete option a dialog should appear. Once you confirm the delete, the environment should be removed from the table|![](./assets/delete_env.gif)
||Selecting Environment:||![]()
1|Click the name of the environment you wish to enter||Navigate to that environment's landing page manager|![](./assets/nav.gif.gif)

