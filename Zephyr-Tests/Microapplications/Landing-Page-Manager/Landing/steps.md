# Landing Page Manager - Landing tab
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Enter Landing Tab:||![]()
1|Click landing tab at top of page||<br><br>Navigate to landing page in Landing Page Manager|
||Settings:||![]()
1|Click settings button on left hand side to update landing settings||<br><br>Update any field to enable the `Update landing` button, save changes by clicking that. You will get a notification if successful and will see updates in the preview section|![](./assets/land_set.gif)
||Info Banner:||![]()
1|Click info banner button on left hand side to update landing info banner||<br><br>If disabled, enable option. Update any field to enable the `Update landing` button, save changes by clicking that. You will get a notification if successful and will see updates in the preview section|![](./assets/banner.gif)
||Buttons:||![]()
1|Click Buttons button on left hand side to update landing Buttons||<br><br>Click add additional button to create a new button for the landing page. Fill out the form and click the `update landing` button to save new button. Expand a button card by clicking on it, while opened you can hide or delete the button by clicking the respective button. If successful you should see updates in the preview section to the right|![](./assets/landing_buttons.gif)
||Consent:||![]()
1|Click consent button on left hand side to update landing consent||<br><br>Update any field to enable the `Update landing` button, save changes by clicking that. You will get a notification if successful and will see updates in the preview section|![](./assets/cons.gif)
||Compatibility:||![]()
1|Click compatibility button on left hand side to update landing compatibility||<br><br>If disabled, enable option. Clcik add additional browser button. Fill out form and click update landing once complete. If successful notification will appear|![](./assets/compat.gif)
