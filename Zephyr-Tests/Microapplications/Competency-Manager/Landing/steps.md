## Navigate to Competency Manager
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Within the OpenDash360™, click on the `Competency Manger` microapplicaton||The `Competency Manager` microapplication will load.|![](./assets/images/competencyd-1.png)

##  Create New Framework
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Click the "Create Framework" button.||A modal will launch.|![](./assets/images/competency-2.png)
2|Fill out the "Create Framework" form and click the "Create Framework" button when finished.||The modal will close and the user will be redirected to the Framework's page.|![](./assets/images/competency-3.png)

##  Create First Competency
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Click the "Create Root" button.||A modal will launch.|![](./assets/images/competency-4.png)
2|Fill out the "Create Competency" form and click the "Create Competency" button when finished.||The modal will close and the competency will be added to the framework's hierarchy.|![](./assets/images/competency-5.png)

##  Create Child Competency
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Hover over the competency that was created in the steps above.||A "+" icon will  appear on the right side of the competency.|![](./assets/images/competency-6.png)
2|Click the "+" icon.||A modal will launch.|![](./assets/images/competency-5.png)
3|Fill out the "Create Competency" form and click the "Create Competency" button when finished.||The modal will close and the competency will be added to the framework's hierarchy. <br><br>Additionally, the diagram on the right side of the page will update to show that the competencies are connected.|![](./assets/images/competency-7.png)


##  Edit Competency Details
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Click the desired competency within the hierarchy chart on the left side of the page.||The competency's details will appear on the left side of the page.|![](./assets/images/competency-7.png)
2|Click the "Options" icon located to the right of the competency's title.||A menu will appear with various options.|![](./assets/images/competency-8.png)
3|Click the "Edit" option.||A modal will appear.|![](./assets/images/competency-5.png)
4|Fill out the desired changes and click the submit button when finished.||The modal will close and the competency will be updated with the new information.|![](./assets/images/competency-7.png)



##  Add Relationship to Competency
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Click the "+" icon located to the right of the "Relationships" title.||A modal will appear.|![](./assets/images/competency-9.png)
2|Click the "Relationship Type" select box and select the desired relationship.||Relationship is selected.|![](./assets/images/competency-10.png)
3|Use the "Search Form" to locate the desired competency. Once found, hover over the comptency's row.||A "+" icon appears.|![](./assets/images/competency-11.png)
4|Click the "+" icon.||The competency will disapear and the user will see a notification at the bottom right corner of their screen that confirms the relationship.|![]()
5|Click the "Add Relationship" button.||The modal will disapear and the user will see the updated relationships..|![]()




##  Add Crosswalk
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Within a framework, click the "Options" icon located next to the framework's title.||A popout menu will appear.|![](./assets/images/competency-12.png)
2|Click the "Crosswalk" option.||A modal will appear that asks the user to select the crosswalked framework.|![](./assets/images/competency-13.png)
3|Choose a framework and click the "Create Crosswalk" button.||The modal will be removed and the user will be redirected to the crosswalk page..|![](./assets/images/competency-14.png)
4|Click a checkbox located next to a competency located in the left column.||The checkbox will be checked.|![](./assets/images/competency-15.png)
5|Click a checkbox located next to a competency located in the right column.||The checkbox will be checked.|![](./assets/images/competency-16.png)
6|Scroll to the bottom of the screen and click the "Map Competencies" button located in the lower right corner.||A modal will appear.|![](./assets/images/competency-17.png)
7|Fill out the form and click the "Confirm" button when finished.||The modal will disapear and the user will be redirected back to the framework page.|![](./assets/images/competency-18.png)


