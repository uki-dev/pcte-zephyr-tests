# OpenDash360™  Organizations
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Search Organization Hierarchy Tree:||![]()
1|From the User Manager microapplication, Click the "Organizations" tab, located in the header.||Organization" tab  loads|![](./assets/org-1.png)
2|Click arrow location to the left of the root organization.||Root organization expands displaying all child organizations. |![](./assets/org-2.png)
3|Click arrow location to the left of a child organization (if applicable).||The child organization expands displaying all child organizations (if applicable). |![](./assets/org-3.png)
||Create Organization:||![]()
1|Click the "Create Org" button located above the organization hierarchy.||"New Organization" panel appears to the right containing a basic form. |![](./assets/org4.png)
2|Complete the "New Organization" form and click "Create Org" button.||"New Organization" is created and user is redirected to organization's "Details" page. |![](./assets/org5.png)
||Change Hierarchy to Show Only Children Within Single Organization:||![]()
1|Hover over desired organization's name.||"Account_Tree" icon appears to the right of the organization's name.|![](./assets/org6.png)
2|Click the "Account_Tree" icon.||"Hierarchy restructures to show only child organizations.|![](./assets/org7.png)
||Revert to Show All Organizations:||![]()
1|Hover over parent organization.||"Account_Tree" icon appears to the right of the organization's name.|![]()
2|Click "Account_Tree" icon.||Hierarchy restructures to show all organizations.|![](./assets/org3.png)
||View Organizations Details (Users):||![]()
1|Within the organization's hierarchy page, click the organization's name.||"User is redirected to organization's "Details" page. This shows the organization's users by default.|![](./assets/org-4.png)
||Search Users:||![]()
1|Click the search input field.<li>Begin typing based on filter type.<li>Press Enter key.||The User table will update based on the search criteria.|![](./assets/search.png)