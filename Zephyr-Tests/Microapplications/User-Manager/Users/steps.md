# OpenDash360™  Users
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Search Users:||![]()
1|Open User Manager Microapplication:<br><br>Click the `User Manager` menu option within the OpenDash360™ main navigation.||Users will be sent to the User Manager microapplication and initially see a table that contains OpenDash360™ users/|![](./assets/users-1.png)
2|Search Users:<br><br><li>Click the search input field.<li>Begin typing based on filter type.<li>Press Enter key.||The User Manager table will update based on the search criteria.|![](./assets/search.png)
||Filter User Columns:||![]()
1|Display Filter:<br><br>Hover a columns header.||Icon will appear on right side of the column header.|![](./assets/head.png)
2|Filter:<br><br>Click the column header.||User table will be updated.|![](./assets/email-column.png)
||Access User Profile:||![]()
1|Click Row:<br><br>Click anywhere on the row of the user that you wish to view||User's profile will load.|![](./assets/users-2.png)
||Add Second Email:||![]()
1|Alternate Email:<br><br>From the "Personal Information" tab, click the "Add alternate email" button. ||"Add Email" input appears.|![](./assets/users-3.png)
2|Save Email:<br><br><li>Enter email address in the input field. <li>Click the "Save" icon.||Email address is added.|![]()
||Change User's Password:||![]()
1|Update Password:<br><br>Enter password into "New Password" input and "Confirm New Password" input..||"Security & Access" tab loads.|![](./assets/users-4.png)
2|Save Password:<br><br>Click the "Save" icon.||Password is changed|![]()
||Change User's Roles:||![]()
1|Change Tab:<br><br>Access "Security & Access" tab.||"Security & Access" tab loads.|![](./assets/users-5.png)
2|Toggle Roles:<br><br>Click all required roles (checkmark designates positive role).||User now has these roles.|![]()