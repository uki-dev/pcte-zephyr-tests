# OpenDash360™  Sponsored Accounts
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Access Sponsored Accounts Interface:||![]()
1|Open User Manager Microapplication:<br><br>Click the `User Manager` menu option within the OpenDash360™ main navigation.||Users will be sent to the User Manager microapplication and initially see a table that contains OpenDash360™ users/|![](./assets/landing.png)
2|Access "Sponsored Accounts" tab:<br><br><li>Click "Sponsored Accounts" tab.||"Sponsored Accounts" tab is loaded.|![](./assets/sponsored-accounts-1.png)
||Search Sponsored Accounts:||![]()
1|Search Users:<br><br><li>Click the search input field.<li>Begin typing based on filter type.<li>Press Enter key.||The "Sponsored Accounts" table will update based on the search criteria.|![](./assets/search.png)
||Filter "Sponsored Accounts" Columns:||![]()
1|Display Filter:<br><br>Hover a columns header.||Icon will appear on right side of the column header.|![](./assets/head.png)
2|Filter:<br><br>Click the column header.||"Sponsored Accounts" table will be updated.|![](./assets/email-column.png)
||Add Sponsored Accounts:||![]()
1|Click "Add Sponsored Accounts" Button:<br><br>Click the "Add Sponsored Accounts" button located above the "Sponsored Accounts" table.||"Add Sponsored Accounts" modal launches|![](./assets/add-sponsored-accounts.png)
2|Submit Form:<br><br><li>Fill in the required field.<li>Click the "Create Sponsorship" button.||Modal is removed. Sponsored Accounts is added|![](./assets/sponsored-accounts-modal.png)
||View Sponsored Account's Details:||![]()
1|Click Sponsored Account's Row:<br><br>Click anywhere on the row of the Sponsored Accounts that you wish to view.||"Sponsored Accounts" modal appears, with Sponsored Accounts Details|![](./assets/details.png)
||Delete Sponsored Account:||![]()
1|Hover Over Sponsored Account's Row:<br><br><li>Hover anywhere on the row of the Sponsored Accounts that you wish to delete.<li>An "X" icon appears on the right side of the row.<li>Click the "X" icon<li>A menu appears with various options <li>Click the "Delete" menu option||Modal appears to confirm action.|![](./assets/sponsored-accounts-2.png)


