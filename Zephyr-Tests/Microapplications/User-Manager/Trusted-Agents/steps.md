# OpenDash360™  Trusted Agents
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Access Trusted Agents Interface:||![]()
1|Open User Manager Microapplication:<br><br>Click the `User Manager` menu option within the OpenDash360™ main navigation.||Users will be sent to the User Manager microapplication and initially see a table that contains OpenDash360™ users/|![](./assets/users-1.png)
2|Access "Trusted Agents" tab:<br><br><li>Click "Trusted Agents" tab.||"Trusted Agents" tab is loaded.|![](./assets/trusted-agents-2.png)
||Search Trusted Agents:||![]()
1|Search Users:<br><br><li>Click the search input field.<li>Begin typing based on filter type.<li>Press Enter key.||The "Trusted Agents" table will update based on the search criteria.|![](./assets/search.png)
||Filter "Trusted Agents" Columns:||![]()
1|Display Filter:<br><br>Hover a columns header.||Icon will appear on right side of the column header.|![](./assets/head.png)
2|Filter:<br><br>Click the column header.||"Trusted Agents" table will be updated.|![](./assets/email-column.png)
||Add Trusted Agents:||![]()
1|Click "Add Trusted Agent" Button:<br><br>Click the "Add Trusted Agent" button located above the "Trusted Agents" table.||"Add Trusted Agent" modal launches|![](./assets/trusted-agents-3.png)
2|Submit Form:<br><br><li>Fill in the username field.<li>Click the "Create Endorsement" button.||Modal is removed. Trusted Agent is added|![](./assets/trusted-agents-3.png)
||View Trusted Agent's Sponsor:||![]()
1|Click "Trusted Agent" Name:<br><br>Click the trusted agent's name that you wish to view.||"Sponsorship" modal appears, with Sponsorship Details|![](./assets/view.png)
||Revoke Trusted Agent's Authority:||![]()
1|Hover Over "Trusted Agent" Row:<br><br>Hover anywhere on the row of the trusted agent that you wish to revoke.||"X" icon appears on the right side of the row.|![](./assets/x.png)
2|Revoke Authority:<br><br>Click the "X" icon.||Modal appears to confirm action.|![]()



