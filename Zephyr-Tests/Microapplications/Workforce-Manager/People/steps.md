## Navigate to Workforce Manager
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Within the OpenDash360™, click on the `Workforce Manger` microapplicaton||The `Workforce Manager` microapplication will load.|![](./assets/images/Workforce-1.png)

## View a List of People in an Organization
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>Click the `People` tab.||The default table will display all of the people in within the organization.|![](./assets/images/People-1.png)
3|<li>To view another organization, the user must click and expand the organization's panel to the left of the organization name.||A panel will pop out that diplays various organizations. |![](./assets/images/Workforce-2.png)
4|<li>Using the file tree, a user will click the organization that they wish to view.||The panel will collapse and the user will be see all of that particular organizaton's people.|![](./assets/images/Workforce-3.png)

## Assign People to a Position
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>The user will click the position that they wish to edit.||The user will now be in the submenu for positions.  The default tab is the `Requirements` tab.  The user will be viewing a table of requirements for that particular position.|![](./assets/images/Workforce-7.png)
3|<li>Click the `People` tab.||The user is now presented with a table that lists the people that are assigned to that particular position.|![](./assets/images/Workforce-8.png)
4|<li>Click the `Assign People` button located above the table.<li>A modal appears and the user must begin typing a user’s name until the auto-fill display’s the correct user. <li>Click on the user’s name.<li>The user can repeat this process until they have added all of the users that need to be added to this position.<li>Click ther `Assign` button.||The modal will close and the new users are added to the table within the `People` tab.|![](./assets/XXXX.png)

## Navigate to the Training Jacket for a Person
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>The user will click the position that they wish to edit.||The user will now be in the submenu for positions.  The default tab is the `Requirements` tab.  The user will be viewing a table of requirements for that particular position.|![](./assets/images/Workforce-7.png)
3|<li>Click the `People` tab.||The user is now presented with a table that lists the people that are assigned to that particular position.|![](./assets/images/Workforce-8.png)
4|<li>Click on the desired person's name.||The user will be redirected to the universal training jacket for the selected person.|![](./assets/XXXX.png)

