## Navigate to Workforce Manager
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Within the OpenDash360™, click on the `Workforce Manger` microapplicaton||The `Workforce Manager` microapplication will load.|![](./assets/images/Workforce-1.png)

## View a list of Positions in an Organization
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>To view another organization, the user must click and expand the organization's panel to the left of the organization name.||A panel will pop out that diplays various organizations. |![](./assets/images/Workforce-2.png)
3|<li>Using the file tree, a user will click the organization that they wish to view.||The panel will collapse and the user will be viewing the positions table of the organization that was selected. This table displays all available positions within the organization that has been selected.|![](./assets/images/Workforce-3.png)

## Create a Position
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>Click the `Create Position` button located above the position's table.<li>In the modal that appears, enter the position's name and the number of people allocated to that position.<li>Click the `Create` button.||The modal will be closed and new position will be created.|![](./assets/images/Workforce-4.png)

## Delete a Position
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>Mouse over the position that you wish to delete.<li>Click the `Options` icon located in the appropriate position's row. <li>Click the `Delete` option.<li>Click the `Delete` button in the Confirmation Dialog modal to confirm the deletion of the assessment template.||The position will be removed from OpenDash360™.|![](./assets/images/Workforce-5.png)

## Edit a Position's Title
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>Mouse over the position that you wish to edit.<li>Click the `Pencil` icon located in the appropriate position's row. <li>In the `Edit Position` modal, the user has the ability to make the desired changes to the position.<li>Click the `Update` button at the bottom of the modal.||The modal will close and the position's details will be updated.|![](./assets/images/Workforce-6.png)

## Add/Remove a Requirement to a Position
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>The user will click the position that they wish to edit.||The user will now be in the submenu for positions.  The default tab is the `Requirements` tab.  The user will be viewing a table of requirements for that particular position.|![](./assets/images/Workforce-7.png)
3|<li>Click the `Add Requirement` button.<li>The user must select the framework that houses the competency(s) that they wish to add.<li>The user can type in the competency until the auto-complete displays the correct competency.<li>Click on it to add the competency to the list.<li>Repeat this process to add the desired competencies from the selected framework.<li>Click on the `Add` button to complete the addition.||The modal will close and the new requirements will be added to the position.|![](./assets/XXXX.png)

## View a List of People in a Position
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>The user will click the position that they wish to edit.||The user will now be in the submenu for positions.  The default tab is the `Requirements` tab.  The user will be viewing a table of requirements for that particular position.|![](./assets/images/Workforce-7.png)
3|<li>Click the `People` tab.||The user is now presented with a table that lists the people that are assigned to that particular position.|![](./assets/images/Workforce-8.png)

## Assign People to a Position
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>The user will click the position that they wish to edit.||The user will now be in the submenu for positions.  The default tab is the `Requirements` tab.  The user will be viewing a table of requirements for that particular position.|![](./assets/images/Workforce-7.png)
3|<li>Click the `People` tab.||The user is now presented with a table that lists the people that are assigned to that particular position.|![](./assets/images/Workforce-8.png)
4|<li>Click the `Assign People` button located above the table.<li>A modal appears and the user must begin typing a user’s name until the auto-fill display’s the correct user. <li>Click on the user’s name.<li>The user can repeat this process until they have added all of the users that need to be added to this position.<li>Click ther `Assign` button.||The modal will close and the new users are added to the table within the `People` tab.|![](./assets/XXXX.png)

## Navigate to the Training Jacket for a Person
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to the `Workforce Manager` microapplication.||The user will enter into and be be viewing the position table of the user's default organization |![](./assets/images/Workforce-1.png)
2|<li>The user will click the position that they wish to edit.||The user will now be in the submenu for positions.  The default tab is the `Requirements` tab.  The user will be viewing a table of requirements for that particular position.|![](./assets/images/Workforce-7.png)
3|<li>Click the `People` tab.||The user is now presented with a table that lists the people that are assigned to that particular position.|![](./assets/images/Workforce-8.png)
4|<li>Click on the desired person's name.||The user will be redirected to the universal training jacket for the selected person.|![](./assets/XXXX.png)

