# OpenDash360: Dashboard Templates
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Add Dashboard Template:||![]()
1|Open Dashboard Manager:<br><br><li>Click the Dashboard Manager menu option from the application launcher.||The Dashboard Manager microapplication will open, displaying a table containing Event Collections.|![](./assets/dashboard-1.png)
2|Navigate into an event collection:<br><br><li>From the table, click on the name of an event to open it.||You are now viewing dashboard templates for this event.|
3|Create a dashboard template:<br><br><li>At the top right of the table, click `Create Dashboard`.<br><br><li>Fill out required fields.<br><br><li>Click `Add Event Collection` button.||The newly created dashboard tempalte is added to the table.|![](./assets/dashboard-2.png)
||Delete Dashboard Template:||![]()
1|Open Dashboard Manager:<br><br><li>Click the Dashboard Manager menu option from the application launcher.||The Dashboard Manager microapplication will open, displaying a table containing Event Collections.|![](./assets/dashboard-1.png)
2|Navigate into an event collection:<br><br><li>From the table, click on the name of an event to open it.||You are now viewing dashboard templates for this event.|
3|Delete Dashboard Template:<br><br><li>Hover over the dashboard that you would like to delete.<br><br><li>Click on the 3 dots on the right side of the row.<br><br><li>Click the `Delete` button.||The recently deleted dashboard is removed from the table.|![](./assets/2-delete.png)
||Edit Dashboard Template Settings:||![]()
1|Open Dashboard Manager:<br><br><li>Click the Dashboard Manager menu option from the application launcher.||The Dashboard Manager microapplication will open, displaying a table containing Event Collections.|![](./assets/dashboard-1.png)
2|View Event Collection:<br><br><li>Click the title of the desired Event Collection.||The Event Collections page will load.|![](./assets/dashboard-3.png)
3|View Dashboard Template:<br><br><li>Click the title of the dashboard template that the user wishes to view.||The Dashoard Template Details page will load.|![](./assets/7-edit.png)
4|Edit Dashboard Template Settings:<br><br><li>Click the `options` icon located next to the Dashboard Template Title, located in the header of the page.<br><br><li>Click the `Dashboard Options` option.<br><br><li>Update the template settings in the form that appears on the right side of the page.<br><br><li>Click the `Save Settings` button. ||An alert will appear at the bottom of the page telling the user that the settings have been saved.|![](./assets/8-edit.png)
||Add Widget to Dashboard Template:||![]()
1|Open Dashboard Manager:<br><br><li>Click the Dashboard Manager menu option from the application launcher.||The Dashboard Manager microapplication will open, displaying a table containing Event Collections.|![](./assets/dashboard-1.png)
2|View Event Collection:<br><br><li>Click the title of the desired Event Collection.||The Event Collections page will load.|![](./assets/dashboard-3.png)
3|View Dashboard Template:<br><br><li>Click the title of the dashboard template that the user wishes to view. <br><br><li>If there is no dashboard, click `Create dashboard` and fill out the required fields.||The Dashoard Template Details page will load.|![](./assets/7-widget.png)
4|Add Widgets to Dashboard Template:<br><br><li>Click the widget card of any widget that is to be added to the dashboard template.<br><br><li>Click the `Save` button located in the header of the page.||The widget will be added and saved to the dashboard template.|![](./assets/9-widget.png)