# OpenDash360: Event Collections
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Add Event Collections:||![]()
1|Open Dashboard Manager:<br><br><li>Click the Dashboard Manager menu option from the application launcher.||The Dashboard Manager microapplication will open, displaying a table containing Event Collections.|![](./assets/1.png)
2|Add Event Collection:<br><br><li>Click `Add Event Collection` button.<br><br><li>Fill out required fields.<br><br><li>Click `Add Event Collection` button.||The newly created event collection is added to the event collection table.|![](./assets/2.png)
||Delete Event Collections:||![]()
1|Open Dashboard Manager:<br><br><li>Click the Dashboard Manager menu option from the application launcher.||The Dashboard Manager microapplication will open, displaying a table containing Event Collections.|![](./assets/1-delete.png)
2|Delete Event Collection:<br><br><li>Hover over the event that you would like to delete.<br><br><li>Click on the 3 dots on the right side of the table row.<br><br><li>Click the `Delete` button.||The recently deleted event collection is removed from the event collection table.|![](./assets/2-delete.png)