# OpenDash360™ Activate/Deactivate Microapplication
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Send Broadcast:||![]()
1|Click `Notifications` within the main OpenDash360™ navigation.||The Notification Broadcast microapplication will load, displaying a form that allows broadcasts to be sent via OpenDash360™.|![](./assets/Notification-Launch.gif)
2|Type Broadcast Title:||Form Updated. This will be the Title of the broadcast received by users.|![](./assets/message.png)
3|Type Broadcast Message:||Form Updated. This will be the Message of the broadcast received by users.|![](./assets/message.png)
4|Select Broadcast via:<br><br><li>Select `Platform`.||The Broadcast will be sent to all users by utlizing the OpenDash360™ Notification Service.|![](./assets/platform.png)
5|<li>Toggle `Send to Everyone` to send broadcast to all OpenDash360™ users. <li>Specify individual `Roles` or `Users` to send messages individually or as a group..||Doing this specifies which users will receive the broadcast.|![](./assets/recipients.gif)
6|Click the `Send Broadcast` button..||The receipients selected receive the following notification.|![](./assets/Notification-Broadcast.gif)