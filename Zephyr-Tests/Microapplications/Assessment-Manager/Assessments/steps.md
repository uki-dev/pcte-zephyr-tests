## Navigate to Assessment Manager
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Within the OpenDash360™, click on the `Assessment Manger` microapplicaton||The `Assessment Manager` microapplication will load.|![](./assets/images/assessment-manager-1.png)

## Create a Manual Assessment
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Click the `Assessment` tab.<li>Click the `Create Assessment` button located above the assessment's table.<li>Input  name and keywords for the new assessment and ensure `Manual Exercise` is selected.<li>Click the `Create` button to generate the new Assessment.||This new assessment is created and the user is redirected to the `Assessment Details` page.|![](./assets/images/assessment-manager-2.png)

## Access Details Page
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Click the `Assessment` tab.<li>Mouse over the assessment that you wish to edit.<li>Click the `Options` icon located in the appropriate assessment's row. <li>Click the `Edit` option. || User is redirected to the `Assessment Details` page.|![](./assets/images/assessment-manager-3.png)

## Clone Assessment
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Click the `Assessment` tab.<li>Mouse over the assessment that you wish to clone.<li>Click the `Options` icon located in the appropriate assessment's row. <li>Click the `Clone` option. || User is redirected to the cloned assessment's `Assessment Details` page.|![](./assets/images/assessment-manager-4.png)

## Delete Assessment
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Click the `Assessment` tab.<li>Mouse over the assessment that you wish to delete.<li>Click the `Options` icon located in the appropriate assessment's row. <li>Click the `Delete` option.<li>Click the `Delete` button in the Confirmation Dialog modal to confirm the deletion of the assessment.|| Assessment is deleted from OpenDash360™. User remains on the Assessment Manager's landing interface.|![](./assets/images/assessment-manager-4.png)

## Publish an Assessment
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Click the `Assessment` tab.<li>Mouse over the assessment that you wish to publish.<li>Click the `Options` icon located in the appropriate assessment's row. <li>Click the `Edit` option.||User is taken to the `Assessment Details` interface and is placed within the `Overview` tab. |![](./assets/images/assessment-manager-3.png)
2|<li>Review / Edit the name, enter a description, tag with metadata, and update the assessment. <li>Click the `Update` button.||The  `Overview` data is updated and the assessment is in the `Draft` state|![](./assets/images/assessment-manager-5.png)
3|<li>Click the `Objective Scoring` sub menu.<li>Review / Edit the scoring levels for the assessment (using global scoring scheme).<li>Toggle `Impose Global Scoring`.<li>Update the appropriate `Score` names.<li>Select the colors for each score option by clicking the color circle.<li>Select the `Passing` option for scores that are considered passing.<li>Click the `Add Additional Scoring Level` for more score options.<li>Click the `Update` button.||Assessment Scoring is updated.|![](./assets/images/assessment-manager-6.png)
4|<li>Click the `Builder` tab.<li>Review / Edit assessment objectives by utilizing the assessment hierarchy on the left. For each objective, define the evaluation criteria justifying each scoring level.<li>Click the `Create Objective ` button and complete the form within the modal to add a top level objective.<li>Click the `Right Arrow` next to the Root topic's name to expand the hierarchy chart.<li>Hover over a level's name within the hierarchy chart and click the `Options` icon to add a new child objective or delete the former objective.<li>Click the `Update` button.||The assment objectives and evaluation criteria are updated. |![](./assets/images/assessment-manager-7.png)
5|<li>Select an objective that you would like to edit, by clicking the objectives title<li>Fill out the fields located under the `Details` header.<li>Fill out the fields under the `Grading Scale` header.<li>Click the `Update` button.||The assment objectives and evaluation criteria are updated. |![](./assets/images/assessment-manager-8.png)
6|<li>Click the `Competency Mapping` tab.<li>Select a target framework.<li>Select the desired line item from the assessment and map it to the target framework.<li>Review the mapping below and confirm the mapping. <li>Continue this for each line item that needs to be mapped.<li>Click the `Save` button.||Competencies are successfully mapped.|![](./assets/images/assessment-manager-9.png)
7|<li>Click the `Preview` tab.<li>Review the assessment as if you were delivering the assessment by utilziing the hierarchy on the left.<li>Click one of the `Status` radio buttons.<li>Add an `Optional Comment`. <li>Click the `Add Comment` button. <li>Select the assessment wheel in the bottom left to preview the live graphic.||Status is added to the Assessment. Comment is added.|![](./assets/images/assessment-manager-10.png)
8|<li>Within the header of the assessment, change the assessment status from `Draft` to `Published'.||Assessment is published.|![](./assets/images/assessment-manager-11.png)

## Schedule and Assign an Assessment
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Click the `Assessment` tab.<li>Mouse over the assessment that you wish to schedule.<li>Click the `Options` icon located in the appropriate assessment's row. <li>Click the `Schedule` option.<li>Enter the `Assessment Delivery Name`, `Start Date`, `Start Time, `End Date` and `End Time`, within the `Schedule Assessment` modal.<li>Click the `Schedule` button located at the bottom of the modal.|| The Assessment has been scheduled. The user is redirected to the `Assessment Schedule` page, where they can enter additional details.|![](./assets/images/assessment-1.png)
2|<li>The user is now within the `Overview ` sub menu on the `Assessment Schedule` page. Review the `Overview` data and modify any editable elements.<li>Toggle the `End Date` option (If applicable).<li>Toggle the `Notify Participants` option (If applicable).<li>Click the `Update` button.||The `Overview` data is updated. Assessment details are displayed in the right column.|![](./assets/images/assessment-2.png)
3|<li>Click the `Assessor` sub menu.<li>Click the `Add Assessors` button located towards the top of the `Assessors Table`. <li>A modal appears and the user can begin typing the Assessor's name within the search field.<li>Select the desired assessor from the table.<li>Repeat this process to continue adding assessors.<li>Click the `Add` button within the modal to complete the addition.||The assessor(s) is added to the scheduled assessment.|![](./assets/images/assessment-3.png)
4|<li>Click the `Add Assessors` button located underneath the `Organizations` heading. <li>A modal appears and the user can begin typing an organization's name within the search field.<li>Select the desired organization from the table.<li>Repeat this process to continue adding organizations.<li>Click the `Add` button within the modal to complete the addition.||Every user within the selected organization will be added as an accessor(s) is added to the scheduled assessment.|![](./assets/images/assessment-4.png)
5|<li>To delete a user from the accessor list, hover over the desired user's card. <li>Click the `Trash` icon.||`The user is removed from the assessment.|![](./assets/images/assessment-5.png)
