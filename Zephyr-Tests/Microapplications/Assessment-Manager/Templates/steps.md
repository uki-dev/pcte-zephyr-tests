## Navigate to Assessment Manager
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Within the OpenDash360™, click on the `Assessment Manger` microapplicaton||The `Assessment Manager` microapplication will load.|![](./assets/images/Assessment-Manager-1.png)

## Create a Manual Assessment Template
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Click the `Create Assessment` button located above the assessment template's table.<li>Input  name and keywords for the new assessment and ensure `Manual Exercise` is selected.<li>Click the `Create` button to generate the new Assessment.||This new assessment template is created and the user is redirected to the `Template Details` page.|![](./assets/images/Assessment-Manager-2.png)

## Access Template Details Page
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Mouse over the assessment template you wish to edit.<li>Click the `Options` icon located in the appropriate template's row. <li>Click the `Edit` option. || User is redirected to the `Template Details` page.|![](./assets/images/Assessment-Manager-3.png)

## Clone Template
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Mouse over the assessment template you wish to clone.<li>Click the `Options` icon located in the appropriate template's row. <li>Click the `Clone` option. || User is redirected to the cloned template's `Template Details` page.|![](./assets/images/clone-1.png)

## Delete Template
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Mouse over the assessment template you wish to delete.<li>Click the `Options` icon located in the appropriate template's row. <li>Click the `Delete` option.<li>Click the `Delete` button in the Confirmation Dialog modal to confirm the deletion of the assessment template.|| Template is deleted from OpenDash360™. User remains on the Assessment Manager's `Templates` tab.|![](./assets/images/delete-1.png)

## Publish a Manual Assessment Template
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Mouse over the assessment template you wish to publish.<li>Click the `Options` icon located in the appropriate template's row. <li>Click the `Edit` option.||User is taken to the `Template Details` interface and is placed within the `Overview` tab. |![](./assets/images/publish-manual-assessment-2.png)
2|<li>Review / Edit the name, enter a description, tag with metadata, and update template. <li>Click the `Update` button.||The  `Overview` data is updated and the assessment is in the `Draft` state|![](./assets/images/publish-manual-assessment-3.png)
3|<li>Click the `Hierarchy` sub menu.<li>Review / Edit the hierarchicial levels for the assessment.<li>Click the `Update` button.||Assessment Hierarchy is updated.|![](./assets/images/publish-manual-assessment-5.png)
4|<li>Click the `Scoring` sub menu.<li>Review / Edit the scoring levels for the assessment (using global scoring scheme).<li>Toggle `Impose Global Scoring`.<li>Click the `Update` button.||Assessment Scoring is updated.|![](./assets/images/publish-manual-assessment-7.png)
5|<li>Click the `Builder` tab.<li>Review / Edit assessment objectives by utilizing the assessment hierarchy on the left. For each objective, define the evaluation criteria justifying each scoring level.<li>Click the `Update` button.||The assment objectives and evaluation criteria are updated. |![](./assets/images/publish-manual-assessment-9.png)
6|<li>Click the `Competency Mapping` tab.<li>Select a target framework.<li>Select the desired line item from the assessment and map it to the target framework.<li>Review the mapping below and confirm the mapping. <li>Continue this for each line item that needs to be mapped.<li>Click the `Save` button.||Competencies are successfully mapped.|![](./assets/images/publish-manual-assessment-11.png)
7|<li>Click the `Preview` tab.<li>Review the assessment as if you were delivering the assessment by utilziing the hierarchy on the left.<li>Click one of the `Status` radio buttons.<li>Add an `Optional Comment`. <li>Click the `Add Comment` button. <li>Select the assessment wheel in the bottom left to preview the live graphic.||Status is added to the Assessment. Comment is added.|![](./assets/images/publish-manual-assessment-13.png)
8|<li>Within the header of the assessment, change the assessment status from `Draft` to `Published'.||Assessment is published.|![](./assets/images/publish-manual-assessment-14.png)

## Schedule and Assign an Assessment Template
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>Navigate to Assessment Manager<li>Mouse over the assessment template you wish to schedule.<li>Click the `Options` icon located in the appropriate template's row. <li>Click the `Schedule` option.<li>Enter the `Assessment Delivery Name`, `Start Date`, `Start Time, `End Date` and `End Time`, within the `Schedule Assessment` modal.<li>Click the `Schedule` button located at the bottom of the modal.|| The Assessment Template has been scheduled. The user is redirected to the `Assessment Schedule` page, where they can enter additional details.|![](./assets/images/schedule-1.png)
2|<li>The user is now within the `Overview ` sub menu on the `Assessment Schedule` page. Review the `Overview` data and modify any editable elements.<li>Toggle the `Participant Notifications` option.<li>Click the `Update` button.||The `Overview` data is updated. Template details are displayed in the right column.|![](./assets/images/schedule-2.png)
3|<li>Click the `Assessor` sub menu.<li>Click the `Add Assessors` button located towards the top of the `Assessors Table`. <li>A modal appears and the user can begin typing the Assessor's name within the search field.<li>Select the desired assessor from the table.<li>Repeat this process to continue adding assessors.<li>Click the `Add Assessors` button within the modal to complete the addition.||The assessor(s) is added to the scheduled assessment.|![](./assets/images/schedule-3.png)
4|<li>Click the `Participants` sub menu<li>Click the `Add Participants` button located towards the top of the `Participants Table`.<li>A modal appears and the user can begin typing a participant's name within the search field.<li>Select the desired participants from the table.<li>Repeat this process to continue adding participants.<li>Click the `Add Participants` button within the modal to complete the addition.||The participant(s) is added to the scheduled assessment.|![](./assets/images/schedule-6.png)

## Deliver a Manual Assessment
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|<li>||xxx|![](./assets/xxx.png)
2|<li>||xxx|![](./assets/xxx.png)
3|<li>||xxx|![](./assets/xxx.png)
4|<li>||xxx|![](./assets/xxx.png)
5|<li>||xxx|![](./assets/xxx.png)
6|<li>||xxx|![](./assets/xxx.png)
7|<li>||xxx|![](./assets/xxx.png)
8|<li>||xxx|![](./assets/xxx.png)
9|<li>||xxx|![](./assets/xxx.png)
10|<li>||xxx|![](./assets/xxx.png)
