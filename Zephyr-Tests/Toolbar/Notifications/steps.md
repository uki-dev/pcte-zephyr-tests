# Notifications
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Open Notification Panel:<br><br>Click the "Alert" icon ||The Notification Panel will open.|![](./assets/note1.png)
2|Mark Notification as "Read":<br><br>Click "arrow" located to the right of a notification <br><br>Click the "Mark as Read" button ||The notification is now not featured.|![](./assets/note2.png)
3|Disable Notification Sound:<br><br>Click the "options" icon located in the Notification Panel header. <br><br> Click the "Mute" option.||Notification sound is disabled.|![](./assets/note3.png)
4|Filter Notifications:<br><br>Click the select menu in the Notification Panel Header.<br><br>Choose a desired filter ||The notification list is automatically updated to show the appropriate notifications.|![](./assets/note4.png)
