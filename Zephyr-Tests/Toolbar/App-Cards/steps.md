# Toolbar
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Toolbar Tests:||![]()
1|Launchpad:<br><br>Click launchpad button on far left of toolbar. When the menu opens click on world clock||<br><br>The world clock widget should launch and the launchpad window should close.|![](./assets/launchpad.gif)
2|Return to lobby:<br><br>If on another page apart from the lobby, click the house icon.||<br><br>You will return to the lobby|![](./assets/return_to_lobby.gif)
3|App cards:<br><br>If there are available app cards, click on one||<br><br>You will be navigated to that page|![](./assets/toolbar_appcards.gif)
4|User center:<br><br>Click the button that has your first name ||<br><br>User center will open|![](./assets/user_center.gif)
5|Chat:<br><br>Click the message icon ||<br><br>Chat will open|![](./assets/chat.gif)
6|Notifications:<br><br>Click the bell icon ||<br><br>Notifications will open|![](./assets/notification.gif)

