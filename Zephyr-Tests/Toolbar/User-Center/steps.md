# User Center
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||User Center Tests:||![]()
1|Open user center:<br><br>Click the button that has your first name ||<br><br>User center will open|![](./assets/user_center.gif)
2|View profile:<br><br>Click the view profile button to display user profile, click the return arrow at the top to go back to the main menu ||<br><br>You will navigate to and awar from the view profile option|![](./assets/view_profile.gif)
3|Run speed test:<br><br>Click the run speed test button to display speed test, click the return arrow at the top to go back to the main menu ||<br><br>You will navigate to and awar from the run speed test option|![](./assets/speed_test.gif)
4|Dark/Light mode:<br><br>Click the toggle labeled dark mode to toggle on dark mode, click again to toggle off ||<br><br>Dark mode will toggle on, the label will update. When clicked again dark mode will toggle off and the name will update|![](./assets/dark_mode.gif)
5|Submit feedback:<br><br>Click the submit feedback button, the feedback option will appear. Fill out the form to submit feedback for the current page ||<br><br>You will navigate to the feedback option. After submitting feedback for the page you will get a notification and be navigated back to the main menu. |![](./assets/feedback.gif)
6|Log out:<br><br>Click the log out button ||<br><br>You will log out. |![](./assets/log_out.gif)
