# OpenDash360™ Taskbar - App Launcher
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Selecting an Application:||![]()
1|<li>Click on one of the app card logos located to the right of the  `Home` icon (Located on the left side of the taskbar).||User will be redirected to the selected application.|![](./assets/app-launcher1.png)
||Return to Lobby:||![]()
1|<li>Click on the `Home` icon (Located on the left side of the taskbar).||User will be redirected to the Lobby.|![](./assets/app-launcher1.png)