# Cycle Forwards and Backwards Through Dashboard
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Dashboard Cycle:||![]()
1|Select `Dashboard` from the left-side menu.||Dashboard page loads in right pane|![](./assets/3-dash.png)
2|<li>Click `Right Arrow` icon in top/left corner of the Dashboard.<br><br><li>Click `Left Arrow` icon in top/left corner of the Dashboard.||Right viewing pane cycles through existing Dashboards.|![](./assets/arrows.png)