# OpenDash360™ Dashboard - Adding Widgets
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Add Widget to Dashboard:||![]()
1|<li>Ensure you are in Edit Dashboard mode.<li>Click the `Add Widget` button located at the top of the dashboard.<br><br><li>Click the desired widget once.<br><br><li>Close the Add Widget Menu.<br><br><li>Reposition (Left Click and Drag) and resize (Corner/Sides).<br><br><li>Click `Save Dashboard` Button on the top right.||The selected widget will be displayed on the dashboard.|![](./assets/1.png)
||View Widget Details:||![]()
1|<li>Click the `Add Widget` button located at the top of the dashboard.<br><br><li>Hover mouse over widget card.<br><br><li>Click the `Kebab/Options` icon located in the top-right corner of the widget card. <br><br><li>Click `View Details`<br><br><li>Click away on the dashboard to close the menu.||The widget sidebar will reload, displaying details regarding the selected widget. From here user's can find details on the widget, launch the widget and add the widget to the hotbar.|![](./assets/3.png)
||Free Float Widget:||![]()
1|<li>Click the `Kebab/Options` Menu Icon and click `Undock`.<br><br><li>Reposition with Drag & Drop on the header of the widget.<br><br><li>Resize the widget.||The widget will move to the desired location.|![](./assets/5.png)
||Maximize Widget:||![]()
1|<li>Click the `Kebab/Options` Menu Icon and click `Maximize`.<br><br><li>Click the `Kebab/Options` Menu Icon and click `Float` to un-maximize.||User maximizes a widget under widget control control option, `Maximize`.|![](./assets/8.png)
||Rate Widget:||![]()
1|<li>Click the `Kebab/Options` Menu Icon and click `Rate Widget`.<br><br><li>Fill out the required forms within the Feedback Modal.<br><br><li>Click the `Submit` button.||OpenDash360™ administrators are sent the widget's feedback through the Feedback Manager microapplication.|![](./assets/9.png)
||Close Widget:||![]()
7|<li>Click the `Kebab/Options` Menu Icon and click `Close`.||User closes an undocked widget under widget  control option, `Close`|![](./assets/10.png)