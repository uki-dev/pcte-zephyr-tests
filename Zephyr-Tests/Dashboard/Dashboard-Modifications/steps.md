# Creating, Editing, Deleting Dashboards
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Creating Dashboard:||![]()
1|Access OpenDash360™:<br><br><li>Open a browser window and type in the following URL:<br><br><li>(For Test Zones) https://opendash.pcte.mil<br><br><li>(For Production) https://rcs01-dash.pcte.mil/<br><br><li>Once the page loads, select the `Login` button and login with your portal credentials for the current zone|If in a test zone, be sure to be on the F5 VPN before logging into the opendash website.|The OpenDash360™ landing page will load in your browser.|![](./assets/dash-new-1.png)
2|<li>Click the `Options` icon located next to the Dashboard title.<br><br><li>Select the `Create Dashboard` option.<br><br><li>Add a title to the New Dashboard modal. <br><br><li>Add a Description to the New Dashboard modal.||A modal opens to input a dashboard's `Name` and `Description.`|![](./assets/dash-new-2.png)
3|Add Widget:<br><br><li>Click the `Add Widget` button<br><br><li>Add any desired widgets to Dashboard by clicking the widget's card.<br><br><li>Click the `Close` button to see your new widget||Widget selected is added.|![](./assets/6.png)
4|Save Dashboard:<br><br><li>Click the `Save Dashboard` button.||New Dashboard is Created and Opened.|![](./assets/7.png)
||Set Dashboard As Default:||![]()
1|Set Dashboard As Default:<br><br><li>Click the `Options` icon<li>Select the `Set as Default` option from the select menu.||When a user refreshes the page the new `Default Dashboard` will open by default.|![](./assets/dash-new-2.png)
||Edit Dashboard:||![]()
1|Edit Dashboard:<br><br><li>Click `Options` icon <br><br><li>Select `Edit Dashboard`.||Dashboard opens in right pane for editing|![](./assets/dash-new-2.png)
2|Make any desired edits to the dashboard<br><br><li>Click the `Save` button in the upper right-hand corner.||The Dashboard will reload with the edits displaying|![](./assets/5-edit.png)
||Delete Dashboard:||![]()
1|<li>Click the `Options` icon<br><br><li>Select the `Edit Dashboard` option.<br><br><li>Click the `Trash` icon and confirm deletion within modal that appears.||Dashboard is deleted.|![](./assets/dashboard-delete.png)
||Delete All Dashboards to Show Default Dashboard:||![]()
1|<li>Click the `Options` icon<br><br><li>Select the `Edit Dashboard` option.<br><br><li>Click the `Trash` icon and confirm deletion within modal that appears.||Dashboard is deleted.|![](./assets/dashboard-delete.png)
2|<li>Repeat this process to delete all dashboards.||A new dashboard will load. It is titled, "Standard User Dashboard". This is the default OpenDash360™ dashboard.|![](./assets/dashboard-default.png)
