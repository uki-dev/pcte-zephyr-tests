# SMTP Add Outgoing Mail Server to JSD
## Steps
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
||Add Outgoing Mail Server to JSD:||![]()
1|Log into helpdesk.pcte.mil with JSD admin role.<br><br><li>Click Gear icon<li>Click System||This should open the system admin console window within JSD|![](./assets/1.png)
2|Scroll down on left side nav menu to the Mail header:<br><br><li>Click `Outgoing Mail`||This should take you to the SMTP settings page|![](./assets/2.png)
3|Click on `Configure new SMTP mail server`||This should open a form UI to fill out.|![](./assets/3.png)
4|Using provided information fill out the form for the follow fields:<br><br><li>Name<li>Description<li>From address<li>Email prefix<li>Service Provider<li>Protocol<li>Host Name<li>SMTP Port<br><br>Click `Add` when finished.||This should take you back to the previous screen with an entry for your newly added outgoing server.|![](./assets/4.png)
5|In order to test the new outgoing mail server:<br><br><li>Click on `Test Email`.||This should open an email preview screen for you to complete.|![](./assets/5.png)