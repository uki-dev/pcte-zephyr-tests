## Login Process
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Click the `Login` Button||User is redirected to the `Login` form.|![](./assets/1.png)
2|The user must now enter their username and password. <br><br>Once this is done, the user must click the Login button.||User is redirected to the OpenDash360™ `Lobby`.|![](./assets/2.png)

## Confirm DoD Banner 
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|Land on landing page||User will see DoD banner on the right side of the page.|![](./assets/1.png)
2|Refresh Page.||User will see DoD banner on the right side of the page.|![](./assets/1.png)

##  Password Reset
Step|Actions|Data|Result|Image
:-|:-|:-|:-|:-:
1|From the `Login` form, click the `Forgot Password` link.||User is redirected to the `Self Service Account Management` form.|![](./assets/4.png)
2|Fill out the email address of the desired account.<br><br>Click the `Request Password Reset` Button.||An email is sent to the user with directions on resetting password. User is also redirected back to the `Login` form.|![](./assets/5.png)