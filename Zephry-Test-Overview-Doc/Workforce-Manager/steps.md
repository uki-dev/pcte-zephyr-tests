# Workforce Manager

## Navigate to Workforce Manager

---

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for Workforce Manager or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/workforce-manager.png)

Once opening the microapplication you will see it display.

![](../Full/assets/images/workforce-2.png)

---

## View People in an Organization

---

### Step 1: Expand Organization Hierarchy

When a user launches the *Workforce Manager* microapplication, they will be presented with an interface with various tabs in the header. Additionally, users will see a _Right Arrow_ icon located in the upper left corner of the interface.

- Click that _Right Arrow_ icon to expand a panel that contains an organizational hierarchy.

  ![](../Full/assets/images/workforce-manager-arrow.png)



### Step 2: Select Organization

Organizations are displayed in hierarchy format. In order to show an organization’s child organizations, a user simply needs to click the _Right Arrow_ icon that is located to the left of the parent organization.

![](../Full/assets/images/workforce-manager-drop-down.png)

![](../Full/assets/images/workforce-1.png)

Doing this transforms the *Right Arrow* icon into a *Down Arrow* icon and then displays every direct child organization.

- Click the name of an organization to close the panel and produce a layout that contains the organization’s information.


### Step 3: Click _People_ Tab

Once an organization is selected, users will see a tab layout at the top of the screen.

- Click the _People_ tab. Doing this will display a table that contains all of the people associated with that particular organization.

  ![](../Full/assets/images/workforce-people-role.png)

## Assign People to a Position

---

### Step 1: Select a Position

After an organization is selected, user will see the following tabs at the top of the screen:

- Positions

- People

- Activity

Users must click the _Positions_ tab.

![](../Full/assets/images/workforce-position-tab.png)

![](../Full/assets/images/workforce-3.png)


### Step 2: Click the _People_ Tab

The _Positions_ tab initially shows all of the requirements of the positions. To add users to the position, users must click the _People_ tab.

![](../Full/assets/images/workforce-manager-people.png)

![](../Full/assets/images/workforce-4.png)


### Step 3: Click _Add User_ Button

Now, that the user is on the _People_ tab, the user must click the _Add User_ button located above the table. Clicking this button generates a modal that contains an *Add Users* form.

![](../Full/assets/images/workforce-people-add.png)

![](../Full/assets/images/image9.png)

### Step 4: Add Users

The _Add Users_ form contains a search form that allows for a selection of people. As the user begins typing into the search field, a drop down menu will display providing results based on the search criteria. 

Multiple users can be selected.

Click the _Submit_ button to close the modal and add the user(s).

![](../Full/assets/images/workforce-people-submit.png)

## Navigate to a Person’s Training Jacket

---

### Step 1: Click the *People* Tab

After an organization is selected, user will see the following tabs at the top of the screen:

- Positions

- People

- Activity

Users must click the _People_ tab.

![](../Full/assets/images/workforce-5.png)

### Step 2: Launch the User's _Training Jacket_

Clicking the _People_ tab will produce a table that contains all of an organization’s members.

- Click the _Options_ icon located on the right side of the desired user's row. Doing this will produce a drop down menu.

- Click the _View Training Jacket_ option. Doing this will launch the _Training Jacket_ application, were the user's profile will be the landing page.


## View Positions in an Organization

---

### Step 1: Expand Organization Hierarchy

When a user launches the *Workforce Manager* microapplication, they will be presented with an interface with various tabs in the header. Additionally, users will see a _Right Arrow_ icon located in the upper left corner of the interface.

Users must click that icon to expand a panel that contains an organizational hierarchy.


### Step 2: Select Organization

Organizations are displayed in hierarchy format. In order to show an organization’s child organizations, a user simply needs to click the _Right Arrow_ icon that is located to the left of the parent organization.

![](../Full/assets/images/image104.png)

Doing this transforms the *Right Arrow* icon into a *Down Arrow* icon and then displays every direct child organization.

After selecting an organization, the left panel will close and users will see that the organization’s positions have been loaded.

## Create a Position

---

### Step 1: Click _Create Position_ Button

After an organization is selected, user will see the following tabs at the top of the screen:

- Positions

- People

- Activity

Users must click the _Create Position_ button that is located above the table. Click this will load a modal with a _Create Position_ form.

![](../Full/assets/images/image78.png)


### Step 2: Complete *Create Position* Form

Users must fill out the *Create Position* form and click the *Submit* button. Doing this will close the modal and create a new position within the organization.

## Delete a Position

---

### Step 1: Click _Option_ Icon

From the organization’s *Position* tab, users must hover over the desired position’s row. Doing this will generate an _Options_ icon on the right side of the row. \* \*

![](../Full/assets/images/image7.png)

Users must click the _Options_ icon. Doing this produces a popup menu with the following options:

- Edit

- Duplicate

- Delete


### Step 2: Click _Delete_ Option

Users must click the *Delete* option. Doing this will immediately remove the position from the organization.

## Edit Position’s Title

---

From the organization’s *Position* tab, users must hover over the desired position’s row. Doing this will generate an _Options_ icon on the right side of the row. \* \*

![](../Full/assets/images/image7.png)

Users must click the _Options_ icon. Doing this produces a popup menu with the following options:

- Edit

- Duplicate

- Delete

### Step 1: Click _Edit_ Option

Users must click the *Edit* option. Doing this will produce a modal that contains an _Edit Position_ form.

![](../Full/assets/images/image70.png)


### Step 2: Update *Edit Position* Form

Users must update the _Edit Position_ form with the desired information. Once complete, the user must click the *Submit *button at the bottom of the modal. Doing this will immediately remove the modal and update the position with the new details.

## Add/Remove a Position’s Requirements

---

### Step 1: Click Position’s Title

From the organization’s *Position* tab, a user must click on the desired position’s name.

![](../Full/assets/images/image116.png)


### Step 2: Click _Add Requirement_ Button

The user must click the _Add Requirement_ button located above the table containing all of the position’s requirements. A modal will appear containing an _Add Requirement_ form.


### Step 3: Fill Out _Add Requirement_ Form

A user must complete the entire _Add Requirement_ form and click the submit button. Doing this will remove the modal and add the new requirement.


### Step 4: Remove Requirement

In order to remove the requirement, the user must hover over the requirement’s row. Doing this will produce a *Trash* icon on the right side of the page.

The user must click this icon to delete the requirement from the position.

## View People in a Position

---

### Step 1: Click Position’s Title

After an organization is selected, user will see the following tabs at the top of the screen:

- Positions

- People

- Activity

Users must click the _Position_ tab.

![](../Full/assets/images/image60.png)


### Step 2: Click _People_ Tab

From the organization’s *Position* interface, the user will want to click the _People_ tab located in the header of the page. Doing this will display a table full of people that are currently in the position.
