# Dashboard Manager

## Add Event Collection

---

### Step 1: Access Dashboard Manager

Users must first open the *Dashboard Manager* microapplication.

### Step 2: Add Event Collection Button

Users will find an _Add Event Collection_ button located above the *Event Collections *table. Clicking this button launches a modal which contains a form that needs to be completed.

![](../Full/assets/images/event-collection.png)

### Step 3: Complete Event Collection Form

Fill out the following fields:

- Event Collection Name 
- Event ID

Click the _Add Event Collection_ button.

![](../Full/assets/images/image30.png)

Clicking this button removes the modal and the new event collection will be created.

## Delete Event Collection

---

### Step 1: Click Options Icon

From the *Event Collection* landing page, users need to:

- Hover over the desired event collection’s row. 

Doing this will make an *Option* icon on the far right side of the event collection’s row.

![](../Full/assets/images/event-collection-2.png)

- Click the _Options_ icon. Doing this will produce a popup menu.
- Click _Delete._


## Duplicate Dashboard Template

---

### Step 1: Locate Dashboard

From the Dashboard Template page, users must locate and hover over the desired dashboard’s row (within the Dashboard Template’s table).

### Step 2: Click Options Icon

Hovering over the template’s row will produce an Options icon on the far right-hand side of the table.

Clicking this icon will produce a dropdown menu with the following options:

-   View
-   Edit
-   Disable
-   Duplicate
-   Delete
    
### Step 3: Click Duplicate

![](../Full/assets/images/event-collection-3.png)

Clicking the Duplicate option from the popup menu will instantly clone the dashboard template and add the new dashboard to the Dashboard Template’s table.

## Add Dashboard Template to Event Collection

---

### Step 1: Select Event Collection

From the *Event Collection* landing page, users must click the desired event collection’s name. This will load a table containing all of the event collection’s dashboard templates.

![](../Full/assets/images/event-collection-4.png)

### Step 2: Click Create Dashboard Button

Click the _Create Dashboard_ button, located above the *Dashboard Template* table.

![](../Full/assets/images/image41.png)


### Step 3: Complete New Template Form

Fill out the _Dashboard Template Name_ field and select the desired _Team_ that will be receiving the dashboard.

Once the user clicks the *Create Template* button, the modal will be removed and the user will be redirected to the *Edit Template* interface.

## Edit Dashboard Template Settings

---

### Step 1: Select Dashboard

![](../Full/assets/images/event-collection-5.png)

From the *Event Collection’s Dashboard* table, the user must click the desired template’s title.

### Step 2: Click the Settings Icon

The user needs to click the _Settings_ icon located on the right side of the page. Doing this launches a panel out of the right side of the page. This panel contains all of the editable settings for the dashboard template.

![](../Full/assets/images/image46.png)

### Step 3: Update Template Settings

Once the user updates the _Template Settings_ form, they must click the *Save Settingsbutton located at the bottom of the form. Doing this closes the _Template Settings panel and updates the template._

## Add Widget to Dashboard Template

---

### Step 1: Select Dashboard

After selecting the appropriate dashboard, the user will land on the *Dashboard Editor* interface.

### Step 2: Click Widget Card

From the *Dashboard Editor* interface, users will see a list of available widgets located on the left side of the page.

![](../Full/assets/images/image123.png)

Users simply need to click the desired widget’s card to add it to the dashboard. These widgets can be resized and moved by dragging and dropping the corners of the widget.

## Delete Dashboard Template

---

### Step 1: Locate Dashboard

From the _Dashboard Template page, users must locate and hover over the desired dashboard’s row (within the Dashboard Template’s table)._

### Step 2: Click Options Icon

Hovering over the template’s row will produce an _Options_ icon on the far right-hand side of the table.

![](../Full/assets/images/image82.png)

Clicking this icon will produce a dropdown menu with the following options:

- View
- Edit
- Disable
- Duplicate
- Delete

### Step 3: Click Delete

Clicking the _Delete_ option from the popup menu will instantly delete the dashboard template.

## Duplicate Dashboard Template

---

### Step 1: Locate Dashboard

From the Dashboard Template page, users must locate and hover over the desired dashboard’s row (within the Dashboard Template’s table).

### Step 2: Click Options Icon

Hovering over the template’s row will produce an Options icon on the far right-hand side of the table.

Clicking this icon will produce a dropdown menu with the following options:

-   View
-   Edit
-   Disable
-   Duplicate
-   Delete
    
### Step 3: Click Duplicate

Clicking the Duplicate option from the popup menu will instantly clone the dashboard template and add the new dashboard to the Dashboard Template’s table.