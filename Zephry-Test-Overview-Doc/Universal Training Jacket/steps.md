# Universal Training Jacket

The Universal Training Jacket is a microapplication for users to be able to check their progress towards their completion of competencies and assessments. Here the user can see completed and and scheduled assessments. 

They can see which competencies that are required and which competency framework they apply to. 

Note: Please complete utilzie the _Assessment Manager_ microapplication before testing the _Universal Training Jacket_. 

Within the _Assessment Manager_ microapplication, please make sure the user that you intend on viewing (within the UTJ) a _Participant_ (within the Assessment Manager).

Data is fed from the _Assessment Manager_ microapplication to the _Universal Training Jacket_  to allow administrators to see a participant's completed assessments and earned competenies.

## Navigate to the Universal Training Jacket

### Navigate to User Manager

---

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for User Manager or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/user-manager-navigation.png)


---

### Search Users

---

#### Step 1: Locate Search Bar 

Users must first locate the search bar located at the top of the user’s table.

![](../Full/assets/images/user-manager-search.png)

![](../Full/assets/images/user-manager-new-2.png)


#### Step 2: Begin Typing

The user must begin typing the name or username of the user that they wish to find. The _User Manager_ table will automatically begin adjusting based on the search criteria.
The information found in the Universal Training Jacket may not be accurate because it is still in development.

#### Step 3: Options - View User Training Jacket

Click on the three dot bubble on the far-right to see the drop down to select the 'View Training Jacket'.

![](../Full/assets/images/user-training-jacket-dropdown.png)

This will allow the user to open the user training jacket microapplication for the specific user.

![](../Full/assets/images/universal-training-jacket.png)

---
## Achievements Tab

The achievements tab contains a vertical tab layout with the following information:

- Overview (Static Data)
- Competencies
- Assignments
- Badges (Static Data)

### Overview

#### Achievements Overview 

The _Achievements Overview_ page is currently static data.

### Competencies

The _Competencies_ tab shows all the competencies that the user has achieved and what framework they are from. 

   ![](../Full/assets/images/universal-training-jacket-compentencies.png)

This data is pulled from the _Assessment Manager_ microapplication. Administrators have the ability to view the participant of an assessment's data here.

#### Search

- A search bar is located at the top of the competency table.

   ![](../Full/assets/images/universal-training-jacket-search-1.png)

- Search by name for competencies using the search bar.

   ![](../Full/assets/images/universal-training-jacket-search.png)
   
#### Sort

- Sort by Competency or Framework Ascending or Descending using the arrow to the right of the column header

    ![](../Full/assets/images/universal-training-jacket-competency-arrow.png)
    ![](../Full/assets/images/universal-training-jacket-framework-arrow.png)
    
Applying these sorting filters will allow the user to interact with large lists of competencies that can span several pages.

---

### Assessments

The _Assessments_ tab contains all the scheduled and completed assessments of a user. This information is pulled from the _Assessment Manager_ microapplication.

![](../Full/assets/images/universal-training-jacket-assessment.png)

#### Search

- A search bar is located at the top of the modal above the competencies

  ![](../Full/assets/images/universal-training-jacket-assessment-search.png)
  
- Search by name for competencies using the search bar

  ![](../Full/assets/images/universal-training-jacket-assessment-search-1.png)
  
  
#### Sort

- Sort by Assessment or Template Ascending or Descending using the arrow to the right of the column header

- Applying these sorting filters will allow the user to interact with large lists of assessments that can span several pages.

  ![](../Full/assets/images/universal-training-jacket-assessment-sort.png)
  
---

### Badges

The _Badges_- tab is not currently operational.

![](../Full/assets/images/universal-training-jacket-badges.png)


---

## Gap Analysis

The _Gap Analysis_ tab is not currently operational.

## Training Plan 

The _Training Plan_ tab is not currently operational.

## History

The _History_ tab is not currently operational.