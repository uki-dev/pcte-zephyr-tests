# Dashboard

## Access Dashboard

---

### Step 1: Lobby Page

After  accessing the OpenDash360™ environment, users will arrive on the Lobby page. 

- Click the _OpenDash360™_ app card, located beneath the _Persistent Cyber Training Environment_ description.

![](../Full/assets/images/dash-1.png)

Clicking this app card will redirect the user to the Dashboard interface.

![](../Full/assets/images/dash-2.png)


## Creating Dashboards

---

### Step 1: Lobby Page

From the Dashboard interface:

- Click the _Options_ icon located to the right of the Dashboard title.
- Click the _Create Dashboard_ option.

![](../Full/assets/images/dash-3.png)


### Step 2: Dashboard Details Modal

A modal will appears that contains the following fields:

- Dashboard Name

- Dashboard Description

![](../Full/assets/images/dash-4.png)


The user must fill out these fields and click the _Update Dashboard_ button. Doing this will close the modal and take the user to the _Dashboard Canvas_.


![](../Full/assets/images/image97.png)

## Modifying Dashboards

---

### Step 1: Adding a Widget

From the _Dashboard Canvas_, click the _Add Widget_ button located at the top of the page. Doing this will open the widget panel from the left side of the page.

![](../Full/assets/images/dash-5.png)

- Click the widget’s card to add the widget to the current dashboard

_Note: The widget panel remains open after clicking a widget, so multiple widgets can be added at a time._

![](../Full/assets/images/image112.png)

When all of the desired widgets have been added to the dashboard, the user can click the area to the right of the widget panel to close the widget panel.

These widgets can be resized and moved by dragging and dropping the corners of the widget.

### Step 2: Save Dashboard

Click the _Save_ button located in the upper right-hand corner of the page. Doing this will redirect the user to the non-editable version of the dashboard.

![](../Full/assets/images/dash-7.png)

## Deleting Dashboards

---

### Step 1: Dashboard Options

From the Dashboard interface (below):

- Click the _Options_ icon located to the right of the Dashboard title.
- Click the _Edit_ option.


![](../Full/assets/images/dash-3.png)

### Step 2: Delete Dashboard

Next, users will want to click the _Trash_ icon located to the left of the _Add Widget_ button.

_Note: Users need more then one dashboard in order to use the delete function._

![](../Full/assets/images/dash-8.png)

Doing this will launch a modal asking for confirmation. 

![](../Full/assets/images/dash-9.png)


## Set Dashboard As Default

---

### Step 1: Dashboard Options

From the Dashboard interface (below):

- Click the _Options_ icon located to the right of the Dashboard title.
- Click the targeted dasboard's name.

![](../Full/assets/images/dash-10.png)

 Doing this will load the targeted dashboard.3

 - Click the same _Options_ icon located to the right of the Dashboard title again
 - Click the _Set as Default_ option

 ![](../Full/assets/images/dash-11.png)

After doing this, users will be able to refresh the page and find that the dashboard is now the default dashboard.

## Toggle Between Dashboards

---

### Step 1: Dashboard Options

From the main Dashboard UI (see below), users will find two arrows located to the left of the Dashboard’s title.

![](../Full/assets/images/dash-12.png)

Clicking these arrows will change the dashboard that is being viewed.