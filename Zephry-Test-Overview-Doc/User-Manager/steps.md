# User Manager

## Navigate to User Manager

---

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for User Manager or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/user-manager-navigation.png)


![](../Full/assets/images/user-manager-new-1.png)

## Search Users

---

### Step 1: Locate Search Bar 

Users must first locate the search bar located at the top of the user’s table.

![](../Full/assets/images/user-manager-search.png)

![](../Full/assets/images/user-manager-new-2.png)


### Step 2: Begin Typing

The user must begin typing the name or username of the user that they wish to find. The _User Manager_ table will automatically begin adjusting based on the search criteria.

## Filter User Columns

---

### Step 1: Click _Filter_ Icon

Locate and click the *Filter* icon located to the right of one of the table headers (Example: First Name). Doing this will launch a popup menu that contains a single search field.

![](../Full/assets/images/user-manager-new-3.png)

### Step 2: Begin Typing Desired Filter

Within this search field, the user must begin typing the desired search criteria. As the user types, the table will automatically update based on the search criteria.

## Access User Profile

---

### Step 1: Click User’s Name

Once an administrator finds the desired user they wish to view, they must:

- Click the user’s username. Doing this will redirect the administrator to the user’s profile.

![](../Full/assets/images/user-manager-new-4.png)


## Add Secondary Email

---

### Step 1: Access User’s Profile

Once an administrator finds the desired user they wish to edit, they must click the user’s username. Doing this will redirect the administrator to the user’s profile.

![](../Full/assets/images/image36.png)



### Step 2: Click _Add Alternate Email_ Button

- Click the _Add Alternate Email_ button located on the left side of the page. Doing this will generate a text field above the button.


### Step 3: Type Alternate Email

-Type the alternate email into the text field.


### Step 4: Click the _Save_ Icon

-Click the _Save_ icon located to the right of the _Alternate Email’s_ heading. Doing this will instantly save the new email address.

   ![](../Full/assets/images/save-icon.png)

## Change Password

---

### Step 1: Access User’s Profile

Once an administrator finds the desired user they wish to edit, they must click the user’s username. Doing this will redirect the administrator to the user’s profile.

### Step 2: Type New Password

The user will see two fields beneath the _Update Password_ heading that need to be filled out.

- New Password

- Confirm New Password

### Step 3: Click the _Save_ Icon

-Click the _Save_ icon located to the right of the _Update Password’s_ heading. Doing this will instantly save the new password.

   ![](../Full/assets/images/save-icon.png)

_Note: Passwords must contain:_


- _1 Lowercase_
- _1 Uppercase_
- _1 Special Character_
- _1 Number_
- _15 Total Characters_


## Add / Remove User’s Role

---

### Step 1: Access Security & Access Tab

-Click on the _Security & Access_ tab located in the header of the page. Doing this will refresh the page and display various OpenDash360™ roles.

### Step 2: Click Desired Role

- Locate a Greyed Out role and hover over it. Doing this will change the background color to blue and will add a Plus icon.

- Click this role. Doing this will add the user to this role.

![](../Full/assets/images/user-manager-1.png)

### Step 3: Refresh Page & Reopen User Manager Application

To confirm the user has been given this role, the user will want to refresh the browser and navigate back to the User Manager microapplication (and specifically to the Security & Access tab for the same user).

The user will see that the role is still applied.

### Step 4: Click Desired Role

Now the user will want to hover over the role that was added previously. Doing this will add an X icon, which signifies that the role will be removed when clicked.

![](../Full/assets/images/user-manager-2.png)

Click this role. Doing this will remove the user from the role.

## View User's Timeline

---

### Step 1: Click the _Timeline_ Tab

A user's timeline will show. 

- Utilize the search field to search the event log. 
- Click the _Arrow_ icon next to the search bar. Doing this will change the order of the event log.


## Search Organizations Hierarchy Tree

---

### Step 1: Navigate to _Organizations_ Tab

From the landing page of the *User Manager* microapplication, click the _Organizations_ tab located at the top of the page.

![](../Full/assets/images/user-manager-new-5.png)

### Step 2: Utilize Search Field

The _Organizations_ tab contains a two column layout. The left column consists of a hierarchy or parent / child organizations. To search organizations within the hierarchy, users must use the search field located at the top of the hierarchy.

- Begin typing the desired organization and the hierarchy will update accordingly.

![](../Full/assets/images/user-manager-new-6.png)


## Display Organization’s Children (Hierarchy)

---

### Step 1: Click the _Right Arrow_ Icon

In order to show an organization’s child organizations, a user simply needs to click the _Right Arrow_ icon that is located to the left of the parent organization.

![](../Full/assets/images/user-manager-new-8.png)


Doing this transforms the *Right Arrow* icon into a *Down Arrow* icon and then displays every direct child organization.

## View Organization’s Details

---

### Step 1: Click Organization’s Name

![](../Full/assets/images/user-manager-new-7.png)

Clicking an organization’s name displays the organization’s details on the right side of the page. The following information can be found about each organization:

- Users
- Details
- Analytics (Disabled)
- History (Disabled)

## Update Organization's Logo

---

Once an organization has been selected, the user will see the  organization’s details on the right side of the page. 

### Step 1: Click _Details_ Tab

![](../Full/assets/images/user-manager-new-9.png)

- Click the _Details_ tab located beneath the organization's name. Doing this will load the _Details_ interface, which contains a _Name_ field and a _File Upload_ button.

### Step 2: Upload Image

- Click the _Choose File_ button and select a logo from your computer. The name of the image will display on the right side of the button and the _Update Details_ button will become active.

- Click the _Update Details_ button to finish uploaded the organization's logo.


## View Student's _Training Jacket_

---

Once an organization has been selected, the user will see the  organization’s details on the right side of the page. 

### Step 1: Click _Options_ Icon

From the _Users_ tab within the organization's details page:

- Hover over the desired user's row. Doing this will produce an _Options_ icon on the right side of the user's row.
- Click the _Options_ icon. Doing this will generate a dropdown menu.

![](../Full/assets/images/user-manager-new-10.png)

### Step 2: Click _Training Jacket_

- Click the _Training Jacket_ option.  Doing that will close the dropdown menu and open the user's profile within the _Training Jacket_ application.


## Access Trusted Agents

---

### Step 1: Click _Trusted Agents_ Tab

From the landing page of the *User Manager* microapplication, the user will need to click the _Trusted Agents_ tab located at the top of the page.

![](../Full/assets/images/user-manager-new-11.png)

_Note: Sponsored Accounts and Trusted Agents can be enabled/disabled within the _Config_ tab of the _User Manager_ microapplication._

## Search Trusted Agents

---

### Step 1: Begin Typing in Search Field

Users must first locate the search bar located at the top of the user’s table. The user must begin typing the name or username of the user that they wish to find.

![](../Full/assets/images/user-manager-new-12.png)

The table will automatically begin adjusting based on the search criteria submitted.

## Add Trusted Agent

---

### Step 1: Click _Add Trusted Agent_ Button

Users must click the *Add Trusted Agent* button located above the trusted agent’s table. Clicking this button produces a modal with an _Add Trusted Agent_’s form.

![](../Full/assets/images/image24.png)



### Step 2: Complete _Add Trusted Agent_ Form

The user must fill out the _Username_ field. As the user is typing the input field will produce available results based on the user’s criteria.



### Step 3: Click _Create Endorsement_ Button

Once the desired user is selected, the user must click the *Create Endorsement* button located at the bottom of the modal. Doing this will remove the modal and the user will now be considered a trusted agent.

## Revoke Trusted Agent’s Authority

---

### Step 1: Hover Over User’s Row

The _Trusted Agents_ tab contains a table that lists all of the available trusted agents. In order to revoke one of the user’s authority, the user must first hover over the targeted user’s row. Doing this will display an *X* icon on the far right side of the row.

![](../Full/assets/images/image101.png)



### Step 2: Click the _X_ Icon

Clicking the *X* icon on the far right side of the user’s row will instantly remove the user’s trusted agent capabilities.







## Change Trusted Agent's Organization

---

### Step 1: Click Trusted Agent's Name

From the _Trusted Agents_ tab:

- Click the desired trusted agent's username. Doing this loads the trusted agent's details page.

![](../Full/assets/images/user-manager-new-13.png)

### Step 2: Select Organizations

Users will need to select the desired organizations from the left side of the page. The goal is to send the organizations to the right side of the page.

- Click the checkbox next to the desired organizations.

### Step 3: Add / Save the Organizations

- Click the _Right Arrow_ located between the two columns. Doing this will add the organizations to the right side of the page. Once this is done, the trusted agent the ability to add users to that particular organizations.




## View Trusted Agent's List of Sponsorships

---

### Step 1: Click Trusted Agent's Name

From the _Trusted Agents_ tab:

- Click the desired trusted agent's username. Doing this loads the trusted agent's details page.

![](../Full/assets/images/user-manager-new-13.png)


### Step 2: Click the _Sponsorships_ Tab

From the trusted agent's details page, click the _Sponsorships_ tab located in the header of the page. Doing this will load a table containing all of the users that the trusted agent has sponsored.


## Access Sponsored Accounts Interface

![](../Full/assets/images/image47.png)

_Note: Sponsored Accounts and Trusted Agents can be enabled/disabled within the _Config_ tab of the _User Manager_ microapplication._

---

### Step 1: Click _Sponsored Accounts_ Tab

From the landing page of the *User Manager* microapplication, the user will need to click the _Sponsored Accounts_ tab located at the top of the page.

## Add Sponsored Accounts

---

### Step 1: Click _Add Sponsored Account_ Button

Users must click the *Add Sponsored Account* button located above the sponsored account’s table. Clicking this button produces a modal with an _Add Sponsored Account’s_ form.

![](../Full/assets/images/image115.png)



### Step 2: Complete _Add Sponsored Account_ Form

The user must fill out the _entire form_, including the *Activation Date* and the _Expiration Date_.



### Step 3: Click _Submit_ Button

Once the entire form is filled out, the user must click the *Submit* button located at the bottom of the modal. Doing this will remove the modal and create the new user.



### Step 4: Navigate to the Platform Settings Tab

Click to access the *Platform Settings*  tab then click the *Data Sync*  vertical tab. Click the *Sync Data*  button to finish syncing the users. By doing this, the newly created sponsored accounts will appear in the *Sponsored Accounts*  interface.

## View Sponsored Account’s Details

---

### Step 1: Click Sponsored Account’s Username

From the _Sponsored Accounts_ tab, users will see a table that shows every sponsored account that has been created.

To view a sponsored account’s details, simply click the desired user’s username.

## Disable Sponsored Account

---

### Step 1: Click the *Options* Icon

From the _Sponsored Accounts_ tab, users will see a table that shows every sponsored account that has been created.

To disable a sponsored account, the user must first hover over the desired user’s row. Doing this will produce an *Options* icon on the right side of the user’s row.



### Step 2: Click the *Options* Icon

Click the _Options_ icon to produce a popup menu which includes:

- Edit

- Enable

- Disable

![](../Full/assets/images/options-user-manager.png)

### Step 3: Click the *Disable* Option

Click the _Disable_ option. Doing this instantly disables the user. The user will remain in the *Sponsored Account’s* table and can be re-enabled by using this same process.






## Change Sync Details

---

### Click _Platform Settings_ Tab

From the landing page of the _User Manager_ application, click the _Platform Settings_ tab located in the header of the page. Doing this will load the _Password Policies_ page.

![](../Full/assets/images/user-manager-new-14.png)

### Click _Platform Settings_ Tab

- Click the _Data Sync_ tab located on the left side of the page. Doing this loads the _Data Sync_ interface.

![](../Full/assets/images/user-manager-new-15.png)

### Update Settings

- Toggle _On_ the first field to enable the _Autosync_ feature.
- Click the select box to update the amount of delay between automatic sync(s).
- Click the green _Check_ icon located to the right of the select box. Doing this saves the datasync details.


## Manually Sync User Data

- From the _Data Sync_ interface, click the _Trigger Sync_ button. This will start the process to sync the platform's user data. The status of the sync is shown to the left of the _Trigger Sync_ button. The status will change to a _Check_ when the sync is finished.

_Note: This process may take several minutes._

## Download the Manual Sync's Log

After completing a manual _Data Sync_:

- Click the _Download Log_ button located at the bottom of the interface. Doing this downloads the file (.txt file).