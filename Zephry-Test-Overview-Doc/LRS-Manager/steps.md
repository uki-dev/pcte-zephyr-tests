# LRS Manager

The tLRS and the aLRS sections of the LRS manager are currently pulling data from the _Assessment Manager_ microapplication.

 The _Statement Processor_, _Audit Logs_ and _Load_ sections are currently inactive.

## tLRS Tab

---

![](../Full/assets/images/lrs-manager-1.png)


This shows all the statement IDs for the tLRS. It is currently tied to the _Assessment Manager_. To test out the _LRS Manager_, please follow the _Assesment Manager_ guide to complete an assesment.

After completing an assesment, the _LRS Manager_ will have statements that can be tested.

The tLRS table includes the following information:

- Statement ID
- Actor
- Verb
- Activity
- Client (Inactive)
- Timestamp

Search for records using the search LRS Records search bar.

![](../Full/assets/images/search-tlrs.png)

To view a statement ID hover of the right side to reveal the _eye_ icon to view details. And click on the _eye_ icon.

![](../Full/assets/images/eye-icon.png)

This will bring up the record detail modal to see the details of the specific statement record.

The details include:

- Activity Id
- Actor
- Verb
- Activity
- Client (Inactive)
- Timestamp

    ![](../Full/assets/images/record-detail.png)

To copy the JSON press on the _Copy JSON_ button.

![](../Full/assets/images/copy-json.png)

Once clicked the _JSON copied_ notification will display showing the JSON has been copied to clipboard.

![](../Full/assets/images/copy-json-notification.png)

Click on the _X_ icon to exit the Record Detail modal.


## aLRS Tab

---

![](../Full/assets/images/lrs-manager-2.png)

This shows all the statement IDs for the aLRS. 

This includes all the information for each client ID:

- Statement ID
- Actor
- Verb
- Activity
- Client (Inactive)
- Timestamp

Search for records using the search LRS Records search bar.

![](../Full/assets/images/search-tlrs.png)

To view a statement ID hover of the right side to reveal the _eye_ icon to view details. And click on the _eye_ icon.

![](../Full/assets/images/eye-icon.png)

This will bring up the record detail modal to see the details of the specific statement record.

The details include:

- Activity Id
- Actor
- Verb
- Activity
- Client (Inactive)
- Timestamp

    ![](../Full/assets/images/record-detail.png)

To copy the JSON press on the _Copy JSON_ button.

![](../Full/assets/images/copy-json.png)

Once clicked the _JSON copied_ notification will display showing the JSON has been copied to clipboard.

![](../Full/assets/images/copy-json-notification.png)

Click on the _X_ icon to exit the Record Detail modal.

## Statement Processor Tab

---

The user interface will show "No Items". 

![](../Full/assets/images/lrs-manager-3.png)

## Audit Logs Tab

---

The _Audit Logs_ section is currently inactive.

## Load Tab

---

The _Load_ section is currently inactive.