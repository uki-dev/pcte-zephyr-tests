# World Clock

## Navigate to World Clock

---

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for World Clock or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/world-clock.png)

Once opening the microapplication you will see it display.

![](../Full/assets/images/world-clock-1.png)

---

## Set Time Zone

---

### Step 1: Click the Timezone Picker

- Click the _Up Arrow_ icon located along the bottom of the widget: Doing this produces a scrollable list of various timezones. 

    ![](../Full/assets/images/world-clock-up-arrow.png)
    
![](../Full/assets/images/world-clock-2.png)


### Step 2: Click Timezone Title

- Click the title of the desired timezone: Doing this closes the scrollable list and automatically updates the timezone.

![](../Full/assets/images/world-clock-3.png)

