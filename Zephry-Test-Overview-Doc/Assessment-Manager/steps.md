# Assessment Manager

![](../Full/assets/images/image22.png)

The Assessment Manager uses a competency backed system to build, schedule, and deliver version-controlled reusable assessments to validate knowledge, skills, and abilities.

Assessments can be structured as quizzes, manual or automated assessments. Assessment Data is sent via Tin Can (xAPI) statements to a Transactional Learner Record Store (tLRS), processed, and stored in an Authoritative Learner Record Store (aLRS).

Assessment Manager allows users to build, manage, and coordinate learning experiences to test user’s competencies. Assessment frameworks allow users to select activities to assess competencies, evaluation criteria, schedule, and assign assessments to users through a step-by-step wizard. The Assessment manager provides the ability to reuse, version control, share, and denote as authoritative (i.e., compliance with a standard).

## Create a Manual Assessment

---

### Step 0: Set Application to _Full Screen_

It is recommended that the application is viewed at the _Full Screen_ size. 

To do this, please click the _Full Screen_ icon located in the header of the application window. (Located to the left of the _Close_ icon).

### Step 1: Navigate to Assessment Manager

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for the Assessment Manager or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/Assessment-Manager-Navigation-2.png)

### Step 2: Create an Assessment

Stay on the Templates Tab on the Assessment Manager Micro-application.

![](../Full/assets/images/Assessment-Manager-template.png)

Click the _Create Template_ button located above the assessment template's table.

![](../Full/assets/images/image22.png)

This will have the create assessment window pop up to fill in corresponding information such as Assessment Name, Delivery Type, and Tags.

![](../Full/assets/images/image26.png)

---

### Step 3: Create Assessment Form

Input name and tags for the new assessment and ensure that the _Manual Exercise button_ delivery type is selected.

![](../Full/assets/images/Assessment-Manager-template-2.png)

_You must have a **Template** name and **Delivery Type** selected to create a template._

![](../Full/assets/images/Assessment-Manager-template-3.png)

Click the _Create_ button to generate the new assessment.

_Note: The user will be redirected to the assessment’s template details page._

---

### Step 4: Assessment Details

Review the name, enter a description, tag with metadata, and update the template with any desired changes. The assessment’s status will be located in the page header and will be set to the *Draft *state.

![](../Full/assets/images/image10.png)

Use the update or cancel button to save or remove any changes to the template details.

![](../Full/assets/images/Assessment-Manager-Template-Details.png)

---

### Step 5: Objective Scoring - Global Scoring

Navigate to the _Objective Scoring_ sub menu that is located in the left column. From the _Objective Scoring_ layout, the user can define hierarchical levels for the assessment.

![](../Full/assets/images/image55.png)

- Toggle Impose Global Scoring to the on position.

- To add additional scoring level click the "__Add additional scoring level__" button

- Update the scoring levels to the appropriate Score name.

- Click the colored circle corresponding to the scoring level, to change or select the color best suited for each score option.

- Select the _Passing_ option for scores that are considered passing.

- Click the _Add Additional Scoring Level_ to provide more scoring options.

- Click the _Update Scoring_ button.

_Note: Setting the Objective Scoring, will be the basis of how the manual test is scored. The colors that are chosen for each score will be used to easily identify whether a student meets a particular criteria._

---

### Step 6: Builder

Navigate to the _Builder_ tab, located in the page header. This tab will allow users to build out their manual assessment.

![](../Full/assets/images/image1.png)

The following steps are used to set the objectives for the assessment:

_Note: For each objective, the user will define the evaluation criteria justifying each scoring level. This will be done in Step 7)._

- Click the _Create Objective_ button

     The add new objective form will display.
     
     ![](../Full/assets/images/Assessment-Manager-Builder-New-Objective.jpg)
     
- Complete the form within the modal to add a top level objective.

- Hover over an objective’s name within the hierarchy chart to produce an _Options_ icon seen below

   ![](../Full/assets/images/Assessment-Manager-Builder-Options.png)

- Clicking the icon creates a submenu

  - Add Child

  - Delete

- Click the _Add Child_ option

     ![](../Full/assets/images/Assessment-Manager-Builder-Options-Select.png)

- Complete the form within the modal to add a child to the desired objective.
  
  ***Adding Child Objective Name and submitting the new objective***

---

### Step 7: Grading Scale

Now the user will fill out the details for each objective. To get started, the user will click the objective’s name within the hierarchy chart. Clicking the objective will produce a form on the right side of the page with two forms:

- Details

- Grading Scale

Fill out the details form to update the objective’s basic information. Additionally, fill out the _Grading Scale’s_ information to show manual assessors what each score’s requirements would be.

![](../Full/assets/images/Assessment-Manager-Builder-Grading-Scale.png)

Click the **Update Objective** button.

_Note: To ensure a user is on the correct objective, the form can be verified by looking at the form’s header and seeing the correct name._

---

### Step 8: Competency Mapping

Click the _Competency Mapping_ tab, located within the header of the page.

![](../Full/assets/images/image6.png)

To begin mapping competencies please click the _Target Framework_ select box that is located in the middle column.

Using the target framework’s hierarchy, locate the desired competency and click the check box to the left of it's title.

![](../Full/assets/images/Assessment-Manager-Competency-Mapping.png)

Next, users will want to click the checkbox located next to the source framework’s competency that is being mapped.

![](../Full/assets/images/image94.png)

Scroll to the bottom of the microapplication, review the competency mapping and click the _Save_ button.

![](../Full/assets/images/image37.png)

These new mappings are added to the _Saved Mapping_ container located in the right column of the widget.

![](../Full/assets/images/image14.png)

---

### Step 9: Preview Assessment

Click the _Preview_ tab, located within the header of the page.

![](../Full/assets/images/image88.png)

Using this tab, users can review the entire manual assessment to ensure that everything is acceptable.

Using the hierarchy, users can view all objectives details.

Within an objective, users can utilize the _Text Area_ to add comments about that particular objective to communicate with other users about the objective’s needs.

---

### Step 10: Publish Assessment

Bring your focus to the header of the page.

![](../Full/assets/images/image96.png)

Click the _Draft_ select menu and set the manual assessment to _Ready for Review_ then set it to _Published._

![](../Full/assets/images/Assessment-Manager-Publish-Assessment.png)

## Schedule & Assign a Manual Assessment

---

### Step 1: Assessment Tab

Navigate back to the _Templates_ tab, where the user will see a table containing all available templates.

![](../Full/assets/images/image22.png)

Located the template you wish to schedule and hover over the template's row. Doing this will produce an _Options_ menu on the right side of the template's row.

- Click the _Options_ icon
- Click the _Schedule_ option

Doing this produces a modal containing the following fields:

- Instance Name
- Start Date

Completing those fields will active the _Schedule_ button. Clicking this button will close the modal and redirect the user to the assessment's details page.

---

### Step 2: Assessment Overview

The assessment’s detail page utilizes tabs across the top of the page and a left column navigation for additional assessment information.

![](../Full/assets/images/image114.png)

By default the user is on the *Overview* page.

Edit any overview details desired.

Click the _Update_ button.

_Note: The Template Details card on the right side of the page cannot be edited. _

_Note: The Notify Participants Toggle is currently not functioning._

---

### Step 3: Assessors

Select the *Assessors* item, in the left column. Assessors are users who will be scoring a participant’s assessments. To add an assessor, please complete the following steps:

![](../Full/assets/images/image12.png)

- Click the _+_ icon located next to the _Users_ heading.

  ![](../Full/assets/images/Assessment-Manager-plus-button.png)


- Utilize the search function to locate the desired user

- Click the checkbox located next to the user’s name to

- Click the _Add_ button located at the bottom of the modal

  ![](../Full/assets/images/Assessment-Manager-Assessors.png)

Clicking the *Add* button will exit the _Add Assessors_ modal and will add the appropriate users.

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above. _

---

### Step 4: Participants

Click the _Participants_ menu item in the left column. The participants layout is very similar to the _Assessors_ layout described above.

![](../Full/assets/images/image31.png)

To add a participant, please complete the following steps:

- Click the _+_ icon located next to the _Users_ heading.

  ![](../Full/assets/images/Assessment-Manager-plus-button.png)

- Utilize the search function to locate the desired user

- Click the checkbox located next to the user’s name to

- Click the _Add_ button located at the bottom of the modal

  ![](../Full/assets/images/Assessment-Manager-participants.png)

Clicking the *Add* button will exit the _Add Participants_ modal and will add the appropriate users.

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above. _

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above._

## Complete a Manual Assessment

---

### Step 1: Assessment Tab

Navigate to the *Assessment Manager* microapplication and click the *Assessment *tab located in the header of the page.

![](../Full/assets/images/image93.png)

Select the assessment that you wish to complete. Clicking the assessment’s title will redirect the user to the assessment’s details page.
 
---

### Step 2: Deliver

Click the _Deliver_ tab, located in the page header.

![](../Full/assets/images/image51.png)

Using this tab, assesors can deliver the entire manual assessment to participants. 

- Using the hierarchy, users can view all objectives details. Click the first parent objective. 

Doing this will load the objective's details on the right side of the screen.

- Click a radio button next to one of the _Measure Status_ options. 

- Utilize the _Text Area_ to add comments about that particular objective to communicate with other users about the objective’s needs.  

- Click the _Update Measure_ button to submit.

Repeat this process for every objective within the assessments _Objective Hierarchy_.

### Step 3: Progress

Click the _Progress_ tab, located in the page header. This tab allows users to review activity within this assessment. 

Users can utilize the filters located at the top of the page to narrow the results.

![](../Full/assets/images/image132.png)

- Click the _Filter Selection_ select box and select one of the filters. Doing this will instantly update the results. 

- Click the search field and begin typing the desired information. Hit the _Enter_ key to submit the search criteria. The results will update accordingly.


---

### Step 4: Outcomes

Click the _Outcomes_ tab, located in the page header. This tab allows users to show assessment outcomes.

![](../Full/assets/images/image57.png)

This interface shows all of the competencies within the left column of the table. 

The next column shows the assessment objectives defined by the competency. This column also utilizes the objective scoring colors that were generated, to show how each objective was scored. 

Finally, the competencies final status is displayed in the right column (if any assessment objective was not scored as a _Pass_, the status will be shown as _Not Achieved_.

### Step 5: Finalize

Click to finalize button to finalize the results of the assessment.

![](../Full/assets/images/finalize_button.png)

A modal appears to confirm the finalization of the assessment. Doing this will send the assessment's results to the participant's _User Training Jacket_.

_Note: Please wait until the modal has disapeared before leaving this interface._

![](../Full/assets/images/finalize_assessment.png)

After the assessment is finalized, the assessment can no longer be edited.





