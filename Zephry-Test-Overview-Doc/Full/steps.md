# Login

![](assets/images/image125.png)

## Login Process

---

### Step 1: Click Login

When a user accesses the OpenDash360™ environment, they will have access to:

Persistent Cyber Training Environment Details

DoD Warning & Consent Banner

Action Buttons

The user must click the _Login_ button located underneath the _Persistent Cyber Training Environment_ information.

Doing this will redirect the user to the _Keycloak Login_ form.

---

### Step 2: Login Credentials

The user must now enter their username and password. Once this is done, the user must click the _Login_ button.

After successfully logging in, the user will be sent to the OpenDah360™ Lobby.


## Confirm DoD Warning & Consent Stays on Landing Page

### Step 1: Refresh Landing Page

When a user accesses the OpenDash360™ environment, they will have access to:

-   Persistent Cyber Training Environment Details
    
-   DoD Warning & Consent Banner
    
-   Action Buttons
    

Now the user will want to refresh their browser. Doing this will display the same landing page as before.

## Password Reset

The process for a user to reset their password begins on the *Keyclock Login *form page.

---

### Step 1: KeyCloak Login Form

From the _Keycloak Login_ form, a user must click the _Forgot Password_ link.

![](assets/images/image23.png)

Clicking this link will redirect the user to the _Self Service Account Management_ page.

![](assets/images/image68.png)

---

### Step 2: Enter Email Address

On the _Self Service Account Management_ page, enter the user’s email address and click the _Request Password Reset_ button.

\*Doing this will send an email to the user. This email will contain directions on resetting the user’s password.

\*_If email is set up in an environment._

# Assessment Manager

![](assets/images/image22.png)

The Assessment Manager uses a competency backed system to build, schedule, and deliver version-controlled reusable assessments to validate knowledge, skills, and abilities.

Assessments can be structured as quizzes, manual or automated assessments. Assessment Data is sent via Tin Can (xAPI) statements to a Transactional Learner Record Store (tLRS), processed, and stored in an Authoritative Learner Record Store (aLRS).

Assessment Manager allows users to build, manage, and coordinate learning experiences to test user’s competencies. Assessment frameworks allow users to select activities to assess competencies, evaluation criteria, schedule, and assign assessments to users through a step-by-step wizard. The Assessment manager provides the ability to reuse, version control, share, and denote as authoritative (i.e., compliance with a standard).

## Create a Manual Assessment

---

### Step 1: Create an Assessment

Navigate to the *Assessment Manager *microapplication and click the _Create Assessment_ button located above the assessment's table.

![](assets/images/image26.png)

![](assets/images/image22.png)

---

### Step 2: Create Assessment Form

Input name and keywords for the new assessment and ensure that the _Manual Exercise button_ is selected.

Click the _Create_ button to generate the new assessment.

_Note: The user will be redirected to the assessment’s details page._

---

### Step 3: Assessment Details

Review the name, enter a description, tag with metadata, and update the template with any desired changes. The assessment’s status will be located in the page header and will be set to the *Draft *state.

![](assets/images/image10.png)

---

### Step 4: Objective Scoring

Navigate to the _Objective Scoring_ sub menu that is located in the left column. From the _Objective Scoring_ layout, the user can define hierarchical levels for the assessment.

![](assets/images/image55.png)

- Toggle Impose Global Scoring.

- Update the appropriate Score names.

- Click the red circle, to select the colors for each score option.

- Select the _Passing_ option for scores that are considered passing.

- Click the _Add Additional Scoring Level_ to provide more scoring options.

- Click the _Update Scoring_ button.

_Note: Setting the Objective Scoring, will be the basis of how the manual test is scored. The colors that are chosen for each score will be used to easily identify whether a student meets a particular criteria._

---

### Step 5: Builder

Navigate to the _Builder_ tab, located in the page header. This tab will allow users to build out their manual assessment.

![](assets/images/image1.png)

The following steps are used to set the objectives for the assessment:

_Note: For each objective, the user will define the evaluation criteria justifying each scoring level. This will be done in Step 7)._

- Click the _Create Objective_ button

- Complete the form within the modal to add a top level objective.

- Hover over an objective’s name within the hierarchy chart to produce an _Options_ icon

- Clicking the icon creates a submenu

  - Add Child

  - Delete

- Click the _Add Child_ option

- Complete the form within the modal to add a child to the desired objective.

---

### Step 6: Grading Scale

Now the user will fill out the details for each objective. To get started, the user will click the objective’s name within the hierarchy chart. Clicking the objective will produce a form on the right side of the page with two forms:

- Details

- Grading Scale

Fill out the details form to update the objective’s basic information. Additionally, fill out the _Grading Scale’s_ information to show manual assessors what each score’s requirements would be.

Click the *Update Objective\*\* *button.

_Note: To ensure a user is on the correct objective, the form can be verified by looking at the form’s header and seeing the correct name._

---

### Step 7: Competency Mapping

Click the _Competency Mapping_ tab, located within the header of the page.

![](assets/images/image6.png)

To map competencies, users will want to click the checkbox located next to the source framework’s competency that is being mapped.

Next, click the _Target Framework_ select box that is located in the middle column and select the desired competency.

Using the target framework’s hierarchy, locate the desired competency.

![](assets/images/image94.png)

Scroll to the bottom of the microapplication, review the competency mapping and click the _Save_ button.

![](assets/images/image37.png)

These new mappings are added to the _Saved Mapping_ container located in the right column of the widget.

![](assets/images/image14.png)

---

### Step 8: Preview Assessment

Click the _Preview_ tab, located within the header of the page.

![](assets/images/image88.png)

Using this tab, users can review the entire manual assessment to ensure that everything is acceptable.

Using the hierarchy, users can view all objectives details.

Within an objective, users can utilize the _Text Area_ to add comments about that particular objective to communicate with other users about the objective’s needs.

---

### Step 9: Publish Assessment

Bring your focus to the header of the page.

![](assets/images/image96.png)

Click the _Draft_ select menu and set the manual assessment to _Published._

## Edit a Manual Assessment

---

### Step 1: Select an Assessment

Navigate to the *Assessment Manager *microapplication and click the name of the assessment that is going to be edited.

This will navigate the user to the assessment’s details page.

![](assets/images/image22.png)

![](assets/images/image10.png)

---

### Step 2: Assessment Details

Review the name, enter a description, tag with metadata, and update the template with any desired changes.

The assessment’s status will be located in the page header and will be set to the *Draft *state.

---

### Step 3: Objective Scoring

Navigate to the _Objective Scoring_ sub menu that is located in the left column. From the _Objective Scoring_ layout, the user can define hierarchical levels for the assessment.

![](assets/images/image55.png)

- Toggle Imposes Global Scoring.

- Update the appropriate Score names.

- Click the red circle, to select the colors for each score option.

- Select the _Passing_ option for scores that are considered passing.

- Click the _Add Additional Scoring Level_ to provide more scoring options.

- Click the _Update Scoring_ button.

_Note: Setting the Objective Scoring, will be the basis of how the manual test is scored. The colors that are chosen for each score will be used to easily identify whether a student meets a particular criteria. _

---

### Step 4: Builder

### ![](assets/images/image1.png)

Navigate to the _Builder_ tab, located in the page header. This tab will allow users to build out their manual assessment.

The following steps are used to set the objectives for the assessment:

_Note: For each objective, the user will define the evaluation criteria justifying each scoring level. This will be done in Step 5)._

- Click the _Create Objective_ button

- Complete the form within the modal to add a top level objective.

- Hover over an objective’s name within the hierarchy chart to produce an _Options_ icon

- Clicking the icon creates a submenu

  - Add Child

  - Delete

- Click the _Add Child_ option

- Complete the form within the modal to add a child to the desired objective.

---

### Step 5: Grading Scale

Now the user will fill out the details for each objective. To get started, the user will click the objective’s name within the hierarchy chart. Clicking the objective will produce a form on the right side of the page with two forms:

- Details

- Grading Scale

Fill out the details form to update the objective’s basic information. Additionally, fill out the _Grading Scale’s_ information to show manual assessors what each score’s requirements would be.

Click the *Update Objective *button.

_Note: To ensure a user is on the correct objective, the form can be verified by looking at the form’s header and seeing the correct name._

---

### Step 6: Competency Mapping

Click the _Competency Mapping_ tab, located within the header of the page.

![](assets/images/image6.png)

To map competencies, users will want to click the checkbox located next to the source framework’s competency that is being mapped.

Next, click the _Target Framework_ select box that is located in the middle column and select the desired competency.

Using the target framework’s hierarchy, locate the desired competency.

![](assets/images/image94.png)

Scroll to the bottom of the microapplication, review the competency mapping and click the _Save_ button.

These new mappings are added to the _Saved Mapping_ container located in the right column of the widget.

![](assets/images/image14.png)

---

### Step 7: Preview Assessment

Click the _Preview_ tab, located within the header of the page.

![](assets/images/image88.png)

Using this tab, users can review the entire manual assessment to ensure that everything is acceptable.

Using the hierarchy, users can view all objectives details.

Within an objective, users can utilize the _Text Area_ to add comments about that particular objective to communicate with other users about the objective’s needs.

## Publish a Manual Assessment

---

### Step 1: Select an Assessment

Navigate to the *Assessment Manager *microapplication and click the name of the assessment that is going to be edited.

This will navigate the user to the assessment’s details page.

![](assets/images/image22.png)

---

### Step 2: Publish Assessment

Bring your focus to the header of the page.

![](assets/images/image96.png)

Click the _Draft_ select menu and set the manual assessment to _Published._

![](assets/images/image62.png)

## Schedule & Assign a Manual Assessment

---

### Step 1: Assessment Tab

Navigate to the *Assessment Manager *microapplication and click the *Assessment *tab located in the header of the page.

![](assets/images/image93.png)

Select the assessment that you wish to schedule. Clicking the assessment’s title will redirect the user to the assessment’s details page.

---

### Step 2: Assessment Overview

The assessment’s detail page utilizes tabs across the top of the page and a left column navigation for additional assessment information.

![](assets/images/image114.png)

By default the user is on the *Overview *page.

Edit any overview details desired.

Click the _Update_ button.

_Note: The Template Details card on the right side of the page cannot be edited. _

---

### Step 3: Assessors

Select the *Assessors *item, in the left column. Assessors are users who will be scoring a participant’s assessments.To add an assessor, please complete the following steps:

- Click the _+_ icon located next to the _Users_ heading.

- Utilize the search function to locate the desired user

- Click the checkbox located next to the user’s name to

- Click the _Add_ button located at the bottom of the modal

![](assets/images/image12.png)

Clicking the *Add *button will exit the _Add Assessors_ modal and will add the appropriate users.

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above. _

---

### Step 4: Participants

Click the _Participants_ menu item in the left column. The participants layout is very similar to the _Assessors_ layout described above.

![](assets/images/image31.png)

To add a participant, please complete the following steps:

- Click the _+_ icon located next to the _Users_ heading.

- Utilize the search function to locate the desired user

- Click the checkbox located next to the user’s name to

- Click the _Add_ button located at the bottom of the modal

Clicking the *Add *button will exit the _Add Participants_ modal and will add the appropriate users.

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above. _

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above._

## Complete a Manual Assessment

---

### Step 1: Assessment Tab

Navigate to the *Assessment Manager *microapplication and click the *Assessment *tab located in the header of the page.

![](assets/images/image93.png)

Select the assessment that you wish to schedule. Clicking the assessment’s title will redirect the user to the assessment’s details page.

---

### Step 2: Assessment Overview

The assessment’s detail page utilizes tabs across the top of the page and a left column navigation for additional assessment information.

![](assets/images/image114.png)

By default the user is on the *Overview *page.

Edit any overview details desired.

Click the _Update_ button.

_Note: The Template Details card on the right side of the page cannot be edited. _

---

### Step 3: Assessors

Select the *Assessors *item, in the left column. Assessors are users who will be scoring a participant’s assessments.To add an assessor, please complete the following steps:

- Click the _+_ icon located next to the _Users_ heading.

- Utilize the search function to locate the desired user

- Click the checkbox located next to the user’s name to

- Click the _Add_ button located at the bottom of the modal

![](assets/images/image12.png)

Clicking the *Add *button will exit the _Add Assessors_ modal and will add the appropriate users.

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above. _

---

### Step 4: Participants

Click the _Participants_ menu item in the left column. The participants layout is very similar to the _Assessors_ layout described above.

![](assets/images/image31.png)

To add a participant, please complete the following steps:

- Click the _+_ icon located next to the _Users_ heading.

- Utilize the search function to locate the desired user

- Click the checkbox located next to the user’s name to

- Click the _Add_ button located at the bottom of the modal

Clicking the *Add *button will exit the _Add Participants_ modal and will add the appropriate users.

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above. _

_Note: Entire organizations can be added to an assessment, but clicking the + icon located next to the Organizations header and following the same steps described above._

---

### Step 5: Deliver

Click the _Deliver_ tab, located in the page header.

![](assets/images/image51.png)

Using this tab, users can review the entire manual assessment to ensure that everything is acceptable.

Using the hierarchy, users can view all objectives details.Within an objective, users can utilize the _Text Area_ to add comments about that particular objective to communicate with other users about the objective’s needs. Select the item you would like to review and review the score and comments.

## View the Outcome of a Manual Assessment

---

### Step 1: Assessment Tab

Navigate to the *Assessment Manager *microapplication and click the *Assessment *tab located in the header of the page.

![](assets/images/image93.png)

Select the assessment that you wish to schedule. Clicking the assessment’s title will redirect the user to the assessment’s details page.

---

### Step 2: Progress

Click the _Progress_ tab, located in the page header. This tab allows users to review activity within this assessment. Users can utilize the filters located at the top of the page to narrow the results.

![](assets/images/image132.png)

---

### Step 3: Outcomes

Click the _Outcomes _ tab, located in the page header. This tab allows users to show assessment outcomes.

![](assets/images/image57.png)

This interface shows all of the competencies within the left column of the table. The next column shows the assessment objectives defined by the competency. This column also utilizes the objective scoring colors that were generated, to show how each objective was scored. Finally, the competencies final status is displayed in the right column (if any assessment objective was not scored as a _Pass_, the status will be shown as _Not Achieved_.

# Competency Manager

![](assets/images/image76.png)

Competency Frameworks store the data describing competencies, their granular components (e.g., KSAOs and information like attitudes and motivation), and relationships among individual competencies (e.g., prerequisites and corequisites).

Also, these frameworks relate competency data to measures of performance and effectiveness based on demonstrated requirements for a given level of mastery in performance of a job or duty. Competency Data is further augmented by non-training related competencies. These competencies define behavior and cognitive characteristics that can be loosely mapped to job and role definitions. Competency Frameworks include but are not limited to:

- DoD Directive 8570/8140

- NIST NICE

- MITRE ATTACK

Custom Frameworks can be created to organize standard and custom competencies and can then share those competencies between enclaves.

Competencies within the Competency Manager microapplication can easily be customized to make measurements of performance as accurate as possible

## Creating Competency Framework

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager *microapplication and click the *Create Framework button *located above the table of frameworks. Clicking this button launches a modal with a basic form that needs to be completed.

![](assets/images/image3.png)

---

### Step 2: Framework Form

### ![](assets/images/image45.png)

Complete the _Create Framework_ form by filling out the following fields:

- Framework Name

- Framework Description

Click the _Submit_ button to generate the new framework.

_Note: The user will be redirected to the framework’s details page._

---

### Step 3: Creating Competencies

When a user arrives on the competency details page, they will notice a hierarchy chart on the left side of the page, with a *Create Root *button above.

![](assets/images/image87.png)

The following steps are used to create root competencies:

_Note: For each competency, the user will be able to add additional information. This will be done in Step 4)._

- Click the _Create Root_ button

- Clicking the button launches a modal that contains a form with the following fields:

  - Name

  - Description

  - Scope

  - Type

- Fill out the form details and click the _Create Competency_ button

Competencies can also be created as children. To do this, please do the following:

- Hover over a competencies name within the hierarchy chart to produce a _Plus_ icon

- Clicking the icon launches a modal that contains a form with the following fields:

  - Name

  - Description

  - Scope

  - Type

- Fill out the form details and click the _Create Competency_ button

---

### Step 4: Competency Details

Click on a competency title within the hierarchy chart in the left column. This will load various details about the selected competency on the right side of the page.

From this section, users can edit:

- Relationships

- Crosswalks

- Levels

- Resources Alignments

![](assets/images/image118.png)

Click the _+_ icon located next to the relationships heading. This launches a modal that contains a select box that will allow the user to choose the relationship they are interested in. Relationships include:

- broadens

- desires

- isEquivalentTo

- isRelatedTo

- narrows

- requires

Select _narrows then choose another competency._

_Doing this narrows this competency to the chosen competency (while still keeping it in its current position)._

![](assets/images/image59.png)

_Select broadens then choose another competency._

_Doing this broadens this competency to the chosen competency (while still keeping it in its current position)._

## Importing Competency Framework from JSON

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager *microapplication and click the *Create Framework button *located above the table of frameworks. Clicking this button launches a modal with a basic form that needs to be completed.

![](assets/images/image3.png)

---

### Step 2: Framework Form

When the *Create Framework *modal launches, click the _Create New_ select option and click the _Import_ option.

![](assets/images/image102.png)

Click the *Choose File *button and select the appropriate JSON file.

Click the _Submit_ button.

## Export Competency Framework as a JSON File

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager *microapplication and click the title of the framework that you wish to export.

![](assets/images/image3.png)

---

### Step 2: Export

### ![](assets/images/image110.png)

When a user arrives on the competency details page, click the _Options_ icon located next to the framework title.

Clicking this button produces a dropdown menu.

Click the _Export as JSON_ option. Doing this downloads the JSON file to the user’s computer.

## Deleting Competency Frameworks

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager *microapplication and click the title of the framework that you wish to delete.

![](assets/images/image3.png)

---

### Step 2: Delete

When a user arrives on the competency details page, click the _Options_ icon located next to the framework title.

![](assets/images/image29.png)

Clicking this button produces a dropdown menu.

Click the *Delete *option. Doing this produces a modal asking the user to confirm the deletion.

## Performing Crosswalk Competencies

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager *microapplication and click the title of the framework that you wish to crosswalk.

![](assets/images/image3.png)

---

### Step 2: Crosswalk

When a user arrives on the competency details page, click the _Options_ icon located next to the framework title.

![](assets/images/image18.png)

Clicking this button produces a dropdown menu.

Click the *Crosswalk *option. Doing this produces a modal that asks the user to select which framework that is being crosswalked.

---

### Step 3: Complete Crosswalk

The _Crosswalk_ interface contains two columns.

- Source Framework

- Target Framework

![](assets/images/image117.png)

Click the checkbox next to your source framework and your target framework’s competency that you wish to crosswalk.

Scroll down and click the *Map Competencies *button.

# Dashboard

## Access Dashboard

---

### Step 1: Lobby Page

After logging into the OpenDash360™ environment, users will arrive on the Lobby page. From here, users will want to click the _OpenDash360_ app card, beneath the Persistent* Cyber Training Environment *description.

![](assets/images/image44.png)

Clicking this app card will redirect the user away from the Lobby, to the Dashboard interface.

## Creating Dashboards

---

### Step 1: Lobby Page

From the Dashboard interface (below), users will want to click the _Options_ icon located to the right of the Dashboard title.

- Click the _Create Dashboard_ option

![](assets/images/image72.png)

---

### Step 2: Dashboard Details Modal

A modal will appears that contains the following fields:

- Dashboard Name

- Dashboard Description

The user must fill out these fields and click the _Update Dashboard_ button. Doing this will close the modal and take the user to the _Dashboard canvas._

![](assets/images/image97.png)

## Modifying Dashboards

---

### Step 1: Adding a Widget

From the _Dashboard Canvas_, click the _Add Widget_ button located at the top of the page. Doing this will open the widget panel from the left side of the page.

- Click the widget’s card to add the widget to the current dashboard

_Note: The widget panel remains open after clicking a widget, so multiple widgets can be added at a time._

![](assets/images/image112.png)

When all of the desired widgets have been added to the dashboard, the user can click the area to the right of the widget panel to close the widget panel.

These widgets can be resized and moved by dragging and dropping the corners of the widget.

---

### Step 2: Save Dashboard

Click the _Save_ button located in the upper right-hand corner of the page. Doing this will redirect the user to the non-editable version of the dashboard.

## Deleting Dashboards

---

### Step 1: Dashboard Options

From the main Dashboard UI (see below), users will want to click the _Options_ icon located to the right of the Dashboard’s title and click the _Edit_ option from the dropdown menu.

![](assets/images/image72.png)

---

### Step 2: Delete Dashboard

Next, users will want to click the _Trash_ icon located to the left of the _Add Widget_ button. Doing this will launch a modal asking for confirmation.

## Set Dashboard As Default

---

### Step 1: Dashboard Options

From the main Dashboard UI (see below), users will want to click the _Options_ icon located to the right of the Dashboard’s title and click the _Set As Default_ option from the dropdown menu.

After doing this, users will be able to refresh the page and find that the dashboard is now the default dashboard.

## Toggle Between Dashboards

---

### Step 1: Dashboard Options

From the main Dashboard UI (see below), users will find two arrows located to the left of the Dashboard’s title.

![](assets/images/image72.png)

Clicking these arrows will change the dashboard that is being viewed.

# Dashboard Manager

![](assets/images/image65.png)

## Add Event Collection

---

### Step 1: Access Dashboard Manager

Users must first open the *Dashboard Manager *microapplication.

---

### Step 2: Add Event Collection Button

Users will find an _Add Event Collection_ button located above the *Event Collections *table. Clicking this button launches a modal which contains a form that needs to be completed.

![](assets/images/image30.png)

---

### Step 3: Complete Event Collection Form

By filling out the *Event Collection Name *and the _Event ID_ fields, users can click the _Add Event Collection_ button.

Clicking this button removes the modal and the new event collection will be created.

## Delete Event Collection

---

### Step 1: Click Options Icon

From the *Event Collection *landing page, users need to hover over the desired event collection’s row. Doing this will make an *Option *icon on the far right side of the event collection’s row.

![](assets/images/image38.png)

The user must click the _Options_ icon. Doing this will produce a popup menu.

Finally, the user will click _Delete._


## Clone Dashboard Template

### Step 1: Locate Dashboard

From the Dashboard Template page, users must locate and hover over the desired dashboard’s row (within the Dashboard Template’s table).

### Step 2: Click Options Icon

Hovering over the template’s row will produce an Options icon on the far right-hand side of the table.

Clicking this icon will produce a dropdown menu with the following options:

-   View
    
-   Edit
    
-   Disable
    
-   Duplicate
    
-   Delete
    

### Step 3: Click Duplicate

Clicking the Duplicate option from the popup menu will instantly clone the dashboard template and add the new dashboard to the Dashboard Template’s table.

## Add Dashboard Template to Event Collection

---

### Step 1: Select Event Collection

From the *Event Collection *landing page, users must click the desired event collection’s name. This will load a table containing all of the event collection’s dashboard templates.

---

### Step 2: Select Event Collection

From the *Event Collection *landing page, users must click on the desired event collection’s name. This will load a table containing all of the event collection’s dashboard templates.

![](assets/images/image41.png)

---

### Step 3: Click Create Dashboard Button

Click the _Create Dashboard_ button, located above the *Dashboard Template *table.

---

### Step 4: Complete New Template Form

Fill out the _Dashboard Template Name_ field and select the desired _Team_ that will be receiving the dashboard.

Once the user clicks the *Create Template *button, the modal will be removed and the user will be redirected to the *Edit Template *interface.

## Edit Dashboard Template Settings

---

### Step 1: Select Dashboard

![](assets/images/image131.png)

From the *Event Collection’s Dashboard *table, the user must click the desired template’s title.

---

### Step 2: Click the Settings Icon

The user needs to click the _Settings_ icon located on the right side of the page. Doing this launches a panel out of the right side of the page. This panel contains all of the editable settings for the dashboard template.

![](assets/images/image46.png)

---

### Step 3: Update Template Settings

Once the user updates the _Template Settings_ form, they must click the *Save Settings *button located at the bottom of the form. Doing this closes the _Template Settings panel and updates the template._

## Add Widget to Dashboard Template

---

### Step 1: Select Dashboard

After selecting the appropriate dashboard, the user will land on the *Dashboard Editor *interface.

---

### Step 2: Click Widget Card

From the *Dashboard Editor *interface, users will see a list of available widgets located on the left side of the page.

![](assets/images/image123.png)

Users simply need to click the desired widget’s card to add it to the dashboard. These widgets can be resized and moved by dragging and dropping the corners of the widget.

## Delete Dashboard Template

---

### Step 1: Locate Dashboard

From the _Dashboard Template page, users must locate and hover over the desired dashboard’s row (within the Dashboard Template’s table)._

---

### Step 2: Click Options Icon

Hovering over the template’s row will produce an _Options_ icon on the far right-hand side of the table.

![](assets/images/image82.png)

Clicking this icon will produce a dropdown menu with the following options:

- View

- Edit

- Disable

- Duplicate

- Delete

---

### Step 3: Click Delete

Clicking the _Delete_ option from the popup menu will instantly delete the dashboard template.


## Clone Dashboard Template

### Step 1: Locate Dashboard

From the Dashboard Template page, users must locate and hover over the desired dashboard’s row (within the Dashboard Template’s table).

### Step 2: Click Options Icon

Hovering over the template’s row will produce an Options icon on the far right-hand side of the table.

Clicking this icon will produce a dropdown menu with the following options:

-   View
    
-   Edit
    
-   Disable
    
-   Duplicate
    
-   Delete
    

### Step 3: Click Duplicate

Clicking the Duplicate option from the popup menu will instantly clone the dashboard template and add the new dashboard to the Dashboard Template’s table.

# Landing Page Manager

## Create Environment

---

### Step 1: Click *Add New Environment *Button

After opening the *Landing Page Manager *microapplication, users are presented with a list of environments.

In order to create a new environment, users must click the *Add New Environment *button located at the top of the page. Doing this generates a modal with an _Add New Environment_ form.

![](assets/images/image80.png)

---

### Step 2: Complete *Add New Environment *Form

The user must complete the four required fields in the _Add New Environment_ form and then click the* Create* button located at the bottom of the modal.

![](assets/images/image16.png)

Doing this closes the modal and generates the new environment.

## Delete Environment

---

### Step 1: Click *Options *Icon

From the _Landing Page Manager’s_ landing page the user must first hover over the desired environment’s row. Doing this produces an *Options *icon on the right side of the row.

![](assets/images/image69.png)

Clicking the _Option_ icon produces a dropdown menu with the following options:

- Settings

- Delete

---

### Step 2: Click _Delete_ Option

The user must click the _Delete_ option. Doing this will delete the environment entirely.

## Edit Environment Details

- Settings

- SMTP

- Custom CSS

- White Labeling

---

### Step 1: Click Environment Name

The user must first select an environment and click the environment’s name.

---

### Step 2: Click _Settings_ Tab

The user must click the _Settings_ tab.

![](assets/images/image15.png)

This tab provides a form that allows the user to edit general settings for the environment.

This form allows administrators to edit the following information:

- Program Logo

- Environment Name

- Keycloak URL

- Environment Bound URL

- Classification Designation

- Enable Forgot Password (Toggle)

After the user edits the desired information, they must click the *Update Environment *button located at the bottom of the form.

---

### Step 3: Click _SMTP_ Tab

The user must click the _SMTP_ tab.

Administrators have the ability to toggle whether they wish to use a Native SMTP Relay Server. By default, this Native Relay is toggled off. Once an administrator toggles the Native Relay on, additional form elements appear.

Administrators must fill out the following information to connect their SMTP Relay:

| SMTP Relay Server (Toggle)
SMTP Host
SMTP Port
Envelope From
Enable SSL (Toggle)
Enable StratTLS (Toggle)
Enable Authentication (Toggle) | Authentication Username
Authentication Password
From SMTP Name
Reply to Display Name
From Email
Reply to Email |
| -- | -- |

After the user edits the SMTP information, they must click the *Update Environment *button located at the bottom of the form.

---

### Step 4: Click _Custom CSS_ Tab

The user must click the _Custom CSS_ tab.

This tab allows the user to add custom css to change how the front-end user interface will look.

After the user adds the desired CSS, they must click the *Update *button located at the bottom of the form.

---

### Step 5: Click _White Labeling_ Tab

The user must click the _White Labeling_ tab.

![](assets/images/image2.png)

This tab provides a form that allows the user to edit the environment’s logos and color schemes.

Specifically, administrators can update the following:

- Platform Logo

- Collapsed Logo

- Color Scheme

After the user edits the desired information, they must click the *Update Environment *button located at the bottom of the form.

---


## Edit Environment Landing Page

- Settings

- Info Banner

- Buttons

- Consent

- Compatibility

---

### Step 1: Click Environment Name

The user must first select an environment and click the environment’s name.

---

### Step 2: Click *Landing *Tab

Next, the user will click the _Landing_ tab, located in the header of the page.

---

### Step 3: Click _Settings_ Tab

By default, the user will be on the *Settings *tab, but if the user is coming from another tab, they will need to click the *Settings *tab.

![](assets/images/image98.png)

The _Settings_ tab allows administrators to update the header information located on the Landing page. Information that is editable include:

- Headline

- Overview

- Background Image

- Browser Compatibility Test (Toggle)

_Note: Administrators will see a preview of the landing page in the right column._

To save the edited information, the administrator must click the _Update Landing_ button located at the bottom of the interface.

---

### Step 4: Click _Info Banner_ Tab

The user must click the _Info Banner_ tab.

The Info Banner form generates a banner that is placed at the top of all login and registration pages. This form can be customized by editing the following fields:

- Enable/Disable (Toggle)

- Banner Text (Using WYSIWYG Editor)

- Banner Icon (Material Icons)

- Banner Background Color

![](assets/images/image28.png)

By toggling a banner off, an administrator is removing the classification banner all together.

_Note: As the Info Banner settings are changed, the preview banner is updated to allow the administrator to see what the banner will look like when posted. _

To save the edited information, the administrator must click the _Update Environment_ button located at the bottom of the interface.

---

### Step 5: Click _Buttons_ Tab

The user must click the _Buttons_ tab.

![](assets/images/image109.png)

The Landing Buttons tab allows administrators to update the buttons that are shown on the environment’s landing page. Administrators can add new buttons and configure the following information in regards to each button:

- Button Text

- Button Destination

- Button Icon (Material Icons)

- Button Background Color

Each button’s details are contained in a collapsible card. An administrator can view/edit these details by clicking the card’s title.

#### Adding Buttons

To add a new button, the administrator simply needs to click the _Add Button_ button beneath the existing cards. After clicking this button, a new collapsible card is added.

_Note: The maximum number of buttons allowed on the PCTE Landing Page is 6._

#### Deleting Buttons

To delete a button, an administrator needs to hover over the card title and click the _Trash_ icon that appears.

###### ![](assets/images/image106.png)

#### Hide Buttons

To hide a button on the front-end, an administrator needs to hover over the card title and click the _Eye_ icon that appears.

#### Saving Changes

Clicking the _Update Appearance_ button at the bottom of the tab saves all button changes.

_Note: As the button settings are added, removed or changed, the preview section is updated to allow the administrator to see what each banner will look like when posted._

---

### Step 6: Click _Consent_ Tab

The user must click the _Consent_ tab.

By clicking the _System Consent_ tab, a form appears that allows administrators to edit the System Consent that is displayed at the bottom of the landing page.

![](assets/images/image113.png)

Administrators have the ability to update:

- Consent Title

- Supplementary Text

- Consent Body (Using WYSIWYG Editor)

_Note: The system consent displays as a modal when the user first arrives on the PCTE Landing Page._

To save the edited information, the administrator must click the _Update Landing_ button located at the bottom of the interface.

---

### Step 7: Click _Capability_ Tab

The user must click the _Capability_ tab.

The compatibility tab allows administrators to set browser requirements for users to access the PCTE environment.

Specifically, administrators have the ability to add multiple browser types and configure the following information about each specific browser:

- Browser Type

- Minimum Version

![](assets/images/image79.png)

To add a Browser, a user simply needs to expand the *Browser Requirements card and then *select the browser from the dropdown menu. Finally, the users must use the input field to list the minimum version allowed.

To save the compatible browsers, the administrator must click the _Update Landing_ button located at the bottom of the interface.

## Edit Environment Lobby

- Info Banner

- App Cards

- Quick Access

- Mini Dash

- Support

---

### Step 1: Click Environment Name

The user must first select an environment and click the environment’s name.

---

### Step 2: Click *Lobby *Tab

Next, the user will click the _Lobby_ tab, located in the header of the page.

---

### Step 3: Click _Settings_ Tab

By default, the user will be on the *Settings *tab, but if the user is coming from another tab, they will need to click the *Settings *tab.

![](assets/images/image81.png)

The _Settings_ tab allows administrators to edit basic information about the Lobby. Editable information includes:

- Lobby Headline

- Lobby Overview

- Lobby Background Image

To save the edited information, the administrator must click the _Update Lobby_ button located at the bottom of the interface.

---

### Step 4: Click _Info Banner_ Tab

The user must click the _Info Banner_ tab.

![](assets/images/image17.png)

The Info Banner form generates a banner that is placed at the top of the Lobby. This form can be customized by editing the following fields:

- Enable/Disable (Toggle)

- Banner Text (Using WYSIWYG Editor)

- Banner Icon (Material Icons)

- Banner Background Color

By toggling a banner off, an administrator is removing the information banner all together.

_Note: As the Info Banner settings are changed, the preview banner is updated to allow the administrator to see what the banner will look like when posted. _

To save the edited information, the administrator must click the _Update Lobby_ button located at the bottom of the interface.

---

### Step 5: Click _App Cards_ Tab

The user must click the _App Cards_ tab.

The App Cards tab of the Lobby Section allows administrators to configure the application cards that sit on the top of the Lobby.

![](assets/images/image32.png)

These cards display in the top section of the Lobby and can be used to give users easy access to various applications within the PCTE environment.

App Cards can be enabled / disabled by clicking the toggle located at the top of the form. Once enabled, app cards can be added by clicking the _Add Additional App Card_ button located at the bottom of the collapsible card interface.

Once a new collapsible card is added, administrators can expand them by clicking the title _Tool Title_. Doing this provides the administrators with a basic form that asks for various information to build the app card. Required information includes:

- Title

- Custom Image

- Destination URL

- Description

- Role Access

Beneath that information, administrators will find another toggle that adds (or removes) the app cards to the Taskbar.

Once an app card is added, the administrator must click the _Save Card_ button and then the _Update Lobby_ button to process all of the changes made.

#### Browser Requirements

Within an app card’s collapsible container, the administrator will find another section titled, _Browser Requirements_. Administrators can add browser requirements for an application, by clicking the _Add Additional Browser_ button and setting the applications requirements.

---

### Step 6: Click _Quick Access_ Tab

The user must click the _Quick Access_ tab.

The _Quick Access_ tab allows administrators to enable / disable the quick access section of the Lobby by clicking the toggle located at the top of the tab interface.

![](assets/images/image40.png)

After enabling Quick Access, administrators can add applications to the Quick Access section by clicking the _Add Additional App_ button. Doing this adds a new collapsible card to the page, that when clicked, expands to provide the administrator with details regarding the application.

Administrators must add the following information for each Quick Access application:

- Microapplication

- Display Name

- Role Access

Once an application is added to the Quick Access section, the administrator must click the _Save App_ button and then click the _Update Lobby_ button to process all of the information.

---

### Step 7: Click _Mini Dash_ Tab

The user must click the _Mini Dash_ tab.

The _MiniDash_ tab allows administrators to enable / disable MiniDash cards by providing toggle buttons for the following applications:

- Notifications

- Account Services

- Network Speedtest

Additionally, administrators can update the title and overview description of the MiniDash section.

---

### Step 8: Click _Support_ Tab

The user must click the _Support_ tab.

![](assets/images/image52.png)

The _Support_ tab allows administrators to edit information within the _Support_ section within the Lobby. The _Support_ tab is broken into three horizontal tabs including:

- Overview

- FAQs

- Videos

---

### Step 9: Click _FAQ_ Tab

The user must click the _FAQ_ tab.

![](assets/images/image42.png)

The _FAQ_ tab at the top of the page allows administrators to generate questions/answers that are held within the panel on the Support portal.

Administrators can generate a question, by clicking the Create FAQ button at the top of the page. This launches a modal, which requires only a question and an answer.

_Note: Administrators have the option to completely disable the FAQ tab on the Support portal, by accessing the Lobby > FAQ tab and toggling off the FAQ option._

---

### Step 10: Click _Videos_ Tab

The user must click the _Videos_ tab.

![](assets/images/image105.png)

The _Videos_ tab allows administrators to generate videos that are held within the Videos panel on the Support portal.

Uploaded videos can be edited by clicking the _Options_ icon and selecting the _Edit_ option.

This launches a modal that gives basic details on the video. Administrators can also add additional details on each video such as:

- Video Name

- Video Description

- Keywords

At the bottom of the modal is a _Preview_ button. This will transform the modal to display the video so the administrator can watch the video.

Administrators can upload videos by clicking the _Upload Video_ button at the top of the page.

Clicking this button launches a multi-step modal that walks the administrator through everything required to post a video.

**Steps to create a video include:**

- Select Asset

- Complete Video Details

- Confirm

_Note: Administrators have the option to completely disable the Videos tab on the Support portal, by accessing the Lobby > Videos tab and toggling the Video option._

# Microapp Manager

![](assets/images/image100.png)

## Add Microapplication

---

### Step 1: Click Add New Microapp Button

Upon accessing the *Microapp Manager *microapplication, users will click the *Add New Microapp *button located above the microapplication’s table. Doing this will launch the *Add New Microapp *modal.

![](assets/images/image126.png)

---

### Step 2: Select Vendor

The modal that appears is a form that allows users to add their microapplication. First, the user must use the select box to choose which vendor this application is associated with.

---

### Step 3: Select Process

The user will see three different tabs beneath the _Vendor_ select box.

- Upload

- Repository

- External

Users must choose between these three processes to add their microapplication.

---

### Step 4a: Upload

The _Upload_ option contains a simple file picker. Using this, users can upload their entire microapplication bundle. Once the file is added, click the _View Details_ button, to proceed to the next step.

---

### Step 4b: Repository

The _Repository_ option provides a _Repository URL, where users can add a link to their application’s repository. If this URL connects correctly, the next field (Ref - Branch / Tag), will turn into a select box, where the user can choose the exact branch that they wish to connect the application to. _

![](assets/images/image35.png)

Once the URL and branch ref are connected, they can click the _View Details_ button, to proceed to the next step.

---

### Step 4c: External

The _External_ option provides the user with a simple text field. Users can provide a direct link to their repository here. Once the repository is connected, they can click the _View Details_ button to proceed to the next step.

---

### Step 5: Confirm

The final step for uploading a microapplication is a form. This form allows users to edit their microapplication’s name, description and icon.

![](assets/images/image27.png)

Once this is complete, the user must click the _Confirm App_ button. The user will be redirected to the application’s page.

## Edit Microapplication

---

### Step 1: Locate Microapplication

From the *Microapp Manager’s *landing page, a user will click the desired microapplication’s title. Doing this will redirect the user to the microapplication’s _Details_ page.

---

### Step 2: Edit Overview

![](assets/images/image19.png)

Once a user has clicked the microapplication’s title, they will be within the microapplication’s editable area.

If desired, change the details located on the left side of the page. Once the correct information has been added, click the _Update App_ button.

## Change Microapplication Permissions

---

### Step 1: Access Permissions

![](assets/images/image64.png)

Users will now click the _Permissions_ tab, located at the top of the page.

---

### Step 2: Change Permissions

To change a roles visibility, users will need to do the following:

**Add Role**
Users will find a list of roles on the left side of the page. To add a role, simply drag the role’s title to the appropriate section on the left (App Visibility card or Client Permissions card).

**Remove Role**
To remove a role, a user needs to hover over the role that is going to be removed and click the *X *icon that appears. The role will be removed instantly.

## Delete Microapplication

---

### Step 1: Locate Microapplication

From the *Microapp Manager’s *landing page, a user will click the microapplication’s title. Doing this will redirect the user to the microapplication’s _Details_ page.

---

### Step 2: Click Options Icon

![](assets/images/image108.png)

Users will notice an *Options *icon located in the header of the page (to the right of the microapplication’s title). Clicking the *Options *icon will launch a popup menu with the following options:

- Edit

- Redeploy

- Delete (If applicable)

---

### Step 3: Delete Microapplication

Next, users will want to click the *Delete *option to delete the microapplication.

## Enable / Disable Microapplication

---

### Step 1: Locate Microapplication

From the _Microapp Manager page, users must locate and hover over the desired microapplication’s row (within the Microapplication table)._

---

### Step 2: Click Options Icon

Hovering over the microapplication’s row will produce an _Options_ icon on the far right-hand side of the table.

![](assets/images/image75.png)

Clicking this icon will produce a dropdown menu with the following options:

- Edit

- Enable (or Disable)

- Delete

---

### Step 3: Click Enable (or Disable)

Clicking the _Enable (or Disable)_ option will enable (or Disable) the microapplication.

_Note: Users can also delete microapplications from this location._

## Add Shared Request

![](assets/images/image25.png)

---

### Step 1: Shared Request Tab

From the landing page of the _Microapp Manager_, users will want to click the _Shared Request_ tab located at the top of the page. Doing this will redirect the user to the *Shared Request *overview page.

![](assets/images/image39.png)

---

### Step 2: Click the Add New Microapp

Next, click the *Add New Microapp *button located at the top of the page. Doing this launches a modal.

---

### Step 3: Complete Shared Request Form

Users will notice the following fields, that are required to add a shared request:

- Request Name

- Request Type

- Access Mapping

The _Request Type_ field will add additional fields to the form depending on the type that is requested.

![](assets/images/image56.png)

Once completed, click the _Save_ button located at the bottom of the form.

## Edit Shared Request

---

### Step 1: Shared Request Tab

From the landing page of the _Microapp Manager_, users will want to click the _Shared Request_ tab located at the top of the page. Doing this will redirect the user to the *Shared Request *overview page.

---

### Step 2: Click Shared Request Title

Users must click the desired Shared Request’s title to launch a modal. This modal contains editable information on the Shared Request.

---

### Step 2: Edit Shared Request

Update the Shared Request’s information and click the _Save_ button.

# Notification Broadcast

## Send Broadcast

---

### Step 1: Notification Basic Information

The Notification Broadcast is a single form that is used to send out notifications to users. To get started, users will need to add a Broadcast Title, a Broadcast Message and a priority.

![](assets/images/image13.png)

Users can choose between the following priorities:

- Critical

- High-Priority

- Low-Priority

- Passive

###

Step 2: Notification Recipients

Users will see that the next section of the form allows them to select recipients of the notification.

- To send the notification to everyone, simply click the _Send to Everyone_ toggle

- To send to certain roles, begin typing the desired role in its textarea.

  - Doing this will generate a popup menu that contains roles related to the text that was typed.

- To send to certain users, begin typing the users name in its texarea

  - Doing this will generate a popup menu that contains users related to the text that was typed.

---

### Step 3: Broadcast Via

Finally, users will use the checkboxes at the bottom of the form to choose how to send the notification\*

\*_The Chat and Email modes of broadcast may not be activated. The Platform type is always active._

# Quick Note

![](assets/images/image21.png)

## Create New Note

---

### Step 1: Click _Take a Note_ Textfield

![](assets/images/image107.png)

Click the _Take a Note_ textfield. Doing this will change the text field to a container with two different elements that must be filled out:

- New Note Title

- New Message Goes Here

Once these fields are complete, click the *Publish *button located underneath (and to the right) of the message container.

## Pin Note

---

### Step 1: Click _Options_ Icon

The user will find and hover over the note that they wish to pin. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note

---

### Step 2: Click _Pin Note_

The user will now click the _Pin Note_ option. Doing this will immediately send the note to the _Pinned Notes_ section.

## Share Note

---

### Step 1: Click _Options_ Icon

The user will find and hover over the note that they wish to share. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note

---

### Step 2: Click _Share Note_

The user will now click the _Share Note_ option. Doing this will immediately open the note and display a search box for _Roles._.

---

### Step 3: Select Recipients

Begin typing the role name that will be receiving this note. An autocomplete will be provided if a role is found.

---

### Step 4: Click _Share_ Button

Finally, the user simply needs to click the _Share_ button located at the bottom of the page.

## Highlight Note

---

### Step 1: Click *Options *Icon

The user will find and hover over the note that they wish to highlight. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note

---

### Step 2: Click _Edit Note_

The user will now click the _Edit Note_ option. Doing this will immediately open the note and display various icons at the bottom of the note.

---

### Step 3: Click *Color *Icon

The user will now click the _Color_ icon. Doing this will immediately open various color options that are available.

---

### Step 4: Select Color

The user will click the desired color.

---

### Step 5: Save the Note

After the color is established, the user needs to click the _Save_ button located in the bottom right corner of the note.

## Add Image to Note

---

### Step 1: Click *Options *Icon

The user will find and hover over the note that they wish to edit. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note

---

### Step 2: Click _Edit Note_

The user will now click the _Edit Note_ option. Doing this will immediately open the note and display various icons at the bottom of the note.

---

### Step 3: Click *Image *Icon

The user will now click the _Image_ icon. This icon acts as a file picker.

---

### Step 4: Select Image

The user will now select the appropriate image file from their computer. Doing this will automatically add the image to the note.

---

### Step 5: Save the Note

After the image is added, the user needs to click the _Save_ button located in the bottom right corner of the note.

## Delete Note

---

### Step 1: Click *Options *Icon

The user will find and hover over the note that they wish to delete. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note

---

### Step 2: Click _Delete Note_

The user will now click the _Delete Note_ option. Doing this will immediately delete the note.

## Filter Notes

---

### Step 1: Select Note Type

Users will find various tabs located at the top of the widget. Options include:

- All Docs

- Personal

- Shared

The user can filter notes by clicking the desired tab. Doing this will load the appropriate notes.

# SMTP

## Add Outgoing Mail Server to JSD

---

### Step 1: Login to Helpdesk

### ![](assets/images/image77.png)

Log into helpdesk.pcte.mil with JSD admin role.

- Click the _Gear_ icon

- Click the _System_ option from the popup menu

Doing this will open the _System Admin Console_ window within JSD.

---

### Step 2: Outgoing Mail

Next, the user will scroll down the left side navigation menu to the _Mail_ header.

- Click _Outgoing Mail_

![](assets/images/image20.png)

Clicking _Outgoing Mail_ will take the user to the SMTP settings page.

---

### Step 3: Click SMTP Button

Click the _Configure New SMTP Mail Server_ button.

Clicking this button will launch a modal titled, _Add SMTP Mail Server_.

![](assets/images/image130.png)

---

### Step 4: Complete SMTP Form

Complete the entire form that contains the following fields:

- Name

- Description

- From address

- Email prefix

- Service Provider

- Protocol

- Host Name

- SMTP Port

Once these fields are complete the user must click the _Add_ button.

Doing this will take you back to the previous screen with an entry for your newly added outgoing server.

---

### Step 5: Testing the Connection

### ![](assets/images/image124.png)

In order to test the new outgoing mail server, the user must click on the _Test Email_ link located on the right side of the page.

Doing this opens an email preview screen for the user to complete.

# Toolbar

The toolbar is located in a fixed position at the bottom of every page. The toolbar can be broken into five sections (left to right):

- App Launcher

- App Cards

- User Center

- Chat

- Notifications

![](assets/images/image99.png)

## Launch Applications / Widgets

---

### Step 1: Click Application Icon

### ![](assets/images/image86.png)

While looking at the Toolbar, users will want to click the icon located on the far left side of the widget (see right).

Clicking this icon will launch a modal that contains all available microapplications and widgets.

---

### Step 2: Searching for Microapplications / Widgets

Users will find a search bar located at the top of the modal. Typing the desired applications title will produce the application icon.

Clicking this icon will close the modal and launch the application.

![](assets/images/image90.png)

## Launch App Cards

---

### Step 1: Click App Card Icon

### ![](assets/images/image111.png)

The next section within the toolbar contains various icons (that are defined as _App Cards_ within the Landing Page Manager).

Clicking these icons will redirect the user to that particular application.

_Note: The Home icon is default and will always be present. This icon links to the OpenDash360™ Lobby._

## Launch User Center

---

### Step 1: Click User Center Icon

The next section within the toolbar contains a single circle with the user’s initials.

Clicking this circle launches the User Center popout.

![](assets/images/image73.png)

![](assets/images/image54.png)

## User Center Functions

Within the User Center, users will find the following options:

- View Profile

- Run Speed Test

- Dark Mode

- Submit Feedback

- Log Out

![](assets/images/image67.png)

---

### Step 1: View Profile

Click the _View Profile_ option. This loads basic information about the user.

The user can navigate back to the _User Center_ interface by clicking the _Back_ icon, located to the left of the _My Profile_ title.

---

### Step 2: Run Speed Test

Click the _Run Speed Test_ option. This loads the Speed Test interface. Users can run the Speed Test by clicking the *Run Test *button located at the bottom of the interface.

---

### Step 3: Toggle Dark Mode

Click the toggle icon located next to the *Dark Mode *option. Doing this will enable _Dark Mode_. Clicking this toggle again, will revert OpenDash360™ back to _Light Mode_

---

### Step 4: Submit Feedback

### ![](assets/images/image127.png)

Click the *Submit Feedback *option. Doing this loads the *Submit Feedback *interface.

This interface consists of a basic form that sends feedback to the *Feedback Manager *microapplication.

The user can navigate back to the _User Center_ interface by clicking the _Back_ icon, located to the left of the _Feedback_ title.

---

### Step 5: Log Out

Click the *Logout *option. Doing this will instantly log the user out of the OpenDash360™ environment. The user will be redirected to the OpenDash360™ Landing page.


## Launch Chat


---

### Step 1: Select Team

Click the select box to choose which team you want to view. This is located at the top of the Chat pane (to the right of the Chat title).

![](assets/images/chat-1.png)

---

### Step 2: Search Conversations

Using the search bar, begin typing the desired conversation. The conversations below will update based on the criteria added.

## Chat Conversations

---

### Step 1: Launch Conversation

Hover over the conversation that you would like to view and click the title. Doing this will open the conversation to the left of the Chat pane.

![](assets/images/chat-2.png)

---

### Step 2: Minimize Conversation

Click the Options icon located at the top of the conversations popout. This produces a menu that contains the following:

-   Minimize
    
-   Launch
    
-   Invite Members
    
-   View Members
    

![](assets/images/chat-3.png)

Click the Minimize option. Doing this will minimize this conversation and will create a new tab in the Taskbar that is titled the same as the conversation.

---

### Step 3: Open Minimized Conversation

Click the tab that was created in the Taskbar. Doing this will reproduce the conversation popout.

![](assets/images/chat-4.png)

---

### Step 4: Launch Conversation

Click the Options icon located at the top of the conversations popout. This produces a menu that contains the following:

-   Minimize
    
-   Launch
    
-   Invite Members
    
-   View Members
    

Click the Launch option. Doing this removes the conversation from its current position and re-produces it as a floating window.

---

### Step 5: Pin Conversation to Taskbar

Click the Options icon. Doing this produces a menu with the following options:

-   Open as Fixed Window
    
-   Pinned Posts
    

Click the Open as Fixed Window option to return the conversation to the taskbar.

---

### Step 6: View Conversation Members

Click the Options icon located at the top of the conversations popout. This produces a menu that contains the following:

-   Minimize
    
-   Launch
    
-   Invite Members
    
-   View Members
    

Click the View Members option. Doing this refreshes the interface and provides a searchable list of all of the members that are tied to the conversation.

![](assets/images/chat-5.png)

Click the X icon to return back to the conversation’s messages.

## Sending Messages

---

### Step 1: Send Text Message

Click into the text field located at the bottom of the conversation panel. Then, type in a brief message and hit the Enter key when finished.

Doing this will send a message to the user(s) that are part of your conversation.

---

### Step 2: Send Image Message

Click the Paper Clip icon located to the right of the conversation text field. Doing this will open up a Finder that will allow the user to search their computer to select an image.

Once an image is located and selected, click into the text field and type a message to go along with the image.

Hit the Enter key. This will successfully send your message (and image).

---

### Step 3: Send Emoticon Message

Click the Smiley Face icon located to the right of the conversation text field. Doing this will open up a panel with a list of Emoticons.

![](assets/images/chat-6.png)

Select the desired Emoticons

Once an icon is located and selected, click into the text field and type a message to go along with the icon.

Hit the Enter key. This will successfully send your message (and icon).

### Editing Messages

---

### Step 1: Pin Message to Channel

Hover over a message that is within the conversation. Doing this will make three options appear:

-   Pin Icon
    
-   Edit
    
-   Delete
    

Note: A user can only Edit or Delete their own message.

![](assets/images/chat-7.png)

Click the Pin icon. Doing this will fill in the icon. Currently, users can see pinned posts by completing the following steps:

-   Launch the conversation (see above)
    
-   Click the Options Icon
    
-   Click Pinned Posts


---

### Step 2: Edit Sent Message

Hover over a message that is within the conversation. Doing this will make three options appear:

-   Pin Icon
    
-   Edit
    
-   Delete
    

Click the Edit option. Update the message and click the Edit button when finished.

---

### Step 3: Delete Sent Message

Hover over a message that is within the conversation. Doing this will make three options appear:

-   Pin Icon
    
-   Edit
    
-   Delete
    

Click the Delete option. Doing this will ask the user for confirmation of deleting the message.

## Open Chat Options

---

### Step 1: Mute Audio Alert

Within the Chat panel (contains a list of all available chat conversations). Click the Options icon located to the right of the Teams select box. Doing this opens a popup menu that contains:

-   Mute Audio Alert
    
-   Mark All Read
    
-   Launch Open Chat
    

Click the Mute Audio Alert option.

![](assets/images/chat-8.png)

Doing this will turn off the sound that occurs when a user receives a message.

---

### Step 2: Mark All Messages as Read

Within the Chat panel (contains a list of all available chat conversations). Click the Options icon located to the right of the Teams select box. Doing this opens a popup menu that contains:

-   Mute Audio Alert
    
-   Mark All Read
    
-   Launch Open Chat
    

Click the Mark All Read option.

Doing this will mark all of the conversations in the Chat application as Read.

---

### Step 3: Launch Open Chat

Within the Chat panel (contains a list of all available chat conversations). Click the Options icon located to the right of the Teams select box. Doing this opens a popup menu that contains:

-   Mute Audio Alert
    
-   Mark All Read
    
-   Launch Open Chat
    

Click the Launch Open Chat option.

Doing this will launch the full scale Chat application in a new window.

## Launch Notifications

---

### Step 1: Click Notifications Icon

Clicking the *Notifications *icon launches the *Notifications *pane.

![](assets/images/image85.png)

Filter the notifications by: Clicking the select box and choosing the desired filter

Mark the Notification as *Read *by: Clicking the notification and then clicking the* Mark as Read *button

Clicking the *X *icon located in the top right corner of the *Notifications *pane will close the interface.


# User Manager

![](assets/images/image71.png)

## Search Users

---

### Step 1: Locate Search Bar

Users must first locate the search bar located at the top of the user’s table.

![](assets/images/image92.png)

---

### Step 2: Begin Typing

The user must begin typing the name or username of the user that they wish to find. The _User Manager_ table will automatically begin adjusting based on the search criteria.

## Filter User Columns

---

### Step 1: Click _Filter_ Icon

Locate and click the *Filter *icon located to the right of one of the table headers (Example: First Name). Doing this will launch a popup menu that contains a single search field.

![](assets/images/image33.png)

---

### Step 2: Begin Typing Desired Filter

Within this search field, the user must begin typing the desired search criteria. As the user types, the table will automatically update based on the search criteria.

## Access User Profile

---

### Step 1: Click User’s Name

Once an administrator finds the desired user they wish to view, they must click the user’s username. Doing this will redirect the administrator to the user’s profile.

![](assets/images/image103.png)

## Add Secondary Email

---

### Step 1: Access User’s Profile

Once an administrator finds the desired user they wish to edit, they must click the user’s username. Doing this will redirect the administrator to the user’s profile.

![](assets/images/image36.png)

---

### Step 2: Click _Add Alternate Email_ Button

The user will now need to click the _Add Alternate Email_ button located on the left side of the page. Doing this will generate a text field above the button.

---

### Step 3: Type Alternate Email

The user now needs to type the alternate email into the text field.

---

### Step 4: Click the _Save_ Icon

The user must now click the _Save_ icon located to the right of the _Alternate Email’s_ heading. Doing this will instantly save the new email address.

## Change Password

---

### Step 1: Access User’s Profile

Once an administrator finds the desired user they wish to edit, they must click the user’s username. Doing this will redirect the administrator to the user’s profile.

---

### Step 2: Type New Password

The user will see two fields beneath the *Update Password *heading that need to be filled out.

- New Password

- Confirm New Password

---

### Step 3: Click the _Save_ Icon

The user must now click the _Save_ icon located to the right of the _Update Password’s_ heading. Doing this will instantly save the new password.

_Note: Passwords must contain:_

- _1 Lowercase_

- _1 Uppercase_

- _1 Special Character_

- _1 Number_

- _8 Total Characters_


## Add / Remove User’s Role

### Step 1: Access Security & Access Tab

The user must click on the Security & Access tab located in the header of the page. Doing this will refresh the page and display various roles that a user has available.

### Step 2: Click Desired Role

Locate a Greyed Out role and hover over it. Doing this will change the background color to blue and will add a Plus icon.

Click this role. Doing this will add the user to this role.

![](assets/images/user-manager-1.png)

### Step 3: Refresh Page & Reopen User Manager Application

To confirm the user has been given this role, the user will want to refresh the browser and navigate back to the User Manager microapplication (and specifically to the Security & Access tab for the same user).

The user will see that the role is still applied.

### Step 4: Click Desired Role

Now the user will want to hover over the role that was added previously. Doing this will add an X icon, which signifies that the role will be removed when clicked.

![](assets/images/user-manager-2.png)

Click this role. Doing this will remove the user from the role.

## Search Organizations Hierarchy Tree

---

### Step 1: Navigate to _Organizations_ Tab

From the landing page of the *User Manager *microapplication, the user will need to click the _Organizations_ tab located at the top of the page.

![](assets/images/image63.png)

---

### Step 2: Utilize Search Field

The _Organizations_ tab contains a two column layout. The left column consists of a hierarchy or parent / child organizations. To search organizations within the hierarchy, users must use the search field located at the top of the hierarchy.

Begin typing the desired organization and the hierarchy will update accordingly.

## Display Organization’s Children (Hierarchy)

---

### Step 1: Click the _Right Arrow_ Icon

In order to show an organization’s child organizations, a user simply needs to click the _Right Arrow_ icon that is located to the left of the parent organization.

![](assets/images/image43.png)

Doing this transforms the *Right Arrow *icon into a *Down Arrow *icon and then displays every direct child organization.

## View Organization’s Details

---

### Step 1: Click Organization’s Name

![](assets/images/image11.png)

Clicking an organization’s name displays the organization’s details on the right side of the page. The following information can be found about each organization:

- Users

- Details

- Organizations

- History

## Access Trusted Agents

---

### Step 1: Click _Trusted Agents_ Tab

From the landing page of the *User Manager *microapplication, the user will need to click the _Trusted Agents_ tab located at the top of the page.

![](assets/images/image58.png)

## Search Trusted Agents

---

### Step 1: Begin Typing in Search Field

Users must first locate the search bar located at the top of the user’s table. The user must begin typing the name or username of the user that they wish to find.

![](assets/images/image83.png)

The table will automatically begin adjusting based on the search criteria submitted.

## Add Trusted Agent

---

### Step 1: Click _Add Trusted Agent_ Button

Users must click the *Add Trusted Agent *button located above the trusted agent’s table. Clicking this button produces a modal with an _Add Trusted Agent_’s form.

![](assets/images/image24.png)

---

### Step 2: Complete _Add Trusted Agent_ Form

The user must fill out the _Username_ field. As the user is typing the input field will produce available results based on the user’s criteria.

---

### Step 3: Click _Create Endorsement_ Button

Once the desired user is selected, the user must click the *Create Endorsement *button located at the bottom of the modal. Doing this will remove the modal and the user will now be considered a trusted agent.

## Revoke Trusted Agent’s Authority

---

### Step 1: Hover Over User’s Row

The _Trusted Agents_ tab contains a table that lists all of the available trusted agents. In order to revoke one of the user’s authority, the user must first hover over the targeted user’s row. Doing this will display an *X *icon on the far right side of the row.

![](assets/images/image101.png)

---

### Step 2: Click the _X_ Icon

Clicking the *X *icon on the far right side of the user’s row will instantly remove the user’s trusted agent capabilities.

## Access Sponsored Accounts Interface

![](assets/images/image47.png)

---

### Step 1: Click _Sponsored Accounts_ Tab

From the landing page of the *User Manager *microapplication, the user will need to click the _Sponsored Accounts_ tab located at the top of the page.

## Add Sponsored Accounts

---

### Step 1: Click _Add Sponsored Account_ Button

Users must click the *Add Sponsored Account *button located above the sponsored account’s table. Clicking this button produces a modal with an _Add Sponsored Account’s_ form.

![](assets/images/image115.png)

---

### Step 2: Complete _Add Sponsored Account_ Form

The user must fill out the _entire form_, including the *Activation Date *and the _Expiration Date_.

---

### Step 3: Click _Submit_ Button

Once the entire form is filled out, the user must click the *Submit *button located at the bottom of the modal. Doing this will remove the modal and create the new user.

---

### Step 4: Navigate to the Platform Settings Tab

Click to access the *Platform Settings*  tab then click the *Data Sync*  vertical tab. Click the *Sync Data*  button to finish syncing the users. By doing this, the newly created sponsored accounts will appear in the *Sponsored Accounts*  interface.


## View Sponsored Account’s Details

---

### Step 1: Click Sponsored Account’s Username

From the _Sponsored Accounts_ tab, users will see a table that shows every sponsored account that has been created.

To view a sponsored account’s details, simply click the desired user’s username.

## Disable Sponsored Account

---

### Step 1: Click the *Options *Icon

From the _Sponsored Accounts_ tab, users will see a table that shows every sponsored account that has been created.

To disable a sponsored account, the user must first hover over the desired user’s row. Doing this will produce an *Options *icon on the right side of the user’s row.

---

### Step 2: Click the *Options *Icon

Click the _Options_ icon to produce a popup menu which includes:

- Edit

- Enable

- Disable

---

### Step 3: Click the *Disable *Option

Click the _Disable_ option. Doing this instantly disables the user. The user will remain in the *Sponsored Account’s *table and can be re-enabled by using this same process.

# Whiteboard

![](assets/images/image50.png)

## Create New Whiteboard

---

### Step 1: Click _Create Board_ Button

When a user first accesses the _Whiteboard_ widget they will see a blank canvas with a list of available whiteboards on the left side of the page. Users will see a _Create Board_ button located beneath the list of boards.

Click the _Create Board_ button.

---

### Step 2: Complete New Whiteboard Form

Users are given a whiteboard form with the fields.

- Name

- Metadata Tags

![](assets/images/image34.png)

The metadata tags can be added and used to help users find the whiteboard. After completing the form, click the _Launch Board_ button to finish creating the whiteboard.

## Add Participant to Whiteboard

![](assets/images/image4.png)

---

### Step 1: Click the _Add Participant_ Button

After creating a new whiteboard, users will see a *Participants *panel located on the right side of the page.

Click the _Add Participant_ button.

---

### Step 2: Click Participant’s Name

Clicking the _Add Participant_ button clears the right panel and displays a list of available users. Simply click the participant’s name that needs to be added to the whiteboard.

## Claim Presenter

---

### Step 1: Click the _Claim Presenter_ Button

In order to edit the whiteboard’s canvas, a user must become the _presenter_. In order to become the _presenter_, users must click the _Claim Presenter_ button that is located in the bottom right corner of the widget.

![](assets/images/image129.png)

Clicking the _Claim Presenter_ button, will display various objects on the left side of the whiteboard’s canvas and give the user editing capabilities.

## Add Shape to Whiteboard

---

### Step 1: Click the *Shape *Icon

Click the *Shape *icon located on the left side of the page. Doing this will generate a popup menu with various shapes and a _Stroke Thickness_ bar.

![](assets/images/image84.png)

---

### Step 2: Select Type & Stroke

Choose one of the four shapes that is located on the top of the popup menu by clicking it. Next, use the number picker to select how large of a border you wish to have on your shape.

---

### Step 3: Draw Shape

Once the shape and stroke are selected, click onto the canvas and drag your mouse to where the shape will be located on the canvas.

## Add Text to Whiteboard

---

### Step 1: Click the *Text *Icon

Click the *Text *icon located on the left side of the page. Doing this will generate a popup menu with a number picker that is used to determine the text size.

![](assets/images/image8.png)

---

### Step 2: Select Text Size

Use the number picker to determine the font size of the text layer that you will be creating.

---

### Step 3: Click Onto Canvas & Type

Once the text size is determined, click onto the canvas and begin typing.

## Change Layer Color

---

### Step 1: Click the _Canvas Color_ Icon

Click the *Canvas Color *icon located on the left side of the page. Doing this will generate a popup menu that contains an extensive color selector.

![](assets/images/image61.png)

---

### Step 2: Select Color

Use the color selector to choose which color you would like to use for your next layer.

---

### Step 3: Add Layer

Once a color is chosen, click the *Text *icon or the _Shape_ icon located on the left side of the page and create a new layer. This layer will have the colors that was chosen.

## Add Image to Whiteboard

---

### Step 1: Click the _Add Image_ Icon

Click the *Image *icon located on the left side of the page. Doing this will generate a file picker which allows users to pick an image from their computer.

![](assets/images/image53.png)

---

### Step 2: Choose Image From Computer

Search your computer for the desired image.

---

### Step 3: Resize Image

After an image is selected it will be added to the canvas. To resize the image, the user must drag and drop the corners of the image to the desired size.

## Move Layer on Whiteboard

---

### Step 1: Click the _Cursor_ Icon

Click the *Cursor *icon located on the left side of the page.

---

### Step 2: Drag & Drop Layer

With the _Cursor_ icon selected, drag and drop the layer that is to be moved, to the desired location.

# Workforce Manager

![](assets/images/image74.png)

## View People in an Organization

---

### Step 1: Expand Organization Hierarchy

When a user launches the *Workforce Manager *microapplication, they will be presented with an interface with various tabs in the header. Additionally, users will see a _Right Arrow_ icon located in the upper left corner of the interface.

Users must click that icon to expand a panel that contains an organizational hierarchy.

---

### Step 2: Select Organization

Organizations are displayed in hierarchy format. In order to show an organization’s child organizations, a user simply needs to click the _Right Arrow_ icon that is located to the left of the parent organization.

![](assets/images/image104.png)

Doing this transforms the *Right Arrow *icon into a *Down Arrow *icon and then displays every direct child organization.

Users must click the name of an organization to close the panel and produce a layout that contains the organization’s information.

---

### Step 3: Click _People_ Tab

Once an organization is selected, users will see a tab layout at the top of the screen.

Users must click the *People *tab. Doing this will display a table that contains all of the people within that particular organization.

## Assign People to a Position

---

### Step 1: Select a Position

After an organization is selected, user will see the following tabs at the top of the screen:

- Positions

- People

- Activity

Users must click the _Positions_ tab.

![](assets/images/image89.png)

---

### Step 2: Click the *People *Tab

The _Positions_ tab initially shows all of the requirements of the positions. To add users to the position, users must click the _People_ tab.

![](assets/images/image120.png)

---

### Step 3: Click _Add User_ Button

Now, that the user is on the _People_ tab, the user must click the _Add User_ button located above the table. Clicking this button generates a modal that contains an *Add Users *form.

![](assets/images/image9.png)

---

### Step 4: Add Users

The _Add Users_ form contains a search form that allows for a selection of people. As the user begins typing into the search field, a dropdown menu will display providing results based on the search criteria.

## Navigate to a Person’s Training Jacket

---

### Step 1: Click the *People *Tab

After an organization is selected, user will see the following tabs at the top of the screen:

- Positions

- People

- Activity

Users must click the _People_ tab.

---

### Step 2: Click Username

Clicking the _People_ tab will produce a table that contains all of an organization’s members.

Users must click a member’s username to view their Training Jacket.

## View Positions in an Organization

---

### Step 1: Expand Organization Hierarchy

When a user launches the *Workforce Manager *microapplication, they will be presented with an interface with various tabs in the header. Additionally, users will see a _Right Arrow_ icon located in the upper left corner of the interface.

Users must click that icon to expand a panel that contains an organizational hierarchy.

---

### Step 2: Select Organization

Organizations are displayed in hierarchy format. In order to show an organization’s child organizations, a user simply needs to click the _Right Arrow_ icon that is located to the left of the parent organization.

![](assets/images/image104.png)

Doing this transforms the *Right Arrow *icon into a *Down Arrow *icon and then displays every direct child organization.

After selecting an organization, the left panel will close and users will see that the organization’s positions have been loaded.

## Create a Position

---

### Step 1: Click _Create Position_ Button

After an organization is selected, user will see the following tabs at the top of the screen:

- Positions

- People

- Activity

Users must click the _Create Position_ button that is located above the table. Click this will load a modal with a _Create Position_ form.

![](assets/images/image78.png)

---

### Step 2: Complete *Create Position *Form

Users must fill out the *Create Position *form and click the* Submit *button. Doing this will close the modal and create a new position within the organization.

## Delete a Position

---

### Step 1: Click _Option_ Icon

From the organization’s *Position *tab, users must hover over the desired position’s row. Doing this will generate an _Options_ icon on the right side of the row. \* \*

![](assets/images/image7.png)

Users must click the _Options_ icon. Doing this produces a popup menu with the following options:

- Edit

- Duplicate

- Delete

---

### Step 2: Click _Delete_ Option

Users must click the *Delete *option. Doing this will immediately remove the position from the organization.

## Edit Position’s Title

From the organization’s *Position *tab, users must hover over the desired position’s row. Doing this will generate an _Options_ icon on the right side of the row. \* \*

![](assets/images/image7.png)

Users must click the _Options_ icon. Doing this produces a popup menu with the following options:

- Edit

- Duplicate

- Delete

---

### Step 1: Click _Edit_ Option

Users must click the *Edit *option. Doing this will produce a modal that contains an _Edit Position_ form.

![](assets/images/image70.png)

---

### Step 2: Update *Edit Position *Form

Users must update the _Edit Position_ form with the desired information. Once complete, the user must click the *Submit *button at the bottom of the modal. Doing this will immediately remove the modal and update the position with the new details.

## Add/Remove a Position’s Requirements

---

### Step 1: Click Position’s Title

From the organization’s *Position *tab, a user must click on the desired position’s name.

![](assets/images/image116.png)

---

### Step 2: Click _Add Requirement_ Button

The user must click the _Add Requirement_ button located above the table containing all of the position’s requirements. A modal will appear containing an _Add Requirement_ form.

---

### Step 3: Fill Out _Add Requirement_ Form

A user must complete the entire _Add Requirement_ form and click the submit button. Doing this will remove the modal and add the new requirement.

---

### Step 4: Remove Requirement

In order to remove the requirement, the user must hover over the requirement’s row. Doing this will produce a *Trash *icon on the right side of the page.

The user must click this icon to delete the requirement from the position.

## View People in a Position

---

### Step 1: Click Position’s Title

After an organization is selected, user will see the following tabs at the top of the screen:

- Positions

- People

- Activity

Users must click the _Position_ tab.

![](assets/images/image60.png)

---

### Step 2: Click _People_ Tab

From the organization’s *Position *interface, the user will want to click the _People_ tab located in the header of the page. Doing this will display a table full of people that are currently in the position.

# World Clock

![](assets/images/image95.png)

## Set Time Zone

---

### Step 1: Hover Over Widget

To set a new time zone, a user must first hover over the widget with their cursor. When the user does this, a _Filter_ icon will appear in the upper right corner of the interface.

### ![](assets/images/image121.png)

---

### Step 2: Click Filter Icon

A user must click the *Filter *icon located in the upper right corner of the widget. Doing this generates a popup menu which contains a *Set Timezone *option.

Click the _Set Timezone_ option. Doing this will navigate the user to the _Edit World Clock_ interface.

---

### Step 3: Select Timezone

![](assets/images/image128.png)

Finally, the user must utilize the select box to select the desired timezone. Once this timezone is chosen, the user must click the _Update_ button located beneath the select box.


# Activity Registry

The Activity Registry is not currently integrated into the DevOps zone. The user interface will show "No Items Found". 

![](../Full/assets/images/activity-registry1.png)

Additionally, the "Competency Search" tab will not show any real information. It will simply state, "competency-search works!"

![](../Full/assets/images/activity-registry2.png)

# LRS Manager

The LRS Manager is not currently integrated into the DevOps zone. 

## tLRS Tab
The user interface will show "No Items". 

![](../Full/assets/images/lrs-manager-1.png)

## aLRS Tab
The user interface will show "No Items". 

![](../Full/assets/images/lrs-manager-2.png)

## Statement Processor Tab
The user interface will show "No Items". 

![](../Full/assets/images/lrs-manager-3.png)

## Audit Logs Tab
This tab is not currently clickable within the DevOps zone.

## Load Tab
This tab is not currently clickable within the DevOps zone.



# App Window Management



OpenDash360™ applications live within an app window that can be minimized, maximized, movable and resized.


## Launching an Application

### Step 1: 

Click the *App Launcher* icon located in the bottom left corner of the OpenDash360™ Taskbar. Doing this generates the *Applications* popout menu.

### Step 2: Click Application Title
Click an application's title. Doing this closes the popout menu and generates an application.



## Resize the Window
After launching an application, complete the following steps:

### Step 1: Drag & Drop Corner
Drag & Drop the bottom right corner of the application window to the desired position. Doing this resizes the window.



## Minimize Window

After launching an application, complete the following steps:

### Step 1: Click *Minus* Icon 
Click the Minus icon located in the top right corner of the application window. Doing this will close the application window and generate a tab within the Taskbar.

### Step 2: Click the New Taskbar Tab
Click the tab that was added to the taskbar when it was minimized. Doing this will relaunch the application to its original location.




## Maximize Window

After launching an application, complete the following steps:

### Step 1: Click the *Square* Icon 
Click the Square icon located in the top right corner of the application window. Doing this will make the application fill the entire screen.

### Step 2: Click the *Multi-Square* Icon 
Click the Multi-Square icon located in the top right corner of the application window. Doing this will revert the application window back to its original size.



## View Application Version

After launching an application, complete the following steps:

### Step 1: Click the *Horizontal Dots* Icon
Click the *Horizontal Dots* Icon located in the top right corner of the application window. Doing this generates a popup menu with the following options:

 - Rate App
 - Version Number

From here, the application's version number is provided.



## Rate Application
After launching an application, complete the following steps:

### Step 1: Click the *Horizontal Dots* Icon
Click the *Horizontal Dots* Icon located in the top right corner of the application window. Doing this generates a popup menu with the following options:

 - Rate App
 - Version Number


### Step 2: Click the *Rate App* Option
Click the *Rate App* Option. Doing this launches the 
*Submit Feedback* form within a modal.



## Close Application
After launching an application, complete the following steps:


### Step 1: Click the *X* Icon
Click the *X* Icon located in the top right corner of the application window. Doing this closes the application.

# Universal Training Jacket

The Universal Training Jacket is not currently integrated into the DevOps zone. The user interface will show "No Items Found". 

![](../Full/assets/images/universal-training-jacket.png)

The information found in the Universal Training Jacket may not be accurate because it is still in development.