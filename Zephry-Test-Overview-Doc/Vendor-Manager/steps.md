# Vendor Manager

---

The OpenDash360™ Vendor Manager is used to create, manage and delete vendors. 

---

## Create Vendor

Users will find a _Create Vendor_ button located above the vendor table. 

- Click the _Create Vendor_ button. 

Doing this generates a modal that contains a basic form titled, _Create Vendor_. This form requires a user to complete the following fields:

- Vendor Name
- Phone Number
- Email Address
- Website
- Description

When a user has completed these fields, they  must click the _Create Vendor_ button located in the bottom right corner of the modal. This will remove the modal, generate the new vendor and redirect the user to the _Edit Vendor_ interface.

## Search Vendor

From the _Vendor Manager_ landing interface, users can quickly locate a vendor by using the search bar above the vendor table. 

Note: To Execute a search, users must hit the Enter key to submit their search.

## Edit Vendor

From the _Vendor Manager_ landing interface, vendors can be edited by clicking on the desired vendor’s title within the vendor table. Doing this redirects the user to the _Edit Vendor_ page. The _Edit Vendor_ page consists of three horizontal tabs at the top of the page:

- Vendor Profile
- Members
- Microapps

Additionally, users will see an _Options_ icon located to the right of the vendor’s name. Clicking this icon expands a dropdown menu that allows a user to delete the vendor from OpenDash360™.  

### Vendor Profile 

Users initially land on the _Vendor Profile_ tab when they click to edit a vendor.

This tab provides a form containing basic information including: 

- Vendor Name
- Phone Number
- Email Address
- Website
- Description

#### Add Logo

Additionally, the column on the right contains a card, which allows users to add a logo for the vendor. 

To do this, a user must simply hover over the card and click the _Edit_ button that appears in the top right corner of the card. After this, a user must browse his / her computer for the logo file. 

After adding the logo, a user must click the _Upload_ button to complete the addition of the logo. 

Finally, the user must _save_ the entire page by clicking the _Update Vendor_ button beneath the form on the left.

### Members

The second tab within the _Edit Vendor_ page is, Members. This tab allows administrators to manage users within a vendor.

#### Add Member to Vendor

Users will find a _Add Member_ button located above the member table. When a user clicks the _Add Member_ button, a modal appears with a card titled, _Add Users to XXXX_. This table allows administrators to search users and add them to a vendor’s organization.

When an administrator finds a user they wish to add to the vendor, they must simply click the row of the desired user.

_Note: Clicking a user will remove the user from the table, but will not close the modal._

When an administrator has finished adding users, they must click the _Add Users to Role_ button located in the bottom right corner of the modal. This will remove the modal and the users will now be members of the vendor.

####  Search Members
From the _Members_ tab, users can quickly locate a member by using the search bar above the member table. 

_Note: To Execute a search, users must hit the Enter key to submit their search._

#### Remove Members
When attempting to remove a member from a vendor, an administrator must simply hover over the row of the appropriate member and click the _-_ icon that appears on the right side of the member’s row. This launches a modal that asks for a confirmation.

_Note: Removing a member cannot be undone. If a member has been removed by mistake, an administrator can re-add the user to the vendor._


### Microapplication

This interface contains every application uploaded by the vendor. The following information can be seen about each application using this interface:

- Application Name
- Version
- Client
- Context
- Upload Date
- Status

Administrators can learn more information about each application by clicking the application’s title. Doing this will redirect the administrator to the application’s page within the Microapp Manager.