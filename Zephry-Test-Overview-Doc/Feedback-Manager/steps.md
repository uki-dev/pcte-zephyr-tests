# Feedback Manager

### Step 1: Navigate to Feedback Manager

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for the Feedback Manager or click on the corresponding Application from the application menu. This will launch the landing page.

![](../Full/assets/images/feedback-manager.png)

## Feedback Manager

---

The Feedback Manager allows the user to search for feedback and see the number of submission for feedback on each item.

![](../Full/assets/images/feedback-manager-1.png)


### Rating

Hover over the Star Ratings to see the number rating corresponding to the stars.

![](../Full/assets/images/feedback-manager-2.png)

The rating can be sorted either ascending or descending using the arrow to the right of Rating Column header.

### View Feedback

Hover over the right end of the the row to see the _eye_ icon 

![](../Full/assets/images/feedback-manager-3.png)

Click the icon to be able to view the feedback for the specific rated item.

This will redirect to another landing page for the specific rated item.

Containing all the ratings by the user and date for the rating.

![](../Full/assets/images/feedback-manager-4.png)

### View Attachment

 If there is an attachment attached to the rating you can view this by hovering over the _image_ icon to next to the _trash_ icon. 

![](../Full/assets/images/feedback-manager-5.png)

Click on the _image_ icon to view the attachment.

This will bring up a window to view the attachment. Press the _X_ icon to exit.

![](../Full/assets/images/feedback-manager-6.png)

### Delete Submission

To delete the submission click on the _trash_ icon to delete the submission.

![](../Full/assets/images/feedback-manager-7.png)
