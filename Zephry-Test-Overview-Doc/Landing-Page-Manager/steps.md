# Landing Page Manager

## Edit Environment Details

---

### Step 1: Click Environment Name

The user must first select an environment and click the environment’s name.

### Step 2: Click _Settings_ Tab

The user must click the _Settings_ tab.

![](../Full/assets/images/image15.png)

This tab provides a form that allows the user to edit general settings for the environment.

This form allows administrators to edit the following information:

- Program Logo

- Environment Name

- Keycloak URL

- Environment Bound URL

- Classification Designation

- Enable Forgot Password (Toggle)

After the user edits the desired information, they must click the *Update Environment* button located at the bottom of the form.

### Step 3: Click _SMTP_ Tab

The user must click the _SMTP_ tab.

Administrators have the ability to toggle whether they wish to use a Native SMTP Relay Server. By default, this Native Relay is toggled off. Once an administrator toggles the Native Relay on, additional form elements appear.

![](../Full/assets/images/smtp-settings.png)

Administrators must fill out the following information to connect their SMTP Relay:

| Toggles | Fields     |
|----------|---------------:|
|  SMTP Relay Server (Toggle) |  SMTP Host |
|  SMTP Port |  Envelope From |
|  Enable SSL (Toggle) | Authentication Password|
|  Enable Authentication (Toggle) |  Authentication Username|
|  Enable StratTLS (Toggle)  |  From SMTP Name | 
|  |  From Email |
|  |  Reply to Display Name|
| |  Reply to Email |

After the user edits the SMTP information, they must click the *Update Environment* button located at the bottom of the form.

![](../Full/assets/images/update-environment.png)


### Step 4: Click _Custom CSS_ Tab

The user must click the _Custom CSS_ tab.

This tab allows the user to add custom css to change how the front-end user interface will look.

After the user adds the desired CSS, they must click the *Update* button located at the bottom of the form.

![](../Full/assets/images/update-environment.png)

### Step 5: Click _Theming_ Tab

The user must click the _Theming_ tab.

![](../Full/assets/images/image2.png)

This tab provides a form that allows the user to edit the environment’s logos and color schemes.

Specifically, administrators can update the following:

- Platform Logo

- Collapsed Logo

- Color Scheme

After the user edits the desired information, they must click the *Update Environment* button located at the bottom of the form.

![](../Full/assets/images/update-environment.png)

## Edit Environment Landing Page

---

### Step 1: Click Environment Name

The user must first select an environment and click the environment’s name.


### Step 2: Click *Landing* Tab

Next, the user will click the _Landing_ tab, located in the header of the page.

![](../Full/assets/images/landing-arrow.png)

### Step 3: Click _Settings_ Tab

By default, the user will be on the *Settings* tab, but if the user is coming from another tab, they will need to click the *Settings* tab.

![](../Full/assets/images/image98.png)

The _Settings_ tab allows administrators to update the header information located on the Landing page. Information that is editable include:

- Headline

- Overview

- Background Image

- Browser Compatibility Test (Toggle)

_Note: Administrators will see a preview of the landing page in the right column._

To save the edited information, the administrator must click the _Update Landing_ button located at the bottom of the interface.


### Step 4: Click _Info Banner_ Tab

The user must click the _Info Banner_ tab.

The Info Banner form generates a banner that is placed at the top of all login and registration pages. This form can be customized by editing the following fields:

- Enable/Disable (Toggle)

- Banner Text (Using WYSIWYG Editor)

- Banner Icon (Material Icons)

- Banner Background Color

![](../Full/assets/images/image28.png)

By toggling a banner off, an administrator is removing the classification banner all together.

_Note: As the Info Banner settings are changed, the preview banner is updated to allow the administrator to see what the banner will look like when posted._

To save the edited information, the administrator must click the _Update Environment_ button located at the bottom of the interface.

![](../Full/assets/images/update-environment.png)


### Step 5: Click _Buttons_ Tab

The user must click the _Buttons_ tab.

![](../Full/assets/images/image109.png)

The Landing Buttons tab allows administrators to update the buttons that are shown on the environment’s landing page. Administrators can add new buttons and configure the following information in regards to each button:

- Button Text

- Button Destination

- Button Icon (Material Icons)

- Button Background Color

Each button’s details are contained in a collapsible card. An administrator can view/edit these details by clicking the card’s title.

#### Adding Buttons

To add a new button, the administrator simply needs to click the _Add Button_ button beneath the existing cards. After clicking this button, a new collapsible card is added.

_Note: The maximum number of buttons allowed on the PCTE Landing Page is 6._

#### Deleting Buttons

To delete a button, an administrator needs to hover over the card title and click the _Trash_ icon that appears.

###### ![](../Full/assets/images/image106.png)

#### Hide Buttons

To hide a button on the front-end, an administrator needs to hover over the card title and click the _Eye_ icon that appears.

#### Saving Changes

Clicking the _Update Appearance_ button at the bottom of the tab saves all button changes.

_Note: As the button settings are added, removed or changed, the preview section is updated to allow the administrator to see what each banner will look like when posted._

### Step 6: Click _Consent_ Tab

The user must click the _Consent_ tab.

By clicking the _System Consent_ tab, a form appears that allows administrators to edit the System Consent that is displayed at the bottom of the landing page.

![](../Full/assets/images/image113.png)

Administrators have the ability to update:

- Consent Title

- Supplementary Text

- Consent Body (Using WYSIWYG Editor)

_Note: The system consent displays as a modal when the user first arrives on the PCTE Landing Page._

To save the edited information, the administrator must click the _Update Landing_ button located at the bottom of the interface.


### Step 7: Click _Capability_ Tab

The user must click the _Capability_ tab.

The compatibility tab allows administrators to set browser requirements for users to access the PCTE environment.

Specifically, administrators have the ability to add multiple browser types and configure the following information about each specific browser:

- Browser Type

- Minimum Version

![](../Full/assets/images/image79.png)

To add a Browser, a user simply needs to expand the *Browser Requirements card and then *select the browser from the dropdown menu. Finally, the users must use the input field to list the minimum version allowed.

To save the compatible browsers, the administrator must click the _Update Landing_ button located at the bottom of the interface.

## Edit Environment Lobby

---

### Step 1: Click Environment Name

The user must first select an environment and click the environment’s name.


### Step 2: Click *Lobby* Tab

Next, the user will click the _Lobby_ tab, located in the header of the page.


### Step 3: Click _Settings_ Tab

By default, the user will be on the *Settings *tab, but if the user is coming from another tab, they will need to click the *Settings *tab.

![](../Full/assets/images/image81.png)

The _Settings_ tab allows administrators to edit basic information about the Lobby. Editable information includes:

- Lobby Headline

- Lobby Overview

- Lobby Background Image

To save the edited information, the administrator must click the _Update Lobby_ button located at the bottom of the interface.


### Step 4: Click _Info Banner_ Tab

The user must click the _Info Banner_ tab.

![](../Full/assets/images/image17.png)

The Info Banner form generates a banner that is placed at the top of the Lobby. This form can be customized by editing the following fields:

- Enable/Disable (Toggle)

- Banner Text (Using WYSIWYG Editor)

- Banner Icon (Material Icons)

- Banner Background Color

By toggling a banner off, an administrator is removing the information banner all together.

_Note: As the Info Banner settings are changed, the preview banner is updated to allow the administrator to see what the banner will look like when posted. _

To save the edited information, the administrator must click the _Update Lobby_ button located at the bottom of the interface.


### Step 5: Click _App Cards_ Tab

The user must click the _App Cards_ tab.

The App Cards tab of the Lobby Section allows administrators to configure the application cards that sit on the top of the Lobby.

![](../Full/assets/images/image32.png)

These cards display in the top section of the Lobby and can be used to give users easy access to various applications within the PCTE environment.

App Cards can be enabled / disabled by clicking the toggle located at the top of the form. Once enabled, app cards can be added by clicking the _Add Additional App Card_ button located at the bottom of the collapsible card interface.

Once a new collapsible card is added, administrators can expand them by clicking the title _Tool Title_. Doing this provides the administrators with a basic form that asks for various information to build the app card. Required information includes:

- Title

- Custom Image

- Destination URL

- Description

- Role Access

Beneath that information, administrators will find another toggle that adds (or removes) the app cards to the Taskbar.

Once an app card is added, the administrator must click the _Save Card_ button and then the _Update Lobby_ button to process all of the changes made.

#### Browser Requirements

Within an app card’s collapsible container, the administrator will find another section titled, _Browser Requirements_. Administrators can add browser requirements for an application, by clicking the _Add Additional Browser_ button and setting the applications requirements.


### Step 6: Click _Quick Access_ Tab

The user must click the _Quick Access_ tab.

The _Quick Access_ tab allows administrators to enable / disable the quick access section of the Lobby by clicking the toggle located at the top of the tab interface.

![](../Full/assets/images/image40.png)

After enabling Quick Access, administrators can add applications to the Quick Access section by clicking the _Add Additional App_ button. Doing this adds a new collapsible card to the page, that when clicked, expands to provide the administrator with details regarding the application.

Administrators must add the following information for each Quick Access application:

- Microapplication

- Display Name

- Role Access

Once an application is added to the Quick Access section, the administrator must click the _Save App_ button and then click the _Update Lobby_ button to process all of the information.


### Step 7: Click _Mini Dash_ Tab

The user must click the _Mini Dash_ tab.

The _MiniDash_ tab allows administrators to enable / disable MiniDash cards by providing toggle buttons for the following applications:

- Notifications

- Account Services

- Network Speedtest

Additionally, administrators can update the title and overview description of the MiniDash section.


### Step 8: Click _Support_ Tab

The user must click the _Support_ tab.

![](../Full/assets/images/image52.png)

The _Support_ tab allows administrators to edit information within the _Support_ section within the Lobby. The _Support_ tab is broken into three horizontal tabs including:

- Overview

- FAQs

- Videos


### Step 9: Click _FAQ_ Tab

The user must click the _FAQ_ tab.

![](../Full/assets/images/image42.png)

The _FAQ_ tab at the top of the page allows administrators to generate questions/answers that are held within the panel on the Support portal.

Administrators can generate a question, by clicking the Create FAQ button at the top of the page. This launches a modal, which requires only a question and an answer.

_Note: Administrators have the option to completely disable the FAQ tab on the Support portal, by accessing the Lobby > FAQ tab and toggling off the FAQ option._


### Step 10: Click _Videos_ Tab

The user must click the _Videos_ tab.

![](../Full/assets/images/image105.png)

The _Videos_ tab allows administrators to generate videos that are held within the Videos panel on the Support portal.

Uploaded videos can be edited by clicking the _Options_ icon and selecting the _Edit_ option.

This launches a modal that gives basic details on the video. Administrators can also add additional details on each video such as:

- Video Name

- Video Description

- Keywords

At the bottom of the modal is a _Preview_ button. This will transform the modal to display the video so the administrator can watch the video.

Administrators can upload videos by clicking the _Upload Video_ button at the top of the page.

Clicking this button launches a multi-step modal that walks the administrator through everything required to post a video.

**Steps to create a video include:**

- Select Asset

- Complete Video Details

- Confirm

_Note: Administrators have the option to completely disable the Videos tab on the Support portal, by accessing the Lobby > Videos tab and toggling the Video option._

