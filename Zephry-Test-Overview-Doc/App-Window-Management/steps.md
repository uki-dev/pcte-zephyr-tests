
# App Window Management



OpenDash360™ applications live within an app window that can be minimized, maximized, movable and resized.


## Launching an Application

---

### Step 1: 

Click the *App Launcher* icon located in the bottom left corner of the OpenDash360™ Taskbar. Doing this generates the *Applications* popout menu.

![](../Full/assets/images/window-1.png)


### Step 2: Click Application Title
Click an application's title (Example: Microapp Manager). Doing this closes the popout menu and generates an application.

![](../Full/assets/images/window-2.png)


![](../Full/assets/images/app-window-management-1.png)



## Resize the Window

---


After launching an application, complete the following steps:

### Step 1: Drag & Drop Corner
Drag & Drop the bottom right corner of the application window to the desired position. Doing this resizes the window.

![](../Full/assets/images/app-window-management-2.png)


## Minimize Window

---

After launching an application, complete the following steps:

### Step 1: Click *Minus* Icon 
Click the Minus icon located in the top right corner of the application window. Doing this will close the application window and generate a tab within the Taskbar.

![](../Full/assets/images/app-window-management-3.png)


### Step 2: Click the New Taskbar Tab
Click the tab that was added to the taskbar when it was minimized. Doing this will relaunch the application to its original location.

![](../Full/assets/images/app-window-management-4.png)



## Maximize Window

---

After launching an application, complete the following steps:

### Step 1: Click the *Square* Icon 
Click the Square icon located in the top right corner of the application window. Doing this will make the application fill the entire screen.

### Step 2: Click the *Multi-Square* Icon 
Click the Multi-Square icon located in the top right corner of the application window. Doing this will revert the application window back to its original size.

![](../Full/assets/images/app-window-management-5.png)


## View Application Version

---

After launching an application, complete the following steps:

### Step 1: Click the *Horizontal Dots* Icon
Click the *Horizontal Dots* Icon located in the top right corner of the application window. Doing this generates a popup menu with the following options:

 - Rate App
 - Version Number

From here, the application's version number is provided.


## Rate Application

---


After launching an application, complete the following steps:

### Step 1: Click the *Vertical Dots* Icon
Click the *Vertical Dots* Icon located in the top right corner of the application window. 

 ![](../Full/assets/images/window-4.png)


Doing this generates a popup menu with the following options:

 - Rate App
 - Wiki
 - Version Number

 ![](../Full/assets/images/window-5.png)

### Step 2: Click the *Rate App* Option
Click the *Rate App* Option. Doing this launches the 
*Submit Feedback* form within a modal.

 ![](../Full/assets/images/app-window-management-7.png)


## Close Application

---


After launching an application, complete the following steps:


### Step 1: Click the *X* Icon
Click the *X* Icon located in the top right corner of the application window. Doing this closes the application.

 ![](../Full/assets/images/window-7.png)


