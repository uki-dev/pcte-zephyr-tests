# Competency Manager

---

## Navigate to Competency Manager

---

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for Competency Manager or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/competency-manager-navigation.png)


---

## Creating Competency Framework

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager* microapplication and click the *Create Framework* button located above the table of frameworks. 

![](../Full/assets/images/image3.png)


Clicking this button launches a modal with a basic form that needs to be completed.

![](../Full/assets/images/create_framework.png)

### Step 2: Framework Form

![](../Full/assets/images/competency-manager-update-1.png)

Complete the _Create Framework_ form by filling out the following fields:

- Framework Name
- Framework Description

Click the _Create Framework_ button to generate the new framework. Clicking this button will redirect the user to the framework’s details page.

![](../Full/assets/images/create_framework.png)

### Step 3: Creating Competencies

The competency details page contains a hierarchy chart on the left side of the page, with a *Create Root* located at the top.

![](../Full/assets/images/image87.png)

The following steps are used to create root competencies:

_Note: For each competency, the user will be able to add additional information. This will be done in Step 4)._

- Click the _Create Root_ button

    ![](../Full/assets/images/competency-root-button.png)

- Clicking the button launches a modal that contains a form with the following fields:

  - Name

  - Description

  - Type

- Fill out the form details and click the _Create Competency_ button. Doing this will close the modal and the new competency will be added to the hierarchy.

    ![](../Full/assets/images/competency-root.png)
    
Competencies can also be created as children. To do this, please do the following:

- Hover over a competencies name within the hierarchy chart to produce a _three-dot_ icon

     ![](../Full/assets/images/competency-add-child.png)
     
- Clicking the icon launches a modal that contains a form with the following fields:

  - Name

  - Description

  - Scope

  - Type

- Fill out the form details and click the _Create Competency_ button. Doing this will close the modal and the new competency will be added to the hierarchy.

    ![](../Full/assets/images/competency-root.png)


### Step 4: Competency Details

Click on a competency title within the hierarchy chart in the left column. This will load various details about the selected competency on the right side of the page.

From this section, users can edit:

- Relationships

- Crosswalks

- Levels

- Resources Alignments

![](../Full/assets/images/image118.png)

### Step 4-A: Relationships

Click the _+_ icon located next to the relationships heading. 

![](../Full/assets/images/plus-icon.png)

This launches a modal that contains a select box that will allow the user to choose the relationship they are interested in. Relationships include:

- broadens

- desires

- isEquivalentTo

- isRelatedTo

- narrows

- requires

Select _narrows_ then choose another competency. 

_Doing this narrows this competency to the chosen competency (while still keeping it in its current position)._

![](../Full/assets/images/image59.png)

Select a competency and click the _Add Relationship_ button.

![](../Full/assets/images/add-relationship.png)

Next, change the _Relationship Type_ to _broadens_

![](../Full/assets/images/relationship-type.png)

_Doing this broadens this competency to the chosen competency (while still keeping it in its current position)._

Select a competency and click the _Add Relationship_ button.

![](../Full/assets/images/add-relationship.png)

Click the _X_ icon located in the top right corner of the modal. You will see that the new relationships have been added under the _Relationships_ section.

![](../Full/assets/images/x-icon.png)

### Step 4-B: Levels

![](../Full/assets/images/competency-levels.png)

Add levels within the Levels portion of the competency details. Click on the _'Add level'_ button.

![](../Full/assets/images/competency-level-button.png)

This brings up the _Add level_ modal:

- Name

- Title

- Value

- Description

Fill in the information for the modal to add a level to the competency.

![](../Full/assets/images/competency-level-modal.png)
  
Click the _Add level_ button at the bottom right of the modal to add this new level to the competency.

![](../Full/assets/images/competency-add-level.png)

The new level will be reflected in the framework 'Levels' portion of the competency details.

### Step 4-C: Crosswalk

Click the three dot icon to the right of the Framework name.

![](../Full/assets/images/three-dot-icon.png)

This will show a drop down menu to select the 'Crosswalk' option.

![](../Full/assets/images/crosswalk_dropdown.png)

This will open the crosswalk modal to add another competency to the framework.

![](../Full/assets/images/crosswalk.png)

Search for the target competency framework in the search and then select it from the list to then be able to create the cross walk. To complete the crosswalk press the _Create crosswalk_ button.

![](../Full/assets/images/create-crosswalk.png)

This will bring up the crosswalk modal to map competencies between the two frameworks. Select the competencies from the two frameworks that you want to crosswalk.

![](../Full/assets/images/crosswalk-competencies.png)

Then press the _Map competencies_ button to complete the crosswalk.

![](../Full/assets/images/map-competencies.png)

This will bring up the Confirm Crosswalk Action modal that shows:

- Target Competencies

- Description

- Rule

It should display as seen below.

![](../Full/assets/images/confirm-crosswalk.png)

Fill in the appropriate description. To confirm the Crosswalk press the confirm button.

![](../Full/assets/images/confirm-button.png)

The mapping notification will display.

![](../Full/assets/images/map-notification.png)

To exit the crosswalk press the *'X'* icon located in the top right of the modal.

![](../Full/assets/images/map-notification-x.png)

### Step 5: Publish Framework

More information coming soon.







## Importing Competency Framework from JSON

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager* microapplication and click the *Create Framework* button located above the table of frameworks. 

![](../Full/assets/images/image3.png)

Clicking this button launches a modal with a basic form that needs to be completed.

### Step 2: Framework Form

![](../Full/assets/images/competency-manager-update-2.png)

- Click the _Create New_ dropdown menu and change the selection to _Import_. Doing this provides the user with a file picker.

- Click the *Choose File* button and select the appropriate JSON file.

Click the _Import Framework_ button.













## Export Competency Framework as a JSON File

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager* microapplication and click the title of the framework that you wish to export.

![](../Full/assets/images/image3.png)

### Step 2: Export

![](../Full/assets/images/image110.png)

When a user arrives on the competency details page, click the _Options_ icon located next to the framework title.

Clicking this button produces a dropdown menu.

Click the _Export as JSON_ option. Doing this downloads the JSON file to the user’s computer.













## Deleting Competency Frameworks

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager* microapplication and click the title of the framework that you wish to delete.

![](../Full/assets/images/image3.png)

### Step 2: Delete

When a user arrives on the competency details page, click the _Options_ icon located next to the framework title.

![](../Full/assets/images/image29.png)

Clicking this button produces a dropdown menu.

Click the *Delete* option. Doing this produces a modal asking the user to confirm the deletion.













## Performing Crosswalk Competencies

---

### Step 1: Assessment Tab

Navigate to the *Competency Manager* microapplication and click the title of the framework that you wish to crosswalk.

![](../Full/assets/images/image3.png)

### Step 2: Crosswalk

When a user arrives on the competency details page, click the _Options_ icon located next to the framework title.

![](../Full/assets/images/image18.png)

Clicking this button produces a dropdown menu.

Click the *Crosswalk* option. Doing this produces a modal that asks the user to select which framework that is being crosswalked.

![](../Full/assets/images/competency-manager-update-3.png)


### Step 3: Complete Crosswalk

The _Crosswalk_ interface contains two columns.

- Source Framework

- Target Framework

![](../Full/assets/images/image117.png)

Click the checkbox next to your source framework and your target framework’s competency that you wish to crosswalk.

Scroll down and click the *Map Competencies* button located in the bottom right section of the screen. 

![](../Full/assets/images/competency-manager-update-4.png)

Doing this produces a modal which asks the user to create a name and description to confirm the crosswalk.

Click the _Confirm_ button to close the modal and redirect the user back to the _Competency Details_ page.