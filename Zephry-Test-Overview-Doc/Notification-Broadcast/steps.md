# Notification Broadcast

---

## Navigate to Notification Broadcast

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for Notifications or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/notification-navigation.png)

---

## Send Broadcast

---

### Step 1: Notification Basic Information

The Notification Broadcast is a single form that is used to send out notifications to users. To get started, users will need to add a Broadcast Title, a Broadcast Message and a priority.

![](../Full/assets/images/image13.png)

Users can choose between the following priorities:

- Critical

- High-Priority

- Low-Priority

- Passive

![](../Full/assets/images/notification-priority.png)

### Step 2: Notification Recipients

Users will see that the next section of the form allows them to select recipients of the notification.

- To send the notification to everyone, simply click the _Send to Everyone_ toggle.

_Note: The Send to Everyone toggle currently has a bug where multiple notifications are sent to a user._

    ![](../Full/assets/images/notification-toggle.png)

- To send to certain roles, begin typing the desired role in its text area.

  - Doing this will generate a popup menu that contains roles related to the text that was typed.

- To send to certain users, begin typing the users name in its text area

  - Doing this will generate a popup menu that contains users related to the text that was typed.
  
  ![](../Full/assets/images/notification-recipients.png)

### Step 3: Broadcast Via

Finally, users will use the check boxes at the bottom of the form to choose how to send the notification.

Options to send broadcast via:

- All
- Platform 
- Chat (Disabled)
- Email (Disabled)

 ![](../Full/assets/images/notification-send.png)
