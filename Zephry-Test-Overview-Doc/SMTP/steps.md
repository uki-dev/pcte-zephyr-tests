# SMTP

## Add Outgoing Mail Server to JSD

---

### Step 1: Login to Helpdesk

### ![](../Full/assets/images/image77.png)

Log into helpdesk.pcte.mil with JSD admin role.

- Click the _Gear_ icon

- Click the _System_ option from the popup menu

Doing this will open the _System Admin Console_ window within JSD.

### Step 2: Outgoing Mail

Next, the user will scroll down the left side navigation menu to the _Mail_ header.

- Click _Outgoing Mail_

![](../Full/assets/images/image20.png)

Clicking _Outgoing Mail_ will take the user to the SMTP settings page.

### Step 3: Click SMTP Button

Click the _Configure New SMTP Mail Server_ button.

Clicking this button will launch a modal titled, _Add SMTP Mail Server_.

![](../Full/assets/images/image130.png)

### Step 4: Complete SMTP Form

Complete the entire form that contains the following fields:

- Name

- Description

- From address

- Email prefix

- Service Provider

- Protocol

- Host Name

- SMTP Port

Once these fields are complete the user must click the _Add_ button.

Doing this will take you back to the previous screen with an entry for your newly added outgoing server.

### Step 5: Testing the Connection

### ![](../Full/assets/images/image124.png)

In order to test the new outgoing mail server, the user must click on the _Test Email_ link located on the right side of the page.

Doing this opens an email preview screen for the user to complete.

