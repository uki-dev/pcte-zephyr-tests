# Microapp Manager

_Note: This Microapplication Upload feature is currently inactive due to an error in the vendor sevice. The rest of the Microapp Manger's features function as expected._

![](../Full/assets/images/image100.png)

## Add Microapplication

---

### Step 1: Navigate to Microapp Manager

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for the Microapp Manager or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/Microapp-navigation.png)

### Step 2: Click Add New Microapp Button

Upon accessing the *Microapp Manager* microapplication, users will click the *Add New Microapp* button located above the microapplication’s table.

![](../Full/assets/images/Microapp-navigation-2.png)

Once clicked this will launch the *Add New Microapp* modal.

![](../Full/assets/images/image126.png)

### Step 3: Select Vendor

The modal that appears is a form that allows users to add their microapplication. 

First, the user must use the select box to choose which vendor this application is associated with.

![](../Full/assets/images/vendor_select.png)

### Step 4: Select Process

The user will see two different tabs beneath the _Vendor_ select box.

- Upload
- Repository

Users must choose between these two processes to add their microapplication.

### Step 5a: Upload

The _Upload_ option contains a simple file picker. Using this, users can upload their entire microapplication bundle. Once the file is added, click the _View Details_ button, to proceed to the next step.

![](../Full/assets/images/upload_microapp.png)

### Step 5b: Repository

The _Repository_ option provides a _Repository URL, where users can add a link to their application’s repository. 

If this URL connects correctly, the next field (Ref - Branch / Tag), will turn into a select box, where the user can choose the exact branch that they wish to connect the application to. _

![](../Full/assets/images/repo_microapp.png)

Once the URL and branch ref are connected, they can click the _View Details_ button, to proceed to the next step.


### Step 6: Confirm

The final step for uploading a microapplication is clicking upload button

![](../Full/assets/images/microapp-upload.png) 

This will bring of a modal to edit the app name, enable the app, tag, bootstrap, description, wiki url, notes, and add an icon to their microapplication.


![](../Full/assets/images/microapp-edit.png)

Once these charges are complete, the user must click the _Update App_ button. The user will be redirected to the application’s page.

## Edit Microapplication

---

### Step 1: Locate Desired Application

From the microapplication landing page, locate the desired application and click the application's title. 

![](../Full/assets/images/image100.png)

Doing this will redirect the user to the microapplication's _Details_ page.

### Step 2: Edit Overview

![](../Full/assets/images/microapp-edit.png)

The microapplication's _Details_ page contains a form that allows users to update the following fields:

- Microapp Name
- Tag
- Bootstrap
- Description
- Wiki URL
- Notes
- Render Contexts
- Client Name
- Root

-  Once the correct information has been added, click the _Update App_ button.

### Step 3: Change Microapplication Icon

![](../Full/assets/images/upload-1.png)

Click the _Choose File_ button to browse your computer and select a png or jpg. The application's icon will save instantly as soon as the file is sel ected.

## Change Microapplication Permissions

---

### Step 1: Access Permissions

![](../Full/assets/images/image64.png)

Users will now click the _Permissions_ tab, located at the top of the page.

### Step 2: Change Permissions

To change a roles visibility, users will need to do the following:

**Add Role**
Users will find a list of roles on the left side of the page. To add a role, simply drag the role’s title to the appropriate section on the left (App Visibility card or Client Permissions card).

![](../Full/assets/images/add_role.png)

**Remove Role**
To remove a role, a user needs to hover over the role that is going to be removed and click the *X* icon that appears. The role will be removed instantly.

![](../Full/assets/images/remove_role.png)

## Delete Microapplication

---

### Step 1: Locate Desired Application

From the microapplication landing page, locate the desired application and click the application's title. 

![](../Full/assets/images/image100.png)

### Step 2: Click Options Icon

![](../Full/assets/images/options_microapp_button.png)

Users will notice an *Options* icon located to the right of the microapplication’s title. Clicking the *Options* icon will launch a popup menu with the following options:

- Edit

- Disable (or Enable)

- Delete (If applicable)

![](../Full/assets/images/options_microapp.png)

### Step 3: Delete Microapplication

Next, users will want to click the *Delete* option to delete the microapplication.

## Enable / Disable Microapplication

---

### Step 1: Locate Microapplication

From the _Microapp Manager page, users must locate and hover over the desired microapplication’s row (within the Microapplication table)._

### Step 2: Click Options Icon

Hovering over the microapplication’s row will produce an _Options_ icon on the far right-hand side of the table.

![](../Full/assets/images/image75.png)

Clicking this icon will produce a dropdown menu with the following options:

- Edit

- Disable (or Enable)

- Delete

### Step 3: Click Enable (or Disable)

Clicking the _Enable (or Disable)_ option will enable (or Disable) the microapplication.

_Note: Users can also delete microapplications from this location._

## Add Shared Request

---

![](../Full/assets/images/image25.png)

### Step 1: Shared Request Tab

From the landing page of the _Microapp Manager_, users will want to click the _Shared Request_ tab located at the top of the page. Doing this will redirect the user to the *Shared Request* overview page.

![](../Full/assets/images/image39.png)

### Step 2: Click the Add Shared Request

Next, click the *Add Shared Request* button located at the top of the page. Doing this launches a modal.

### Step 3: Complete Shared Request Form

Users will notice the following fields, that are required to add a shared request:

- Request Name
- Request Type
- Access Mapping

The _Request Type_ field will add additional fields to the form depending on the type that is requested.

![](../Full/assets/images/image56.png)

Once completed, click the _Save_ button located at the bottom of the form.

## Edit Shared Request

---

### Step 1: Shared Request Tab

From the landing page of the _Microapp Manager_, users will want to click the _Shared Request_ tab located at the top of the page. Doing this will redirect the user to the *Shared Request *overview page.

### Step 2: Click Shared Request Title

Users must click the desired Shared Request’s title to launch a modal. This modal contains editable information on the Shared Request.

### Step 3: Edit Shared Request

Update the Shared Request’s information and click the _Save_ button.

