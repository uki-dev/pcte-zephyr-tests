# Whiteboard


## Navigate to Whiteboard

---

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for Whiteboard or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/white-board.png)

---

![](../Full/assets/images/image50.png)

## Create New Whiteboard

---

### Step 1: Click _Create Board_ Button

When a user first accesses the _Whiteboard_ widget they will see a blank canvas with a list of available whiteboards on the left side of the page. Users will see a _Create Board_ button located beneath the list of boards.

Click the _Create Board_ button.

![](../Full/assets/images/white-board-create.png)

### Step 2: Complete New Whiteboard Form

Users are given a whiteboard form with the fields.

- Name

- Metadata Tags

![](../Full/assets/images/image34.png)

The metadata tags can be added and used to help users find the whiteboard. After completing the form, click the _Launch Board_ button to finish creating the whiteboard.

## Add Participant to Whiteboard

---

![](../Full/assets/images/image4.png)

### Step 1: Click the _Add Participant_ Button

After creating a new whiteboard, users will see a *Participants* panel located on the right side of the page.

Click the _Add Participant_ button.

### Step 2: Click Participant’s Name

Clicking the _Add Participant_ button clears the right panel and displays a list of available users. Simply click the participant’s name that needs to be added to the whiteboard.

## Claim Presenter

---

### Step 1: Click the _Claim Presenter_ Button

In order to edit the whiteboard’s canvas, a user must become the _presenter_. In order to become the _presenter_, users must click the _Claim Presenter_ button that is located in the bottom right corner of the widget.

![](../Full/assets/images/image129.png)

Clicking the _Claim Presenter_ button, will display various objects on the left side of the whiteboard’s canvas and give the user editing capabilities.

## Add Shape to Whiteboard

---

### Step 1: Click the *Shape *Icon

Click the *Shape *icon located on the left side of the page. Doing this will generate a popup menu with various shapes and a _Stroke Thickness_ bar.

![](../Full/assets/images/image84.png)

### Step 2: Select Type & Stroke

Choose one of the four shapes that is located on the top of the popup menu by clicking it. Next, use the number picker to select how large of a border you wish to have on your shape.

### Step 3: Draw Shape

Once the shape and stroke are selected, click onto the canvas and drag your mouse to where the shape will be located on the canvas.

## Add Text to Whiteboard

---

### Step 1: Click the *Text *Icon

Click the *Text *icon located on the left side of the page. Doing this will generate a popup menu with a number picker that is used to determine the text size.

![](../Full/assets/images/image8.png)


### Step 2: Select Text Size

Use the number picker to determine the font size of the text layer that you will be creating.


### Step 3: Click Onto Canvas & Type

Once the text size is determined, click onto the canvas and begin typing.

## Change Layer Color

---

### Step 1: Click the _Canvas Color_ Icon

Click the *Canvas Color *icon located on the left side of the page. Doing this will generate a popup menu that contains an extensive color selector.

![](../Full/assets/images/image61.png)


### Step 2: Select Color

Use the color selector to choose which color you would like to use for your next layer.


### Step 3: Add Layer

Once a color is chosen, click the *Text *icon or the _Shape_ icon located on the left side of the page and create a new layer. This layer will have the colors that was chosen.

## Add Image to Whiteboard

---

### Step 1: Click the _Add Image_ Icon

Click the *Image *icon located on the left side of the page. Doing this will generate a file picker which allows users to pick an image from their computer.

![](../Full/assets/images/image53.png)


### Step 2: Choose Image From Computer

Search your computer for the desired image.


### Step 3: Resize Image

After an image is selected it will be added to the canvas. To resize the image, the user must drag and drop the corners of the image to the desired size.

## Move Layer on Whiteboard

---

### Step 1: Click the _Cursor_ Icon

Click the *Cursor *icon located on the left side of the page.


### Step 2: Drag & Drop Layer

With the _Cursor_ icon selected, drag and drop the layer that is to be moved, to the desired location.
