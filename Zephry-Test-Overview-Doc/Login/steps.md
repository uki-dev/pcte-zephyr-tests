# Login

![](../Full/assets/images/image125.png)

## Login Process

---

### Step 1: Click Login

When a user accesses the OpenDash360™ environment, they will have access to:

- Persistent Cyber Training Environment Details
- DoD Warning & Consent Banner
- Action Buttons

Users must click the _Login_ button located beneath the _Persistent Cyber Training Environment_ information.

Doing this will redirect the user to the _Keycloak Login_ form.

![](../Full/assets/images/image23.png)

### Step 2: Login Credentials

The user must now enter their username and password. Once this is done, the user must click the _Login_ button.

After successfully logging in, the user will be sent to the OpenDah360™ Lobby.

![](../Full/assets/images/lobby-1.png)




## Confirm DoD Warning & Consent Stays on Landing Page

---

### Step 1: Refresh Landing Page

When a user accesses the OpenDash360™ environment, they will see the DoD Warning & Consent Banner located on the right side of the page.

![](../Full/assets/images/lobby-2.png)

    
The user will want to refresh their browser. After this, they will notifce that the DoD Warning & Consent Banner remains in the same location.

## Password Reset

---

### Step 1: KeyCloak Login Form

The process for a user to reset their password begins on the *Keyclock Login *form page.

- Click the _Forgot Password_ link.

![](../Full/assets/images/lobby-3.png)

Clicking this link will redirect the user to the _Self Service Account Management_ page.

![](../Full/assets/images/image68.png)

### Step 2: Enter Email Address

On the _Self Service Account Management_ page, enter the user’s email address and click the _Request Password Reset_ button.

\*Doing this will send an email to the user. This email will contain directions on resetting the user’s password.

\*_If email is set up in an environment._
