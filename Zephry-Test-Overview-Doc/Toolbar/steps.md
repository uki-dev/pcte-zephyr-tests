# Toolbar

_Note: Please utilize Google Chrome when testing the Toolbar. Firefox has some known-issues that have yet to been resolved._

![](../Full/assets/images/taskbar-new-1.png)

## Launch Applications / Widgets

---

### Step 1: Click Application Icon

Click the icon located on the far left side of the toolbar.

Doing this will launch a panel that contains all available applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)


### Step 2: Searching for Microapplications / Widgets

Users will find a search bar located at the top of the panel. Typing the desired applications title will produce the application icon.

- Type _User_

This will filter the applications to only display the _User Manager_ application.

![](../Full/assets/images/taskbar-new-2.png)

- Click the _User Manager_ application title.

Doing this will close the panel and load the _User Manager_ application. 






## Launch App Cards

---

### Step 1: Click App Card Icon

![](../Full/assets/images/taskbar-new-3.png)

The user will see various icons located to the right of a faint vertical rule. These icons are known as _App Cards_.

- Click one of the _App Card_ icons.
- Doing this will load the application that the app card is associated with.

_Note: The Home icon is default and will always be present. This icon links to the Lobby._








## Launch User Center

---

### Step 1: Click User Center Icon

Users will see a single circle with the user’s initials located on the right side of the toolbar.

![](../Full/assets/images/taskbar-new-4.png)

- Click this circle: Doing this launches the _User Center_ .

Click the _X_ icon in the upper-right corner of the panel to close the _User Center_.


![](../Full/assets/images/user_center_x.png)




## View Roles & Organization Information

### Step 1: Click _User Center_ Icon

Users will see a single circle with the user’s initials located on the right side of the toolbar.

![](../Full/assets/images/taskbar-new-4.png)

- Click this circle: Doing this launches the _User Center_ .

### Step 2: Click _My Profile_

- Click the _My Profile_ button: Doing this loads profile information within the _User Center_ panel.

![](../Full/assets/images/taskbar-new-6.png)

Users will find their _Roles_ and _Organization Information_ at the bottom of this panel.

### Step 3: Close _User Center_

Click the _X_ icon in the upper-right corner of the panel to close the _User Center_.

![](../Full/assets/images/user_center_x.png)



## Change Password

### Step 1: Click _User Center_ Icon

Users will see a single circle with the user’s initials located on the right side of the toolbar.

![](../Full/assets/images/taskbar-new-4.png)

- Click this circle: Doing this launches the _User Center_ .

### Step 2: Click _Change Password_

- Click the _Change Password_ button: Doing this loads the _Change Password_ interface within the _User Center_ panel.

- Fill out the following fields: _Current Password_, _New Password_, _Confirm New Password_ .

![](../Full/assets/images/taskbar-new-7.png)

- Click the _Update Profile_ button: Doing this will update the user's password.

_Note: Passwords must meet the PCTE Password Requirements._

### Step 3: Close _User Center_

Click the _X_ icon in the upper-right corner of the panel to close the _User Center_.

![](../Full/assets/images/user_center_x.png)



## Submit Feedback

### Step 1: Click _User Center_ Icon

Users will see a single circle with the user’s initials located on the right side of the toolbar.

![](../Full/assets/images/taskbar-new-4.png)

- Click this circle: Doing this launches the _User Center_ .

### Step 2: Click _Submit Feedback_

- Click the _Submit Feedback_ button: Doing this loads the _Feedback_ form within the panel.  
- Take note of what page that is being rated. The user should see the page's name listed at the top of the panel.

![](../Full/assets/images/taskbar-new-8.png)

- Select the number of stars.
- Type in a comment.
- Click the _Choose File_ button to attach an image of the user interface that is being rated.
- Click the _Submit Feedback_ button.

Clicking the _Submit Feedback_ button will send the feedback and close the _User Center_ panel.







## Logout

### Step 1: Click _User Center_ Icon

Users will see a single circle with the user’s initials located on the right side of the toolbar.

![](../Full/assets/images/taskbar-new-4.png)

- Click this circle: Doing this launches the _User Center_ .

### Step 2: Click _Log Out_

- Click the _Log Out_ button.

Doing this will instantly sign the user out of OpenDash360™












## Send Chat Message

---

Click the _Chat_ icon located on the right side of the toolbar.

![](../Full/assets/images/taskbar-new-10.png)


### Step 1: Select Team

- Find and click the _Teams_ select box located at the top of the _Chat_ panel. 

![](../Full/assets/images/taskbar-new-9.png)

Changing a team updates the chat conversations that are available to the user.

_Note: New conversations might not populate the _Conversation List_. If this is the case, the current work-around is to toggle the _Teams_ filter twice._

### Step 2: Search Conversations

- Utilize the _Search Conversations_ text field to find the desired conversation.

![](../Full/assets/images/taskbar-new-11.png)

The search field will update while the user types (Does not require submission of text field). 

### Step 3: Chat Conversations

Hover over the conversation that you would like to view and click the title. Doing this will open the conversation to the left of the Chat pane.

![](../Full/assets/images/chat-2.png)


### Step 4: Minimize Conversation

Click the Options icon located at the top of the conversations popout. This produces a menu that contains the following:

-   Minimize
-   Launch
-   Invite Members
-   Manage Members
    

![](../Full/assets/images/chat-3.png)

Click the Minimize option. Doing this will minimize this conversation and will create a new tab in the Taskbar that is titled the same as the conversation.


### Step 5: Open Minimized Conversation

Click the tab that was created in the Taskbar. Doing this will reproduce the conversation popout.

![](../Full/assets/images/chat-4.png)


### Step 6: Launch Conversation

Click the Options icon located at the top of the conversations popout. This produces a menu that contains the following:

-   Minimize
-   Launch
-   Invite Members
-   Manage Members
    

Click the Launch option. Doing this removes the conversation from its current position and re-produces it as a floating window.


### Step 7: Pin Conversation to Taskbar

Click the Options icon. Doing this produces a menu with the following options:

-   Open as Fixed Window
    
-   Pinned Posts
    

Click the Open as Fixed Window option to return the conversation to the taskbar.


### Step 8: View Conversation Members

Click the Options icon located at the top of the conversations popout. This produces a menu that contains the following:

-   Minimize
-   Launch
-   Invite Members
-   Manage Members
    

Click the View Members option. Doing this refreshes the interface and provides a searchable list of all of the members that are tied to the conversation.

![](../Full/assets/images/chat-5.png)

Click the X icon to return back to the conversation’s messages.

### Step 9: Sending Messages

Click into the text field located at the bottom of the conversation panel. Then, type in a brief message and hit the Enter key when finished.

Doing this will send a message to the user(s) that are part of your conversation.


### Step 10: Send Image Message

Click the Paper Clip icon located to the right of the conversation text field. Doing this will open up a Finder that will allow the user to search their computer to select an image.

Once an image is located and selected, click into the text field and type a message to go along with the image.

Hit the Enter key. This will successfully send your message (and image).


### Step 11: Send Emoticon Message

Click the Smiley Face icon located to the right of the conversation text field. Doing this will open up a panel with a list of Emoticons.

![](../Full/assets/images/chat-6.png)

Select the desired Emoticons

Once an icon is located and selected, click into the text field and type a message to go along with the icon.

Hit the Enter key. This will successfully send your message (and icon).

## Editing Message

---

### Step 1: Pin Message to Channel

Hover over a message that is within the conversation. Doing this will make three options appear:

-   Pin Icon
    
-   Edit
    
-   Delete
    

Note: A user can only Edit or Delete their own message.

![](../Full/assets/images/chat-7.png)

Click the Pin icon. Doing this will fill in the icon. Currently, users can see pinned posts by completing the following steps:

-   Launch the conversation (see above)
    
-   Click the Options Icon
    
-   Click Pinned Posts



### Step 2: Edit Sent Message

Hover over a message that is within the conversation. Doing this will make three options appear:

-   Pin Icon
    
-   Edit
    
-   Delete
    

Click the Edit option. Update the message and click the Edit button when finished.


### Step 3: Delete Sent Message

Hover over a message that is within the conversation. Doing this will make three options appear:

-   Pin Icon
    
-   Edit
    
-   Delete
    

Click the Delete option. Doing this will ask the user for confirmation of deleting the message.

## Open Chat Options

---

### Step 1: Mute Audio Alert

Within the Chat panel (contains a list of all available chat conversations). Click the Options icon located to the right of the Teams select box. Doing this opens a popup menu that contains:

-   Mute Audio Alert
    
-   Mark All Read
    
-   Launch Open Chat
    

Click the Mute Audio Alert option.

![](../Full/assets/images/chat-8.png)

Doing this will turn off the sound that occurs when a user receives a message.


### Step 2: Mark All Messages as Read

Within the Chat panel (contains a list of all available chat conversations). Click the Options icon located to the right of the Teams select box. Doing this opens a popup menu that contains:

-   Mute Audio Alert
    
-   Mark All Read
    
-   Launch Open Chat
    

Click the Mark All Read option.

Doing this will mark all of the conversations in the Chat application as Read.


### Step 3: Launch Open Chat

Within the Chat panel (contains a list of all available chat conversations). Click the Options icon located to the right of the Teams select box. Doing this opens a popup menu that contains:

-   Mute Audio Alert
    
-   Mark All Read
    
-   Launch Open Chat
    

Click the Launch Open Chat option.

Doing this will launch the full scale Chat application in a new window.

## Launch Notifications

---

### Step 1: Click Notifications Icon

Clicking the *Notifications* icon launches the *Notifications* pane.

![](../Full/assets/images/image85.png)

Filter the notifications by: Clicking the select box and choosing the desired filter

![](../Full/assets/images/filter_notifications.png)

Mark the Notification as *Read* by: Clicking the notification and then clicking the *Mark as Read* button

Clicking the *X* icon located in the top right corner of the *Notifications* pane will close the interface.



## Collapse / Expand Toolbar

---

### Step 1: Right Click Toolbar

The toolbar can be collapsed be performing the following actions:

- Right click an empty space on the toolbar. Doing this produces a popout menu.

![](../Full/assets/images/taskbar-new-12.png)

- Select the _Collapse Toolbar_ option. Doing this removes the toolbar and only leaves behind an icon in the bottom-left corner of the window.

![](../Full/assets/images/taskbar-new-13.png)

- Click the icon that in the bottom-left corner of the window: Doing this generates the toolbar.