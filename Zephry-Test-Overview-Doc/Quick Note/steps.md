# Quick Note

_Note: Quick Note currently has a few issues that need to be addressed before it can be utilized. Once these issues have been addressed, UKI will let the test team know._

![](../Full/assets/images/image21.png)

## Create New Note

---

### Step 1: Click _Take a Note_ Textfield

![](../Full/assets/images/take-note.png)


Click the _Take a Note_ textfield. Doing this will change the text field to a container with two different elements that must be filled out:

- New Note Title

- New Message Goes Here

![](../Full/assets/images/image107.png)

Once these fields are complete, click the *Publish* button located underneath (and to the right) of the message container.

## Pin Note

---

### Step 1: Click _Options_ Icon

The user will find and hover over the note that they wish to pin. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note

### Step 2: Click _Pin Note_

The user will now click the _Pin Note_ option. Doing this will immediately send the note to the _Pinned Notes_ section.

## Share Note

---

### Step 1: Click _Options_ Icon

The user will find and hover over the note that they wish to share. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note

### Step 2: Click _Share Note_

The user will now click the _Share Note_ option. Doing this will immediately open the note and display a search box for _Roles._.

### Step 3: Select Recipients

Begin typing the role name that will be receiving this note. An autocomplete will be provided if a role is found.

### Step 4: Click _Share_ Button

Finally, the user simply needs to click the _Share_ button located at the bottom of the page.

## Highlight Note

---

### Step 1: Click *Options* Icon

The user will find and hover over the note that they wish to highlight. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note

### Step 2: Click _Edit Note_

The user will now click the _Edit Note_ option. Doing this will immediately open the note and display various icons at the bottom of the note.


### Step 3: Click *Color* Icon

The user will now click the _Color_ icon. Doing this will immediately open various color options that are available.

### Step 4: Select Color

The user will click the desired color.


### Step 5: Save the Note

After the color is established, the user needs to click the _Save_ button located in the bottom right corner of the note.

## Add Image to Note

---

### Step 1: Click *Options* Icon

The user will find and hover over the note that they wish to edit. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note


### Step 2: Click _Edit Note_

The user will now click the _Edit Note_ option. Doing this will immediately open the note and display various icons at the bottom of the note.


### Step 3: Click *Image* Icon

The user will now click the _Image_ icon. This icon acts as a file picker.


### Step 4: Select Image

The user will now select the appropriate image file from their computer. Doing this will automatically add the image to the note.

### Step 5: Save the Note

After the image is added, the user needs to click the _Save_ button located in the bottom right corner of the note.

## Delete Note

---

### Step 1: Click *Options* Icon

The user will find and hover over the note that they wish to delete. Hovering over the note will produce a _Settings_ icon on the right side of the note.

When the user clicks this _Options_ icon, a popup menu will display, giving the following options:

- Edit Note

- Share Note

- Pin Note

- Delete Note


### Step 2: Click _Delete Note_

The user will now click the _Delete Note_ option. Doing this will immediately delete the note.

## Filter Notes

---

### Step 1: Select Note Type

Users will find various tabs located at the top of the widget. Options include:

- All Docs

- Personal

- Shared

The user can filter notes by clicking the desired tab. Doing this will load the appropriate notes.

