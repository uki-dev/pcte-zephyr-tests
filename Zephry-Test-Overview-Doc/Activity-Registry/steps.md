# Activity Registry

---

The  Activity Registry is not currently integrated into the DevOps zone.
---

### Navigate to Activity Registry

---

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for Activity Registry or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/activity-registry-navigation.png)


---

The landing page of the _Activity Registry_ consists of two tabs located within the header of the page:

- Learning Resources
- Competency Search

When an administrator launches the _Activity Registry_ application, they will see a table containing various resources.

![](../Full/assets/images/activity-1.png)


## View Learning Resource Details

Click the name of the desired learning resource. Doing this loads a new interface that contains the following tabs:

- Overview
- Related Content

![](../Full/assets/images/activity-2.png)


### Overview

This interface allows administrators to see the following details:

- Resource Name
- Description
- URL
- Educational Use
- Learning Resource Type
- Identifier
- Creation Date
- Creation Author
- Publish Date

Beneath those details, administrators will see an _Assesses_ section. This section contains the URLs of all of the competencies that this resource assesses.

- Double click to highlight one of the URL's titles. Hit the _Command_ _C_ keys to copy the selection. Doing this copies the entire URL of the competency.

### Related Content 

Currently is not operable.

## Activity Registry Search

Currently is not operable.