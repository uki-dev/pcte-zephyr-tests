# Lobby

## Server

---

At the very top of the page is the Server Dropdown.

![](../Full/assets/images/server.png)

Click on the drop down arrow to show the servers that can be changed to.

![](../Full/assets/images/server-1.png)

## Launch App Cards

---

The app cards are located directly below the title _'Persistent Cyber Training Environment'_.

![](../Full/assets/images/lobby-a-1.png)

These app cards interact with outside resources directly from PCTE.

### Step 1: App Card OpenDash360

Hover over the OpenDash360 App card and see the arrow in box icon. 

![](../Full/assets/images/app-cards-open.png)

Click the icon located on the top right side of the App card.

Doing this will redirect you to the OpenDash360 portal.




### Account Services - Change password

Scrolling down further on the lobby. Users will see the Account Services Displayed. This will show the details for the user:

- Username
- Email address
- Role
- Organization
- Password Expiration Date
- Change Password

![](../Full/assets/images/lobby-a-7.png)

Click the _Change password_ button located at the bottom of the Account Services.

This will bring up the Change password window for the user to enter a new password.


The display shows:

- New Password
- Confirm New Password
- Cancel Button
- Change Button

The user will type in matching passwords for the _New Password_ and _Confirm New Password_ fields and click on the **Change** button. 



## Notifications

---

### Step 1: Scroll Down to View the Notifications

![](../Full/assets/images/lobby-a-2.png)

The user will see all the notifications that are currently available each notification is clickable and can be marked as read or unread.

![](../Full/assets/images/lobby-a-3.png)

- Click one of the _App Card_ icons.
- Doing this will load the application that the app card is associated with.

_Note: The Home icon is default and will always be present. This icon links to the Lobby._

### Step 2: Sort by Priority / Date

To the right of the Priority and Date column headers is an arrow for both descending and ascending sort of notification

![](../Full/assets/images/lobby-a-4.png)

- Downward Arrow = Descending
- Upward Arrow = Ascending 

### Step 3: Dropdown Notifications

To the right of the notifications title is a Dropdown to filter notifications by:

- All
- Unread
- Read
- Critical
- High-Priority
- Low-Priority
- Passive
  
### Step 4: Three Dot

![](../Full/assets/images/notification-three-dot.png)

To the right top corner of the notification display is the three dot icon that allows you to both mute or clear all the notifications.

\*_Note: The clear all notifications is still not operable_

---

### Step 1: FAQ

![](../Full/assets/images/lobby-a-5.png)

Scrolling down further on the lobby. Users will see the Frequently Asked Questions portion of the lobby.

- The user can click on the Frequently Asked Questions window to expand and collapse the window. 

The user can click onto the windows within to expand and collapse the Frequently Asked Questions.

_\*Note: No current FAQs in the dev environment_





---
### Step 1: Support Videos

![](../Full/assets/images/lobby-a-6.png)


Scrolling down to the bottom of the lobby. Users will see the Support Videos. 

Click on the Support Video and it will open and begin to play

_\*Note: This is currently not functioning in the dev environment_

