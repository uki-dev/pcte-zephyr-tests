# JQR Manager

---

Before using the JQR Manager, please access the _Competency Manager_ microapplication and verify that the _Cyber Mission Forces_ framework exists.

If this is not the case, please use the _Competency Manager_ guide and create the _Cyber Mission Forces_ framework and then return to the _JQR Manager_ microapplication.
---

## Navigate to JQR Manager

From the lobby page click the nine square icon located in the bottom left corner of the web browser, this is the menu to launch the listed applications.

![](../Full/assets/images/Assessment-Manager-Navigation-1.png)

Search for the JQR Manager or click on the corresponding Application from the application menu. This will launch the application window.

![](../Full/assets/images/jqr-navigation.png)

---

## JQR Manager

![](../Full/assets/images/jqr-app.png)

The landing page of the JQR Manager contains two tabs:

- Work roles
- Config
 
 Users will want to start at the _Config_ tab, so that they can choose which framework that they would like to add a JQR within.

---

## Create JQR


First, navigate to the _Config_ tab.

- From the _Config_ tab, click the _Choose a framework_ select box and select the framework that you would like to work within. Selecting a framework will automatically save and the user will be working within that framework.

- Click the _Work Role_ tab.

- Click on the Create JQR button located in the top right of the modal. 

![](../Full/assets/images/jqr-button.png)

This will open the JQR template. Fill in the following information to begin creating the JQR:

- JQR name
- Description
- JQR Logo
- Import JQR**

![](../Full/assets/images/jqr-template.png)

Click the Build JQR Button. To finalize the JQR.

![](../Full/assets/images/jqr-build.png)

_**Note: The Import JQR field is for importing a JQR using a JSON file. Please Disregard for now._

---

## Edit Work Role

Click on the desired JQR to access the _JQR Details_ page. 

![](../Full/assets/images/jqr-name.png)

The _JQR Details_ page includes the following tabs:

- Overview
- Learning Activities (Disabled)
- Work Roles (Disabled)
- People (Disabled)

Click the JQR's name within the hierarchy on the left column. Doing this will load the JQR's details on the right side of the page. 

![](../Full/assets/images/jqr-overview.png)

### Proficiency Levels

Click the _Add proficiency_ button or the *plus* icon. 

![](../Full/assets/images/add-proficiency.png) 

![](../Full/assets/images/plus-icon.png) 

This will bring up the proficiency modal which allows the user to fill out the following information. 

- Proficiency name
- Description

![](../Full/assets/images/create-proficiency.png)

Click the _Create proficiency_ button. Doing this will close the modal and the proficiency will be added the the JQR's _Proficiency Levels_.

![](../Full/assets/images/create-proficiency-button.png)

### Edit/Delete Competency

Click on the three dot icon to the top right corner of the layout to edit or delete the competency.

![](../Full/assets/images/edit-competency.png)

### Learning Activities

Not currently operable.

### Work Roles

Not currently operable.

### People

Not currently operable.

---

## Config

The config is for the JQR Global Settings. So the user can choose a framework and add scoring to the competency types.

Fields include:

- Framework

- Competency Types / Proficiency Level

![](../Full/assets/images/jqr-config.png)

### Choose a framework

Click the dropdown menu and select a framework.

![](../Full/assets/images/jqr-framework.png)

### Create Competency Type

Click the _plus_ icon located to the right of Competency Types / Proficiency Level.

![](../Full/assets/images/plus-icon.png) 

This will bring up the _Create Competency Type_ modal to add a single or Multi competency.

Adding a _Competency Type / Proficiency Level_ here, adds a global competency / proficiency level so that they can be added to any work role.

![](../Full/assets/images/jqr-competency.png)

**Single:**

   Fields include:

   - Name 
   - Description
   
**Multi:**

   Fields include:

   - Search for Competency Types
   
   ![](../Full/assets/images/jqr-competency-multi.png)
   
Click the submit button to submit the competency type.

### Add Scoring

Click the drop down arrow to the left of the added competency.

![](../Full/assets/images/dropdown-arrow.png)

Shows similar scoring dashboard to assessment manager.

- To add additional scoring level click the "__Add additional scoring level__" button

- Update the scoring levels to the appropriate Score name.

- Click the _Add Additional Scoring Level_ to provide more scoring options.

- Click the _Save_ button.

![](../Full/assets/images/scoring.png)

### Edit/Delete Competency Types / Proficiency Level

Click the three dot icon to the right of the competency type to edit or delete the competency type.

![](../Full/assets/images/edit-types.png)