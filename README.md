# Zephyr Tests for OD360
## Change Log
## v2.0 new Features
* Update PCTE-18629
* Update PCTE-18632
* Update PCTE-18633
* Update PCTE-18635
* Update PCTE-18655
* Update PCTE-18657
* Update PCTE-18658
* Update PCTE-18659
* Update PCTE-18660
* Update PCTE-18661
* Update PCTE-18662
* Update PCTE-18663
* Update PCTE-18664
* Update PCTE-18668
* Update PCTE-18669
* Update PCTE-18672
* Update PCTE-18673
* Update PCTE-18674
* Update PCTE-18675
## Regression
* Update PCTE-11992
* Update PCTE-12285
* Update PCTE-11997
* Update PCTE-11995
* Update PCTE-11996
* Update PCTE-11998
* Update PCTE-13450
* Update PCTE-14921